import {Redirect} from 'react-router';
import React from 'react';
import '@coreui/coreui/dist/css/coreui.min.css'
import "./login.css"
import './Requisition.css'
import Requisition from "./Requisition"
//taking a class ccomponent
 class Login extends React.Component {
    
     //by taking a constructor
    constructor(){
        //super function allows the call the constructor of parent class
        super();
        //this.state allows us to store property values of the component
      this.state={
          //input is taken as null
            input:{},
            //errors are taken as null
            errors:{},
            isPasswordShown:false,
            emailValidation:false,
            passwordValidation:false,
            condition :false
        };
        
    } 
         //the function for password visibility toggling
    togglePasswordVisibility=()=>{
         const {isPasswordShown}=this.state;
         this.setState({isPasswordShown:!isPasswordShown});
     }
    //handle change event for input changes handling
    handleChange=(event)=>{
        //by taking input variable we are storing the value of input state in this.state 
            let input=this.state.input;
            //by importing values of target value to name
            input[event.target.name]=event.target.value;
            if(event.target.name=="user"){
                if(input["user"]=="sanahardeep1998@gmail.com"){
                let input=this.state.input;
                let errors=this.state.errors;
                var pattern1=/^[\w]{3,}[\d]?[\.]?[\w]{2,}[@][\w]{2,}\.[\w]{2,}$/i;
                input=event.target.value;
                var valid=pattern1.test(input)
                if(valid===true){
                errors["user"]="";
                this.state.emailValidation=true;
                }
                else{
                errors["user"]="Please enter valid email";
                }
            }
            }
            if(event.target.name=="password"){
                if(input["password"]=="Sana.deep7")
                {
                let errors=this.state.errors;
                var pattern2=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/i;
                input=event.target.value;
                var valid=pattern2.test(input)
                if(valid===true){
                errors["password"]="";
                this.state.passwordValidation=true
                }
                else{
                errors["password"]="Please enter valid password";
                }
            }
            }
            //by using setState we are taking input
            this.setState({
                input
            });
    }
// handle submit event takes place whenever we input some value
    handleSubmit=(event)=>{
        if(this.state.emailValidation==true && this.state.passwordValidation==true /*&& this.state.condition*/){
            this.setState({ condition: true });
            // window.location.href="https://www.google.co.in/"
            // window.open("https://www.google.co.in/")
            // console.log(window.location.href,"window.location.href")
            // this.setstate({condition:true})
            // console.log(this.state.condition,"after setstate")
            // if(this.state.condition){
                // return <Redirect to='/Requisition'/>
            // }
                
            // event.preventDefault()
            // }
        //     <Router>
        //    <Link to="/Requisition"></Link>
        //    <Switch>
        //        <Route path="/Requisition"></Route>
        //    </Switch>
        //     </Router>
        }
        else  {
            let errors=this.state.errors
            errors["email"]='*required'
            errors["password"]='*required'
            event.preventDefault()

    }

    }    
    //   render () {
    
    //      return <button onClick={() => this.handleClick()}>Redirect</button>;
    // }
  render() {
      const {isPasswordShown,condition}=this.state;
      if (condition) {
        return <Redirect to='/Requisition'/>;
      }
    return (
      <div className="main-div">
          <form /*onSubmit={this.handleSubmit}*/>
          <div>
        <input id="email"type="text"name="user" value={this.state.input.email} placeholder='Enter your mail' onChange={this.handleChange}></input>
        <div className="error">{this.state.errors.email}</div>
      </div>
      <div>
          <input id="password" type={(isPasswordShown)?"text":"password"} name="password"value={this.state.input.password} placeholder="Enter your password" onChange={this.handleChange}></input>
          {/* here we have taken a button to toggle the password visiblity */}
          <button type="button" onClick={this.togglePasswordVisibility}>show</button>
          <div className="error">{this.state.errors.password}</div>
      </div>
      <div className="sub-div">
          <button id="submit" type="submit" value="Submit" onClick={this.handleSubmit} >Submit</button>
      </div>
      </form>
      </div>
    )
  }
}
export default Login;
// import React, { Component } from 'react'
// import { Redirect } from 'react-router-dom'
// import Requisition from './Requisition.js'
// export default class Login extends Component {
//     constructor(props){
//     super()
//     this.state={
//         page:null,
//     }
//     console.log(this.state.page,"before condition")
//  HandleOnClick=()=>{
//     if(this.state.page){
//     this.props.history.push('/Requisition.js')
//     }
//     this.setState({
//         page:true
//     })
// }
//     // const handleClick=(event)=>{
//     //   if(this.state.auth==true)
//     //     return <Redirect to="/Requisition" />
//     }
//     // }
//   render() {
//      console.log(this.state.page)
//     return (
//       <div>
//         <button onClick={()=>this.HandleOnClick}value="submit">submit</button>
//       </div>
//     )
//   }
// }
// import { Redirect } from "react-router-dom"
// import React,{Component} from "react";
// class Login extends React.Component {
//   constructor(props){
//      super(props)
//      this.state = {
//     	    condition: false
//   		}
//   }
//   handleClick=()=> {
//     this.setState({ condition: true });
//   }

//   render () {
//     const { condition } = this.state;

//      if (condition) {
//        return <Redirect to='/Requisition'/>;
//      }
//      return <button onClick={() => this.handleClick()}>Redirect</button>;
// }
// }
// export default Login