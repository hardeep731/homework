import React from 'react';
// import commonService from '../services/commonService';

const ports = {
  "status": "OK",
  "data": [
      {
          "portId": -449290735,
          "branchId": "1",
          "countryId": "0",
          "portCode": "JR",
          "portName": "FUJAIRAH",
          "isActive": "1",
          "portRegion": "0",
          "createBy": "FELICIA",
          "editBy": "FELICIA",
          "createDate": "2012-01-30 11:06:36.073",
          "editDate": null
      },
      {
          "portId": 6,
          "branchId": "1",
          "countryId": "0",
          "portCode": "SG",
          "portName": "SINGAPORE",
          "isActive": "1",
          "portRegion": "0",
          "createBy": "BTS",
          "editBy": "",
          "createDate": "2007-03-07 12:26:47.357",
          "editDate": null
      },
      {
          "portId": -449180735,
          "branchId": "1",
          "countryId": "0",
          "portCode": "TP",
          "portName": "TANJUNG PELEPAS",
          "isActive": "1",
          "portRegion": "0",
          "createBy": "RITA_DIR",
          "editBy": "RITA_DIR",
          "createDate": "2012-12-28 15:50:01.777",
          "editDate": null
      }
  ],
  "error": null
}

export default ports.data;