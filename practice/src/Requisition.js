import './Requisition.css'
import React from 'react';
import bargeName from './vesselList';
import vesselDepartmentName from './vesselDept';
import ports from './ports';
import employeeList from './employeeList';
import orderType from './orderType';
import '@coreui/coreui/dist/css/coreui.min.css'
import {
  CButton,
  CTableRow,
  CTableHeaderCell,
  CTableDataCell,
  CFormInput,
  CSpan,
  CCol,
  CContainer,
  CFormLabel,
  CCard,
  CCardHeader,
  CCardBody,
  CForm,
  CModal,
  CModalHeader,
  CModalBody,
  CRowlapse,
  CFormGroup,
  CModalFooter,
  CInput,
  CLabel,
  CDataTable,
  CSwitch,
  CBadge,

  CInvalidFeedback,
  CFormText,
  CInputGroup,
  CHeader,
  CFormSelect,
  CNavItem,
  CTableHead,
  CTable,
  CTableBody,
  CTableFoot,
  CRow,
} from "@coreui/react";
import moment from 'moment';
import { mockComponent } from 'react-dom/test-utils';
class Requisition extends React.Component {
  constructor() {
    super()
    this.state = {
      data: vesselDepartmentName,
      vessel: bargeName,
      ports: ports,
      employeeList: employeeList,
      input:{},
      errors:{},
      requisitiondate:moment().format('YYYY-MM-DD'),
      expiryDate: moment().add(7,"days").format('YYYY-MM-DD')
    }
  }
  componentDidUpdate(){
    console.log("component updated")
  }
    handleChange=(event)=>{ 
      if(event.target.name === "reqDate"){
      let expDate=moment(event.target.value).add(7,'days').format('YYYY-MM-DD')
      console.log(expDate,"expirydate")
      let  errors=this.state.errors
      let  currentDate= moment().format('YYYY-MM-DD')
      console.log(currentDate,"current date")
      this.setState({requisitiondate:event.target.value,
      expiryDate:expDate})
        if(currentDate>event.target.value)
        {
          errors["reqDate"]="Enter a valid date"
        }
        else{
          errors["reqDate"]=""
        } 
        
      }
     if(event.target.name=="Referenceno"){

     }
    }
  render() {
    console.log(this.state.requisitiondate,"in render")
    let data = this.state.data
    let vessel = this.state.vessel
    let ports = this.state.ports
    let RequestedBy=this.state.employeeList
    return (
      <>
        <CHeader className='Header'>
          <h2>Requisition Page</h2>
        </CHeader>
        <CContainer fluid className='Div1'>
          <CForm>
              <CInputGroup className='Input-FieldsRow1'>
                <CRow><CCol><CFormLabel className='required'>Requisition Date:</CFormLabel>
                  < CFormInput type="Date" id="Requisitiondate" name="reqDate" value={this.state.requisitiondate}onChange={this.handleChange}></CFormInput>
                  <CCol><CFormText id="error">{this.state.errors.reqDate}</CFormText></CCol></CCol></CRow>
                <CRow><CCol><CFormLabel className='required'>Reference No:</CFormLabel>
                  <CFormInput type="Text" id="input"name='Referenceno'></CFormInput></CCol></CRow>
                <CRow><CCol><CFormLabel className='required'>Order Type:</CFormLabel>
                  <CFormSelect id="select">
                  {orderType.map((val, key) =>
                      <option key={key} id={val.orderTypeId}>{val.orderTypeName}</option>)}
                  </CFormSelect></CCol></CRow>
                <CRow><CCol><CFormLabel className='required'>Port:</CFormLabel>
                  <CFormSelect id="select">
                    {ports.map((val, key) =>
                    <option key={key} id={val.portId}>{val.portName}</option>)}
                    </CFormSelect></CCol></CRow>
                 
                <CRow><CCol><CFormLabel className='required'>Vessel:</CFormLabel>
                  <CFormSelect id="select">
                    {
                      vessel.map((val, key) =>
                        <option key={key} id={val.bargeId}>{val.bargeName}</option>)}</CFormSelect></CCol></CRow>
                      
                <CRow><CCol><CFormLabel className='required'>Vessel Dept :</CFormLabel>
                  <CFormSelect id="select">
                    {
                      data.map((val, key) =>
                        <option key={key}>{val.vesselDepartmentName}</option>)
                    }
                    </CFormSelect></CCol></CRow>
                    </CInputGroup >
                    <CInputGroup className='Input-FieldsRow1'>
                    <CRow><CCol><CFormLabel className='required'>Expiry Date:</CFormLabel>
                  < CFormInput type="Date" id="Expirydate" name="expDate"value={this.state.expiryDate}onChange={this.handleChange}></CFormInput></CCol></CRow>
                  <CRow><CCol><CFormLabel className='required'>Equipment:</CFormLabel>
                  <CFormSelect id="equipment">{ports.map((val, key) =>
                    <option key={key} id={val.portId}>{val.portName}</option>)}</CFormSelect></CCol></CRow>
                  <CRow><CCol><CFormLabel>Model:</CFormLabel>
                  <CFormSelect id="equipment">{ports.map((val, key) =>
                    <option key={key} id={val.portId}>{val.portName}</option>)}</CFormSelect></CCol></CRow>
                  <CRow><CCol><CFormLabel className='required'>Requested By:</CFormLabel>
                  <CFormSelect id="RequestedBy">
                  {
                      RequestedBy.map((val, key) =>
                        <option key={key} id={val.employeeId}>{val.employeeName}</option>)}</CFormSelect></CCol></CRow>
                      <CRow><CCol><CFormLabel>Remarks:</CFormLabel>
                  <CFormInput type="Text" id="Remarks"></CFormInput></CCol></CRow>
              </CInputGroup>
          </CForm>
        </CContainer>

        <CContainer className='Data-Table'>
          <CTable>
            <CTableHead>
              <CTableRow>
                <CTableHeaderCell scope="col">#</CTableHeaderCell>
                <CTableHeaderCell scope="col">Class</CTableHeaderCell>
                <CTableHeaderCell scope="col">Heading</CTableHeaderCell>
                <CTableHeaderCell scope="col">Heading</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              <CTableRow>
                <CTableHeaderCell scope="row">1</CTableHeaderCell>
                <CTableDataCell>Mark</CTableDataCell>
                <CTableDataCell>Otto</CTableDataCell>
                <CTableDataCell>@mdo</CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableHeaderCell scope="row">2</CTableHeaderCell>
                <CTableDataCell>Jacob</CTableDataCell>
                <CTableDataCell>Thornton</CTableDataCell>
                <CTableDataCell>@fat</CTableDataCell>
              </CTableRow>
              <CTableRow>
                <CTableHeaderCell scope="row">3</CTableHeaderCell>
                <CTableDataCell colSpan="2">Larry the Bird</CTableDataCell>
                <CTableDataCell>@twitter</CTableDataCell>
              </CTableRow>
            </CTableBody>
            <CTableFoot>hello</CTableFoot>
          </CTable>
        </CContainer>
        <CContainer className='divButtons'>
          
        <CRow>
                      <CCol><CButton  id="button"className='mt-2' color="success" >Save</CButton></CCol>
                      <CCol><CButton  id="button"className='mt-2' color="success" disabled>Submit</CButton></CCol>
                      <CCol><CButton  id="button"className='mt-2' color="secondary" disabled>Print</CButton></CCol>
                      <CCol><CButton  id="button"className='mt-2' color="danger">Clear</CButton></CCol>
                      </CRow>
        </CContainer>
      </>
    )
  }
}
export default Requisition;
