import React from 'react';

const employeeList = {
    "status": "OK",
    "data": [
        {
            "employeeId": "9991332014030003",
            "employeeCode": "A ATIKURRAHMAN",
            "employeeName": "A ATIKURRAHMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332014020012",
            "employeeCode": "ABDILLAH BIN MANSOR",
            "employeeName": "ABDILLAH BIN MANSOR",
            "isActive": 1
        },
        {
            "employeeId": "1332010030003",
            "employeeCode": "ABDUL ALEEM B M G",
            "employeeName": "ABDUL ALEEM BIN MOHAMED GHOUSE",
            "isActive": 1
        },
        {
            "employeeId": "1332008110031",
            "employeeCode": "ABDUL BAHRI MONY",
            "employeeName": "ABDUL BAHRI MONY",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120005",
            "employeeCode": "ABDUL KADIR SIAMA",
            "employeeName": "ABDUL KADIR SIAMA",
            "isActive": 1
        },
        {
            "employeeId": "999150720140007",
            "employeeCode": "ABDUL MAJEED",
            "employeeName": "ABDUL MAJEED BIN ABDUL HAMEED",
            "isActive": 1
        },
        {
            "employeeId": "1332009110010",
            "employeeCode": "MANAF",
            "employeeName": "ABDUL MANAF BIN JAFFAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010002",
            "employeeCode": "ABDUL MAULANA",
            "employeeName": "ABDUL MAULANA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100001",
            "employeeCode": "ABDUL MUIN LIDA",
            "employeeName": "ABDUL MUIN LIDA",
            "isActive": 1
        },
        {
            "employeeId": "1332009100002",
            "employeeCode": "ABDUL RACHMAN",
            "employeeName": "ABDUL RACHMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070045",
            "employeeCode": "ABDUL RAHMAN BIN A",
            "employeeName": "ABDUL RAHMAN BIN AWANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040005",
            "employeeCode": "ABDUL RASHID",
            "employeeName": "ABDUL RASHID BIN ABDUL GANNI",
            "isActive": 1
        },
        {
            "employeeId": "1332010040006",
            "employeeCode": "ABDUL RIJAL",
            "employeeName": "ABDUL RIJAL",
            "isActive": 1
        },
        {
            "employeeId": "1332008090028",
            "employeeCode": "ABDUL ROHIM",
            "employeeName": "ABDUL ROHIM RIDWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008100001",
            "employeeCode": "ABDUL ROUF",
            "employeeName": "ABDUL ROUF",
            "isActive": 1
        },
        {
            "employeeId": "1332012060002",
            "employeeCode": "ABDUL ZAZID USMAN",
            "employeeName": "ABDUL ZAZID USMAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040011",
            "employeeCode": "ABDULLAH",
            "employeeName": "ABDULLAH",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040024",
            "employeeCode": "ABDULMAJEED B.A.H",
            "employeeName": "ABDULMAJEED B.A.H",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060016",
            "employeeCode": "ACHMAD RIZAL FEBY",
            "employeeName": "ACHMAD RIZAL FEBY NUGROHO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080051",
            "employeeCode": "ACHMAD SAKDULLAH",
            "employeeName": "ACHMAD SAKDULLAH",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050004",
            "employeeCode": "ACHMAT FAIRUS",
            "employeeName": "ACHMAT FAIRUS",
            "isActive": 1
        },
        {
            "employeeId": "1332011050002",
            "employeeCode": "ADE MULYAN",
            "employeeName": "ADE MULYAN",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150014",
            "employeeCode": "ADE SURYANTO",
            "employeeName": "ADE SURYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999030620160004",
            "employeeCode": "ADE YULIUS MARTONO",
            "employeeName": "ADE YULIUS MARTONO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030015",
            "employeeCode": "ADI LOTULUNG",
            "employeeName": "ADI LOTULUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110010",
            "employeeCode": "ADI SISWANTO",
            "employeeName": "ADI SISWANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010110005",
            "employeeCode": "ADI TYASWORO",
            "employeeName": "ADI TYASWORO",
            "isActive": 1
        },
        {
            "employeeId": "99992016060004",
            "employeeCode": "ADI YULIUS MARTONO",
            "employeeName": "ADI YULIUS MARTONO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020008",
            "employeeCode": "ADIYOSO",
            "employeeName": "ADIYOSO",
            "isActive": 1
        },
        {
            "employeeId": "1332008110016",
            "employeeCode": "ADONG R N MARBUN",
            "employeeName": "ADONG RUSTAN NAULI MARBUN",
            "isActive": 1
        },
        {
            "employeeId": "1332014020008",
            "employeeCode": "ADUUL ZAZID",
            "employeeName": "ADUUL ZAZID",
            "isActive": 1
        },
        {
            "employeeId": "1332011040011",
            "employeeCode": "AFDHAL",
            "employeeName": "AFDHAL",
            "isActive": 1
        },
        {
            "employeeId": "1332020030001",
            "employeeCode": "AGGIE POH",
            "employeeName": "AGGIE POH",
            "isActive": 1
        },
        {
            "employeeId": "9991332014030004",
            "employeeCode": "AGUNG AMPI YANTO",
            "employeeName": "AGUNG AMPI YANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070026",
            "employeeCode": "AGUNG HARIO",
            "employeeName": "AGUNG HARIO",
            "isActive": 1
        },
        {
            "employeeId": "9999240620150008",
            "employeeCode": "AGUNG SIDIK WIYONO",
            "employeeName": "AGUNG SIDIK WIYONO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070013",
            "employeeCode": "AGUS",
            "employeeName": "AGUS",
            "isActive": 1
        },
        {
            "employeeId": "9999100920150006",
            "employeeCode": "AGUS ASIS WALUYO",
            "employeeName": "AGUS ASIS WALUYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110003",
            "employeeCode": "AGUS BUDIYANTO",
            "employeeName": "AGUS BUDIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008110021",
            "employeeCode": "AGUS DIRMAWAN",
            "employeeName": "AGUS DIRMAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120004",
            "employeeCode": "AGUS DWIANTO",
            "employeeName": "AGUS DWIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110009",
            "employeeCode": "AGUS HAFID",
            "employeeName": "AGUS HAFID",
            "isActive": 1
        },
        {
            "employeeId": "1332009110009",
            "employeeCode": "AGUS HENDRO IRIANTO",
            "employeeName": "AGUS HENDRO IRIANTO",
            "isActive": 1
        },
        {
            "employeeId": "999230420140012",
            "employeeCode": "AGUS MUFTI",
            "employeeName": "AGUS MUFTI HADAYATULAH",
            "isActive": 1
        },
        {
            "employeeId": "999220820140006",
            "employeeCode": "AGUS PRIYANTO",
            "employeeName": "AGUS PRIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999010420150002",
            "employeeCode": "AGUS PRIYONO",
            "employeeName": "AGUS PRIYONO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010005",
            "employeeCode": "AGUS SALIM",
            "employeeName": "AGUS SALIM",
            "isActive": 1
        },
        {
            "employeeId": "1332020090001",
            "employeeCode": "AGUS SETYADI",
            "employeeName": "AGUS SETYADI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090016",
            "employeeCode": "AGUS SUPRIADI",
            "employeeName": "AGUS SUPRIADI",
            "isActive": 1
        },
        {
            "employeeId": "1332009030021",
            "employeeCode": "AGUS SUPRIYANTO",
            "employeeName": "AGUS SUPRIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120010",
            "employeeCode": "AGUS SUSILO",
            "employeeName": "AGUS SUSILO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030013",
            "employeeCode": "AGUSTINUS",
            "employeeName": "AGUSTINUS",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050002",
            "employeeCode": "AHMAD ASDAR",
            "employeeName": "AHMAD ASDAR",
            "isActive": 1
        },
        {
            "employeeId": "999240420140014",
            "employeeCode": "AHMAD FAUZIE",
            "employeeName": "AHMAD FAUZIE BIN SHARIL",
            "isActive": 1
        },
        {
            "employeeId": "99992016090009",
            "employeeCode": "AHMAD SULAIMI",
            "employeeName": "AHMAD SULAIMI",
            "isActive": 1
        },
        {
            "employeeId": "1332019040002",
            "employeeCode": "AHMAD ZAKI M RAZALI",
            "employeeName": "AHMAD ZAKI M RAZALI",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010015",
            "employeeCode": "AINUR ROFIQ",
            "employeeName": "AINUR ROFIQ",
            "isActive": 1
        },
        {
            "employeeId": "1332010070036",
            "employeeCode": "AIRCON SOLA ALTURA",
            "employeeName": "AIRCON SOLA ALTURA",
            "isActive": 1
        },
        {
            "employeeId": "1332008080047",
            "employeeCode": "AJI BUDIYANTO",
            "employeeName": "AJI BUDIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999110520150001",
            "employeeCode": "AKHMAD NURDIANSYAH",
            "employeeName": "AKHMAD NURDIANSYAH",
            "isActive": 1
        },
        {
            "employeeId": "1332008080024",
            "employeeCode": "ALAN TAN",
            "employeeName": "ALAN TAN",
            "isActive": 1
        },
        {
            "employeeId": "9999190420160010",
            "employeeCode": "ALDI EKA MUNANDAR",
            "employeeName": "ALDI EKA MUNANDAR",
            "isActive": 1
        },
        {
            "employeeId": "1332010010011",
            "employeeCode": "ALDWIN NICOLAS OANES",
            "employeeName": "ALDWIN NICOLAS OANES",
            "isActive": 1
        },
        {
            "employeeId": "1332009120261",
            "employeeCode": "ALEK DEVIYANTO",
            "employeeName": "ALEK DEVIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080025",
            "employeeCode": "ALEX LEE",
            "employeeName": "ALEX LEE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110018",
            "employeeCode": "ALEXANDER EFFENDI",
            "employeeName": "ALEXANDER EFFENDI",
            "isActive": 1
        },
        {
            "employeeId": "1332010040002",
            "employeeCode": "ALEXON RYAN",
            "employeeName": "ALEXON RYAN",
            "isActive": 1
        },
        {
            "employeeId": "1332012040001",
            "employeeCode": "ALFA KHAIRULLAH ZAIN",
            "employeeName": "ALFA KHAIRULLAH ZAIN",
            "isActive": 1
        },
        {
            "employeeId": "1332012040002",
            "employeeCode": "ALFA KHAIRULLAH ZAIN",
            "employeeName": "ALFA KHAIRULLAH ZAIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017060002",
            "employeeCode": "ALFIED",
            "employeeName": "ALFIE D SILVA",
            "isActive": 1
        },
        {
            "employeeId": "1332008110022",
            "employeeCode": "ALFRET MAKAPUAS",
            "employeeName": "ALFRET MAKAPUAS",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040003",
            "employeeCode": "ALI ABADI",
            "employeeName": "ALI ABADI",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010002",
            "employeeCode": "ALI FIKRI",
            "employeeName": "ALI FIKRI",
            "isActive": 1
        },
        {
            "employeeId": "1332008070035",
            "employeeCode": "ALI IMRAN SYAM",
            "employeeName": "ALI IMRAN SYAM",
            "isActive": 1
        },
        {
            "employeeId": "1332012110001",
            "employeeCode": "ALI MUHTAR",
            "employeeName": "ALI MUHTAR",
            "isActive": 1
        },
        {
            "employeeId": "9999040520160005",
            "employeeCode": "ALI SAPUTRA",
            "employeeName": "ALI SAPUTRA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012070002",
            "employeeCode": "ALIAS UMAR",
            "employeeName": "ALIAS UMAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120007",
            "employeeCode": "ALISYA MURTI",
            "employeeName": "ALISYA MURTI PRASETYO",
            "isActive": 1
        },
        {
            "employeeId": "1332008110001",
            "employeeCode": "ALLY ALFRET",
            "employeeName": "ALLY ALFRET",
            "isActive": 1
        },
        {
            "employeeId": "1332009100001",
            "employeeCode": "ALOYSIUS RUDDY",
            "employeeName": "ALOYSIUS RUDDY",
            "isActive": 1
        },
        {
            "employeeId": "1332008080012",
            "employeeCode": "ALVIN KOH",
            "employeeName": "ALVIN KOH",
            "isActive": 1
        },
        {
            "employeeId": "1332009090017",
            "employeeCode": "AMANTE SORIANO",
            "employeeName": "AMANTE SORIANO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080001",
            "employeeCode": "AMID BIN HARON",
            "employeeName": "AMID BIN HARON",
            "isActive": 1
        },
        {
            "employeeId": "1332010030005",
            "employeeCode": "AMIN FAUZI DAMANIK",
            "employeeName": "AMIN FAUZI DAMANIK",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120013",
            "employeeCode": "AMIN ISKAK A B",
            "employeeName": "AMIN ISKAK ALIAS ABDULLAH",
            "isActive": 1
        },
        {
            "employeeId": "1332009030006",
            "employeeCode": "AMIRUDDIN",
            "employeeName": "AMIRUDDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332010100021",
            "employeeCode": "AMJITH THATTARAKKAL",
            "employeeName": "AMJITH THATTARAKKAL",
            "isActive": 1
        },
        {
            "employeeId": "1332012110003",
            "employeeCode": "AMRI SIJAYA",
            "employeeName": "AMRI SIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100022",
            "employeeCode": "AMRI WIJAYA",
            "employeeName": "AMRI WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "1332010070009",
            "employeeCode": "AN JIN TAO",
            "employeeName": "AN JIN TAO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030010",
            "employeeCode": "ANAMBAS NALA",
            "employeeName": "ANAMBAS NALA",
            "isActive": 1
        },
        {
            "employeeId": "9999280720150013",
            "employeeCode": "ANDARIAS SUMULE P.",
            "employeeName": "ANDARIAS SUMULE PAKULINGAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090008",
            "employeeCode": "ANDI ABDULLAH",
            "employeeName": "ANDI ABDULLAH SANGNGADI",
            "isActive": 1
        },
        {
            "employeeId": "1332010040007",
            "employeeCode": "ANDI ASRUL",
            "employeeName": "ANDI ASRUL",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040009",
            "employeeCode": "ANDI ATIKURAHMAN",
            "employeeName": "ANDI ATIKURAHMAN",
            "isActive": 1
        },
        {
            "employeeId": "999190320140003",
            "employeeCode": "ANDI ATIKURRAHMAN",
            "employeeName": "ANDI ATIKURRAHMAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040013",
            "employeeCode": "ANDI KRISNA PURNAWAN",
            "employeeName": "ANDI KRISNA PURNAWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009110002",
            "employeeCode": "ANDI MUHAMAD",
            "employeeName": "ANDI MUHAMAD",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010008",
            "employeeCode": "ANDI NIRWANG",
            "employeeName": "ANDI NIRWANG",
            "isActive": 1
        },
        {
            "employeeId": "999090120150007",
            "employeeCode": "ANDI SUHANDANA",
            "employeeName": "ANDI SUHANDANA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040009",
            "employeeCode": "ANDI SYAMSUL RIJAL",
            "employeeName": "ANDI SYAMSUL RIJAL",
            "isActive": 1
        },
        {
            "employeeId": "1332009040018",
            "employeeCode": "ANDI WIJAYA",
            "employeeName": "ANDI WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030001",
            "employeeCode": "ANDRE MARIO SUPARDI",
            "employeeName": "ANDRE MARIO SUPARDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120001",
            "employeeCode": "ANDREW KERK",
            "employeeName": "ANDREW KERK PEI WEI",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150009",
            "employeeCode": "ANDRI LIHARDO P.",
            "employeeName": "ANDRI LIHARDO PARTUMPUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009110004",
            "employeeCode": "ANDRIANTO",
            "employeeName": "ANDRIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070032",
            "employeeCode": "ANDRIS ARNOLD KUMAJA",
            "employeeName": "ANDRIS ARNOLD KUMAJA",
            "isActive": 1
        },
        {
            "employeeId": "1332010020005",
            "employeeCode": "ANDRY SETIAWAN",
            "employeeName": "ANDRY SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999270620160011",
            "employeeCode": "ANDY",
            "employeeName": "ANDY",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040006",
            "employeeCode": "ANDY ANG",
            "employeeName": "ANDY ANG KOK SHENG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080010",
            "employeeCode": "ANDY CHUA",
            "employeeName": "ANDY CHUA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018060003",
            "employeeCode": "ANDY KURNIAWAN",
            "employeeName": "ANDY KURNIAWAN WICAKSONO",
            "isActive": 1
        },
        {
            "employeeId": "1332009090022",
            "employeeCode": "ANG CHOON HONG",
            "employeeName": "ANG CHOON HONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332009090022",
            "employeeCode": "ANG CHOON HONG PETER",
            "employeeName": "ANG CHOON HONG PETER",
            "isActive": 1
        },
        {
            "employeeId": "9999332017110004",
            "employeeCode": "ANG KOK HAO, JACKSON",
            "employeeName": "ANG KOK HAO, JACKSON",
            "isActive": 1
        },
        {
            "employeeId": "999200920140017",
            "employeeCode": "ANG WEE CHEOK",
            "employeeName": "ANG WEE CHEOK",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040003",
            "employeeCode": "ANG ZHI SONG",
            "employeeName": "ANG ZHI SONG, BERNARD",
            "isActive": 1
        },
        {
            "employeeId": "1332008070025",
            "employeeCode": "ANGGA FAUNDY",
            "employeeName": "ANGGA FAUNDY",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080003",
            "employeeCode": "ANKA WINATA",
            "employeeName": "ANKA WINATA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110002",
            "employeeCode": "ANMON",
            "employeeName": "ANMON",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110009",
            "employeeCode": "ANOM AHMADI",
            "employeeName": "ANOM AHMADI",
            "isActive": 1
        },
        {
            "employeeId": "1332009090001",
            "employeeCode": "ANTHONY CLEMENT",
            "employeeName": "ANTHONY CLEMENT",
            "isActive": 1
        },
        {
            "employeeId": "9999260620150009",
            "employeeCode": "ANTHONY L. SILIMANG",
            "employeeName": "ANTHONY L. SILIMANG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080033",
            "employeeCode": "ANTHONY ONG",
            "employeeName": "ANTHONY ONG",
            "isActive": 1
        },
        {
            "employeeId": "9999020720150001",
            "employeeCode": "ANTHONY VILLADOS",
            "employeeName": "ANTHONY VILLADOS ATABELO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070053",
            "employeeCode": "ANTO PRASETYO",
            "employeeName": "ANTO PRASETYO",
            "isActive": 1
        },
        {
            "employeeId": "1332009100004",
            "employeeCode": "ANTONIO DENNY",
            "employeeName": "ANTONIO DENNY",
            "isActive": 1
        },
        {
            "employeeId": "1332009110013",
            "employeeCode": "ANTONIO DENNY WIJAYA",
            "employeeName": "ANTONIO DENNY WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "9999270620150009",
            "employeeCode": "ANTONY L. SILIMANG",
            "employeeName": "ANTONY L. SILIMANG",
            "isActive": 1
        },
        {
            "employeeId": "1332009120258",
            "employeeCode": "APRIL",
            "employeeName": "APRIL",
            "isActive": 1
        },
        {
            "employeeId": "9999030620160003",
            "employeeCode": "APRIZAL SOMAT WALA",
            "employeeName": "APRIZAL SOMAT WALA",
            "isActive": 1
        },
        {
            "employeeId": "1332020080019",
            "employeeCode": "AR",
            "employeeName": "ARANDA",
            "isActive": 1
        },
        {
            "employeeId": "1332010090002",
            "employeeCode": "ARCON SOLA ALTRA",
            "employeeName": "ARCON SOLA ALTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332013020007",
            "employeeCode": "ARDAN AGUSTIAN",
            "employeeName": "ARDAN AGUSTIAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120014",
            "employeeCode": "ARDENIA DIEMZ",
            "employeeName": "ARDENIA DIEMZ MARCO PARAMI",
            "isActive": 1
        },
        {
            "employeeId": "1332010060015",
            "employeeCode": "ARI NOVANDIKA",
            "employeeName": "ARI NOVANDIKA GITARUSMA",
            "isActive": 1
        },
        {
            "employeeId": "1332009120256",
            "employeeCode": "ARI PURWANTO",
            "employeeName": "ARI PURWANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010001",
            "employeeCode": "ARIF MULIADI",
            "employeeName": "ARIF MULIADI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050010",
            "employeeCode": "ARIFF B JUMADIR",
            "employeeName": "ARIFF B JUMADIR",
            "isActive": 1
        },
        {
            "employeeId": "9991332011120005",
            "employeeCode": "ARIFUDDIN GUNAWAN",
            "employeeName": "ARIFUDDIN GUNAWAN",
            "isActive": 1
        },
        {
            "employeeId": "999300620140007",
            "employeeCode": "ARIS SULAIMAN",
            "employeeName": "ARIS SULAIMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008080050",
            "employeeCode": "ARLIN DAULAT SILITON",
            "employeeName": "ARLIN DAULAT SILITONGA",
            "isActive": 1
        },
        {
            "employeeId": "1332011030007",
            "employeeCode": "ARMAN",
            "employeeName": "ARMAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090033",
            "employeeCode": "ARMAN MALLUA",
            "employeeName": "ARMAN MALLUA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120007",
            "employeeCode": "ARYO MUSLIM",
            "employeeName": "ARYO MUSLIM NURADITA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100013",
            "employeeCode": "ASEP SETIA",
            "employeeName": "ASEP SETIA",
            "isActive": 1
        },
        {
            "employeeId": "9999130620160005",
            "employeeCode": "ASRIANTO",
            "employeeName": "ASRIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999130320150001",
            "employeeCode": "ASRIO AHMAD",
            "employeeName": "ASRIO AHMAD",
            "isActive": 1
        },
        {
            "employeeId": "1332019030003",
            "employeeCode": "ASRUDDIN SYAHRUDIN",
            "employeeName": "ASRUDDIN SYAHRUDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011060007",
            "employeeCode": "ASRUL",
            "employeeName": "ASRUL",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070005",
            "employeeCode": "ASRUL UDIN PERPATIH",
            "employeeName": "ASRUL UDIN PERPATIH",
            "isActive": 1
        },
        {
            "employeeId": "1332009050002",
            "employeeCode": "ASWAD",
            "employeeName": "ASWAD",
            "isActive": 1
        },
        {
            "employeeId": "9991332013030001",
            "employeeCode": "ATAN SYAFRIN",
            "employeeName": "ATAN SYAFRIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040004",
            "employeeCode": "ATIKURRAHMAN",
            "employeeName": "ATIKURRAHMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009110007",
            "employeeCode": "AUDI MUH",
            "employeeName": "AUDI MUH",
            "isActive": 1
        },
        {
            "employeeId": "1332019030002",
            "employeeCode": "11111",
            "employeeName": "AUDITOR",
            "isActive": 1
        },
        {
            "employeeId": "1332009040010",
            "employeeCode": "AUNG AUNG",
            "employeeName": "AUNG AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009030020",
            "employeeCode": "AUNG AUNG THEIN",
            "employeeName": "AUNG AUNG THEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090014",
            "employeeCode": "AUNG CHO HTWE",
            "employeeName": "AUNG CHO HTWE",
            "isActive": 1
        },
        {
            "employeeId": "1332009110005",
            "employeeCode": "AUNG HTU HAN",
            "employeeName": "AUNG HTU HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070012",
            "employeeCode": "AUNG KHIN",
            "employeeName": "AUNG KHIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010003",
            "employeeCode": "AUNG KO KO",
            "employeeName": "AUNG KO KO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030012",
            "employeeCode": "AUNG KO MIN",
            "employeeName": "AUNG KO MIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070002",
            "employeeCode": "AUNG KYAW HEIN",
            "employeeName": "AUNG KYAW HEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011010009",
            "employeeCode": "AUNG KYAW LIN",
            "employeeName": "AUNG KYAW LIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040005",
            "employeeCode": "AUNG KYAW MIN MAUNG",
            "employeeName": "AUNG KYAW MIN MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999180620150006",
            "employeeCode": "AUNG KYAW OO",
            "employeeName": "AUNG KYAW OO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070006",
            "employeeCode": "AUNG KYI",
            "employeeName": "AUNG KYI",
            "isActive": 1
        },
        {
            "employeeId": "1332009090010",
            "employeeCode": "AUNG LIN TUN",
            "employeeName": "AUNG LIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332008100003",
            "employeeCode": "AUNG LWIN",
            "employeeName": "AUNG LWIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011050009",
            "employeeCode": "AUNG MAUNG KYAW",
            "employeeName": "AUNG MAUNG KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050005",
            "employeeCode": "AUNG MIN HTUT",
            "employeeName": "AUNG MIN HTUT",
            "isActive": 1
        },
        {
            "employeeId": "1332008110008",
            "employeeCode": "AUNG MIN SOE",
            "employeeName": "AUNG MIN SOE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110002",
            "employeeCode": "AUNG MOE AYE",
            "employeeName": "AUNG MOE AYE",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070005",
            "employeeCode": "AUNG MOE YU AYE",
            "employeeName": "AUNG MOE YU AYE",
            "isActive": 1
        },
        {
            "employeeId": "1332009030016",
            "employeeCode": "AUNG MYAT",
            "employeeName": "AUNG MYAT",
            "isActive": 1
        },
        {
            "employeeId": "1332011020007",
            "employeeCode": "AUNG MYIN OO",
            "employeeName": "AUNG MYIN OO",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120002",
            "employeeCode": "AUNG MYINT MYAT",
            "employeeName": "AUNG MYINT MYAT",
            "isActive": 1
        },
        {
            "employeeId": "9999140120160005",
            "employeeCode": "AUNG MYO",
            "employeeName": "AUNG MYO",
            "isActive": 1
        },
        {
            "employeeId": "1332010110004",
            "employeeCode": "AUNG MYO KYAW",
            "employeeName": "AUNG MYO KYAW",
            "isActive": 1
        },
        {
            "employeeId": "1332011050008",
            "employeeCode": "AUNG MYO NAING",
            "employeeName": "AUNG MYO NAING",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010019",
            "employeeCode": "AUNG MYO OO",
            "employeeName": "AUNG MYO OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030004",
            "employeeCode": "AUNG MYO WIN",
            "employeeName": "AUNG MYO WIN",
            "isActive": 1
        },
        {
            "employeeId": "9999040420160001",
            "employeeCode": "AUNG MYO ZAW",
            "employeeName": "AUNG MYO ZAW",
            "isActive": 1
        },
        {
            "employeeId": "1332011060004",
            "employeeCode": "AUNG MYO ZIN",
            "employeeName": "AUNG MYO ZIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009020001",
            "employeeCode": "AUNG NAING",
            "employeeName": "AUNG NAING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050021",
            "employeeCode": "AUNG NYEIN THU",
            "employeeName": "AUNG NYEIN THU",
            "isActive": 1
        },
        {
            "employeeId": "99992016070005",
            "employeeCode": "AUNG SAN MOE",
            "employeeName": "AUNG SAN MOE",
            "isActive": 1
        },
        {
            "employeeId": "1332010020018",
            "employeeCode": "AUNG SOE",
            "employeeName": "AUNG SOE",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100012",
            "employeeCode": "AUNG SOE MOE",
            "employeeName": "AUNG SOE MOE",
            "isActive": 1
        },
        {
            "employeeId": "999090120150003",
            "employeeCode": "AUNG THU",
            "employeeName": "AUNG THU",
            "isActive": 1
        },
        {
            "employeeId": "1332020040001",
            "employeeCode": "AUNG THU HAN",
            "employeeName": "AUNG THU HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332009110005",
            "employeeCode": "AUNG THU HAN",
            "employeeName": "AUNG THU HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050005",
            "employeeCode": "AUNG THU RAIN",
            "employeeName": "AUNG THU RAIN",
            "isActive": 1
        },
        {
            "employeeId": "9999260520150002",
            "employeeCode": "AUNG THU YA PHYO",
            "employeeName": "AUNG THU YA PHYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090007",
            "employeeCode": "AUNG THUYA SOE",
            "employeeName": "AUNG THUYA SOE",
            "isActive": 1
        },
        {
            "employeeId": "1332011050010",
            "employeeCode": "AUNG TUN KYAW",
            "employeeName": "AUNG TUN KYAW",
            "isActive": 1
        },
        {
            "employeeId": "1332009090018",
            "employeeCode": "AUNG WIN NAING",
            "employeeName": "AUNG WIN NAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020013",
            "employeeCode": "AUNG ZAW NYEIN",
            "employeeName": "AUNG ZAW NYEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332010070002",
            "employeeCode": "BACHRUN MARANG",
            "employeeName": "BACHRUN MARANG",
            "isActive": 1
        },
        {
            "employeeId": "1332010060016",
            "employeeCode": "BACHTIAR ARI",
            "employeeName": "BACHTIAR ARI WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "1332012060006",
            "employeeCode": "BAHARUDDIN",
            "employeeName": "BAHARUDDIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100005",
            "employeeCode": "BAHRUL",
            "employeeName": "BAHRUL",
            "isActive": 1
        },
        {
            "employeeId": "999181220140007",
            "employeeCode": "BAHTIAR EFENDI",
            "employeeName": "BAHTIAR EFENDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110004",
            "employeeCode": "BAI YONG FENG",
            "employeeName": "BAI YONG FENG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020023",
            "employeeCode": "BAMBANG ADI",
            "employeeName": "BAMBANG ADI PURWANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008090023",
            "employeeCode": "BAMBANG HERMANTO",
            "employeeName": "BAMBANG HERMANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008120003",
            "employeeCode": "BAMBANG MEGO WARNO",
            "employeeName": "BAMBANG MEGO WARNO",
            "isActive": 1
        },
        {
            "employeeId": "999300620140005",
            "employeeCode": "BAMBANG PRAYOGI",
            "employeeName": "BAMBANG PRAYOGI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010007",
            "employeeCode": "BAMBANG PURWOKO",
            "employeeName": "BAMBANG PURWOKO",
            "isActive": 1
        },
        {
            "employeeId": "999090120150008",
            "employeeCode": "BAMBANG RIYANTO",
            "employeeName": "BAMBANG RIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010008",
            "employeeCode": "BAMBANG SULISTYO",
            "employeeName": "BAMBANG SULISTYO KURNIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999080120160002",
            "employeeCode": "BANYAR OO",
            "employeeName": "BANYAR OO",
            "isActive": 1
        },
        {
            "employeeId": "1332010100018",
            "employeeCode": "BARHARUDDIN",
            "employeeName": "BARHARUDDIN ABDUL GANI",
            "isActive": 1
        },
        {
            "employeeId": "1332010040004",
            "employeeCode": "BASYARUDIN",
            "employeeName": "BASYARUDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070023",
            "employeeCode": "BAYU AGUSTIANTHO",
            "employeeName": "BAYU AGUSTIANTHO",
            "isActive": 1
        },
        {
            "employeeId": "1332010050010",
            "employeeCode": "BAYU ANGGA DWI",
            "employeeName": "BAYU ANGGA DWI",
            "isActive": 1
        },
        {
            "employeeId": "1332008070046",
            "employeeCode": "BENEDICT A. GOMBIO",
            "employeeName": "BENEDICT A. GOMBIO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080005",
            "employeeCode": "BENEDICT ALBINA COMB",
            "employeeName": "BENEDICT ALBINA COMBO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070002",
            "employeeCode": "BENEDICTUS ERWIN",
            "employeeName": "BENEDICTUS ERWIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090018",
            "employeeCode": "BENI RONAL",
            "employeeName": "BENI RONAL",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040002",
            "employeeCode": "BERGANIO ALEXON",
            "employeeName": "BERGANIO ALEXON RYAN MACARAEG",
            "isActive": 1
        },
        {
            "employeeId": "1332008070044",
            "employeeCode": "BERNARDO GUTIERREZ C",
            "employeeName": "BERNARDO GUTIERREZ CULIS",
            "isActive": 1
        },
        {
            "employeeId": "1332012060007",
            "employeeCode": "BERNAT M.",
            "employeeName": "BERNAT M.",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080013",
            "employeeCode": "BERNAT MARLEN BUTAR",
            "employeeName": "BERNAT MARLEN BUTAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080012",
            "employeeCode": "BIBAT FRANCIS",
            "employeeName": "BIBAT FRANCIS WALTER BENITEZ",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040005",
            "employeeCode": "BO BO HAN",
            "employeeName": "BO BO HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040029",
            "employeeCode": "BO BO KYAW",
            "employeeName": "BO BO KYAW",
            "isActive": 1
        },
        {
            "employeeId": "1332008070013",
            "employeeCode": "RICKY BOEY",
            "employeeName": "BOEY FOOK ONN, RICKY",
            "isActive": 1
        },
        {
            "employeeId": "1332008080023",
            "employeeCode": "BOO PUWEN",
            "employeeName": "BOO PUWEN",
            "isActive": 1
        },
        {
            "employeeId": "1332020060004",
            "employeeCode": "B7",
            "employeeName": "BOONTEK 7",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010011",
            "employeeCode": "BORHANUDDIN B M",
            "employeeName": "BORHANUDDIN BIN MAHMUD",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020008",
            "employeeCode": "BOY INDRA PUTRA",
            "employeeName": "BOY INDRA PUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332012100005",
            "employeeCode": "BRAMTON CELLY",
            "employeeName": "BRAMTON CELLY",
            "isActive": 1
        },
        {
            "employeeId": "1332008090019",
            "employeeCode": "BUDI BUDIMAN",
            "employeeName": "BUDI BUDIMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070020",
            "employeeCode": "BUDIONO",
            "employeeName": "BUDIONO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070056",
            "employeeCode": "BURHANUDDIN A. GAIN",
            "employeeName": "BURHANUDDIN A. GAIN",
            "isActive": 1
        },
        {
            "employeeId": "1332012020005",
            "employeeCode": "C/E-ILHAM ACHMAD",
            "employeeName": "C/E-ILHAM ACHMAD",
            "isActive": 1
        },
        {
            "employeeId": "1332012020006",
            "employeeCode": "C/O HERI PURWANTO",
            "employeeName": "C/O HERI PURWANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010070001",
            "employeeCode": "CAIGOY.J.CATIDING",
            "employeeName": "CAIGOY.J.CATIDING",
            "isActive": 1
        },
        {
            "employeeId": "999020120150002",
            "employeeCode": "CAMERO LEONARD",
            "employeeName": "CAMERO LEONARD UBUNGEN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100004",
            "employeeCode": "CANRA NAPITU",
            "employeeName": "CANRA NAPITU",
            "isActive": 1
        },
        {
            "employeeId": "1332008120008",
            "employeeCode": "CECEP DADANG KUSNADY",
            "employeeName": "CECEP DADANG KUSNADY",
            "isActive": 1
        },
        {
            "employeeId": "1332008090027",
            "employeeCode": "CECILIO A. TAMUNDONG",
            "employeeName": "CECILIO ALBINA TAMUNDONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010012",
            "employeeCode": "CECILIOALBINA",
            "employeeName": "CECILIOALBINA TAMUNDONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030009",
            "employeeCode": "CELLY BRAMTON",
            "employeeName": "CELLY BRAMTON",
            "isActive": 1
        },
        {
            "employeeId": "1332019040001",
            "employeeCode": "DOREEN CHAN",
            "employeeName": "CHAN KHENG KIOW, DOREEN",
            "isActive": 1
        },
        {
            "employeeId": "9999250620160009",
            "employeeCode": "CHAN KOK LEONG",
            "employeeName": "CHAN KOK LEONG",
            "isActive": 1
        },
        {
            "employeeId": "9999060120160001",
            "employeeCode": "DAMIEN CHAN",
            "employeeName": "CHAN KOK LEONG, DAMIEN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050011",
            "employeeCode": "CHAN LI LEONG",
            "employeeName": "CHAN LI LEONG",
            "isActive": 1
        },
        {
            "employeeId": "1332010060018",
            "employeeCode": "CHANDRA SANJAYA",
            "employeeName": "CHANDRA SANJAYA",
            "isActive": 1
        },
        {
            "employeeId": "1332008080056",
            "employeeCode": "CHARLES HUTAPEA",
            "employeeName": "CHARLES HUTAPEA",
            "isActive": 1
        },
        {
            "employeeId": "9999120120160003",
            "employeeCode": "CHEN GUANG ZHONG",
            "employeeName": "CHEN GUANG ZHONG",
            "isActive": 1
        },
        {
            "employeeId": "9999211120150006",
            "employeeCode": "BEN CHEN",
            "employeeName": "CHEN GUANG ZHONG, BEN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110014",
            "employeeCode": "CHEN KANGPENG",
            "employeeName": "CHEN KANGPENG",
            "isActive": 1
        },
        {
            "employeeId": "1332010070023",
            "employeeCode": "CHEN LONG",
            "employeeName": "CHEN LONG",
            "isActive": 1
        },
        {
            "employeeId": "1332010070024",
            "employeeCode": "CHEN ZE HUA",
            "employeeName": "CHEN ZE HUA",
            "isActive": 1
        },
        {
            "employeeId": "1332014070006",
            "employeeCode": "CHEW CHOONG KHEONG",
            "employeeName": "CHEW CHOONG KHEONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120002",
            "employeeCode": "CHEW JUNJIE",
            "employeeName": "CHEW JUNJIE",
            "isActive": 1
        },
        {
            "employeeId": "1332010020020",
            "employeeCode": "CHIAM HENG FONG",
            "employeeName": "CHIAM HENG FONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009010002",
            "employeeCode": "CHIT KO KO AUNG",
            "employeeName": "CHIT KO KO AUNG",
            "isActive": 1
        },
        {
            "employeeId": "999200920140018",
            "employeeCode": "CHNG HAK TECK",
            "employeeName": "CHNG HAK TECK, JACKSON",
            "isActive": 1
        },
        {
            "employeeId": "9991332012090003",
            "employeeCode": "CHOIRUL ROZIQIN",
            "employeeName": "CHOIRUL ROZIQIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100007",
            "employeeCode": "ANNA CHONG",
            "employeeName": "CHONG YEE NA, ANNA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070008",
            "employeeCode": "CHOO YI SIANG",
            "employeeName": "CHOO YI SIANG, ISABELLE",
            "isActive": 1
        },
        {
            "employeeId": "1332009060019",
            "employeeCode": "CHRISBIYANTO RURU",
            "employeeName": "CHRISBIYANTO RURU",
            "isActive": 1
        },
        {
            "employeeId": "1332008090007",
            "employeeCode": "CHRISTIAN M.MANDY",
            "employeeName": "CHRISTIAN M.MANDY",
            "isActive": 1
        },
        {
            "employeeId": "1332008070043",
            "employeeCode": "CHRISTIAN MARCIANO M",
            "employeeName": "CHRISTIAN MARCIANO MANOY",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100006",
            "employeeCode": "CHRISTOFEL",
            "employeeName": "CHRISTOFEL",
            "isActive": 1
        },
        {
            "employeeId": "99992016090013",
            "employeeCode": "CHRISTOPHER F. R.",
            "employeeName": "CHRISTOPHER FREYBI RUMUAT",
            "isActive": 1
        },
        {
            "employeeId": "1332011010006",
            "employeeCode": "CHU JING HWA",
            "employeeName": "CHU JING HWA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090011",
            "employeeCode": "CHU THIAM KIAT",
            "employeeName": "CHU THIAM KIAT",
            "isActive": 1
        },
        {
            "employeeId": "9991332013030002",
            "employeeCode": "WILSON CHU",
            "employeeName": "CHU THIAM KIAT, WILSON",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040011",
            "employeeCode": "CHUA CHIN HOW",
            "employeeName": "CHUA CHIN HOW",
            "isActive": 1
        },
        {
            "employeeId": "1332020040003",
            "employeeCode": "DARREN CHUA",
            "employeeName": "CHUA JUN NIAN, DARREN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070009",
            "employeeCode": "CHUA WAH GUAN",
            "employeeName": "CHUA WAH GUAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332009070009",
            "employeeCode": "ANDY CHUA WAH GUAN",
            "employeeName": "CHUA WAH GUAN, ANDY",
            "isActive": 1
        },
        {
            "employeeId": "1332008070007",
            "employeeCode": "ZZ CHUA",
            "employeeName": "CHUA ZONG ZHI, ALWYN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012050011",
            "employeeCode": "CHUANG KIANAIK PETER",
            "employeeName": "CHUANG KIAN AIK PETER",
            "isActive": 1
        },
        {
            "employeeId": "1332020080007",
            "employeeCode": "C5",
            "employeeName": "CHWNTEK 5",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050016",
            "employeeCode": "CONTAWE ARNEL",
            "employeeName": "CONTAWE ARNEL CASTRO",
            "isActive": 1
        },
        {
            "employeeId": "9999040720160002",
            "employeeCode": "DACUYCUY EMMANUEL",
            "employeeName": "DACUYCUY EMMANUEL JR CALIBO",
            "isActive": 1
        },
        {
            "employeeId": "1332012100001",
            "employeeCode": "DADAN",
            "employeeName": "DADAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020019",
            "employeeCode": "DADAN NOOR ARIEF",
            "employeeName": "DADAN NOOR ARIEF",
            "isActive": 1
        },
        {
            "employeeId": "1332019080002",
            "employeeCode": "DADANG KURNIADI",
            "employeeName": "DADANG KURNIADI",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080024",
            "employeeCode": "DADI SUPRIADI",
            "employeeName": "DADI SUPRIADI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040012",
            "employeeCode": "DAHONO",
            "employeeName": "DAHONO",
            "isActive": 1
        },
        {
            "employeeId": "1332010070022",
            "employeeCode": "DAI SHENG",
            "employeeName": "DAI SHENG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012050005",
            "employeeCode": "DAMRI KRISTAL",
            "employeeName": "DAMRI KRISTAL MANGLOLO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110010",
            "employeeCode": "DANANG AGUNG",
            "employeeName": "DANANG AGUNG SAPUTRO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070033",
            "employeeCode": "DANANG PRAMUSUSETYA",
            "employeeName": "DANANG PRAMUSUSETYA",
            "isActive": 1
        },
        {
            "employeeId": "9999240720150008",
            "employeeCode": "DANI MARDANI",
            "employeeName": "DANI MARDANI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080022",
            "employeeCode": "DANIEL WOON",
            "employeeName": "DANIEL WOON",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010009",
            "employeeCode": "DANIL AL GHAZALI",
            "employeeName": "DANIL AL GHAZALI",
            "isActive": 1
        },
        {
            "employeeId": "1332010100010",
            "employeeCode": "DANTE MATEO",
            "employeeName": "DANTE MATEO",
            "isActive": 1
        },
        {
            "employeeId": "1332009040012",
            "employeeCode": "DAPID RIKARDO",
            "employeeName": "DAPID RIKARO",
            "isActive": 1
        },
        {
            "employeeId": "999290520140006",
            "employeeCode": "DARWIN RAJAGUKGUK",
            "employeeName": "DARWIN RAJAGUKGUK",
            "isActive": 1
        },
        {
            "employeeId": "1332013020008",
            "employeeCode": "DASA RIVAND",
            "employeeName": "DASA RIVAND",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020020",
            "employeeCode": "DAUID RATNOSANTOSO",
            "employeeName": "DAUID RATNOSANTOSO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020024",
            "employeeCode": "DAVID RATNO SANTOSO",
            "employeeName": "DAVID RATNO SANTOSO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080029",
            "employeeCode": "DAVIS WONG",
            "employeeName": "DAVIS WONG",
            "isActive": 1
        },
        {
            "employeeId": "1332008120006",
            "employeeCode": "DECEP DADANG KUSNADY",
            "employeeName": "DECEP DADANG KUSNADY",
            "isActive": 1
        },
        {
            "employeeId": "1332008120007",
            "employeeCode": "DECEP DADANG KUSNADY",
            "employeeName": "DECEP DADANG KUSNADY",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120007",
            "employeeCode": "DEDY JUNAIDI",
            "employeeName": "DEDY JUNAIDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008120015",
            "employeeCode": "DEKKI L. RUMAMBI",
            "employeeName": "DEKKI LENGKONG RUMAMBI",
            "isActive": 1
        },
        {
            "employeeId": "1332008070021",
            "employeeCode": "JOEY",
            "employeeName": "DELA PENA JOEY M.",
            "isActive": 1
        },
        {
            "employeeId": "1332008070039",
            "employeeCode": "DELFIS TIMBANGNUSA",
            "employeeName": "DELFIS TIMBANGNUSA",
            "isActive": 1
        },
        {
            "employeeId": "1332008070024",
            "employeeCode": "DENDY SUDI SUNARSO",
            "employeeName": "DENDY SUDI SUNARSO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010004",
            "employeeCode": "DENJIE PUTRA HANDY",
            "employeeName": "DENJIE PUTRA HANDY WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070010",
            "employeeCode": "DENNY ETWIN RIMPOROK",
            "employeeName": "DENNY ETWIN RIMPOROK",
            "isActive": 1
        },
        {
            "employeeId": "1332008080055",
            "employeeCode": "DENNY LARENGGAM",
            "employeeName": "DENNY LARENGGAM",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100014",
            "employeeCode": "DENVER MATIAS",
            "employeeName": "DENVER MATIAS BULAONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100015",
            "employeeCode": "DENVER MATIAS",
            "employeeName": "DENVER MATIAS BULAONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010011",
            "employeeCode": "DEVIS OKTAMA",
            "employeeName": "DEVIS OKTAMA",
            "isActive": 1
        },
        {
            "employeeId": "9999100920150003",
            "employeeCode": "DHANI ARIF RAKHMANTO",
            "employeeName": "DHANI ARIF RAKHMANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999270620150011",
            "employeeCode": "DIAN WIDIARTO",
            "employeeName": "DIAN WIDIARTO",
            "isActive": 1
        },
        {
            "employeeId": "99992016070017",
            "employeeCode": "DIAN WIRANATA S.",
            "employeeName": "DIAN WIRANATA SIREGAR",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020007",
            "employeeCode": "DICKY GUNAWAN",
            "employeeName": "DICKY GUNAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999100920150004",
            "employeeCode": "DIDIK WAHYUDI",
            "employeeName": "DIDIK WAHYUDI",
            "isActive": 1
        },
        {
            "employeeId": "1332010010010",
            "employeeCode": "DIDIT AGUNG",
            "employeeName": "DIDIT AGUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080003",
            "employeeCode": "DIMAS SETIAWAN",
            "employeeName": "DIMAS SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090014",
            "employeeCode": "DIOS AGUSTINUS",
            "employeeName": "DIOS AGUSTINUS BARAHAMA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100018",
            "employeeCode": "DIPA UTAMA",
            "employeeName": "DIPA UTAMA",
            "isActive": 1
        },
        {
            "employeeId": "1332010020010",
            "employeeCode": "JONIMAR ORTIZ",
            "employeeName": "DIZON JONIMAR ORTIZ",
            "isActive": 1
        },
        {
            "employeeId": "1332010030019",
            "employeeCode": "DIZON JONIMAR ORTIZ",
            "employeeName": "DIZON JONIMAR ORTIZ",
            "isActive": 1
        },
        {
            "employeeId": "1332008100021",
            "employeeCode": "DJOKO WINARNO",
            "employeeName": "DJOKO WINARNO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120008",
            "employeeCode": "DJONLY DAWID",
            "employeeName": "DJONLY DAWID",
            "isActive": 1
        },
        {
            "employeeId": "1332008070041",
            "employeeCode": "DJUFRI ABDUL RAHMAN",
            "employeeName": "DJUFRI ABDUL RAHMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010100011",
            "employeeCode": "DONG HANZHAO",
            "employeeName": "DONG HANZHAO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013020005",
            "employeeCode": "DONG ZHIYONG",
            "employeeName": "DONG ZHIYONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060010",
            "employeeCode": "DU YUAN",
            "employeeName": "DU YUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332020080014",
            "employeeCode": "D8",
            "employeeName": "DUANGTALAY 8",
            "isActive": 1
        },
        {
            "employeeId": "1332008080007",
            "employeeCode": "DWI AGUS HARIYANTO",
            "employeeName": "DWI AGUS HARIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009080004",
            "employeeCode": "DWI ARIFIANTO",
            "employeeName": "DWI ARIFIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009060014",
            "employeeCode": "DWIGHT VALDEZ ASADA",
            "employeeName": "DWIGHT VALDEZ ASADA",
            "isActive": 1
        },
        {
            "employeeId": "1332011050014",
            "employeeCode": "EDDY SUPYANDI",
            "employeeName": "EDDY SUPYANDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120015",
            "employeeCode": "EDGAR BUDZAERY",
            "employeeName": "EDGAR BUDZAERY",
            "isActive": 1
        },
        {
            "employeeId": "1332013020001",
            "employeeCode": "EDGAR BUDZERY",
            "employeeName": "EDGAR BUDZERY",
            "isActive": 1
        },
        {
            "employeeId": "1332013020002",
            "employeeCode": "EDGAR",
            "employeeName": "EDGAR BUDZERY",
            "isActive": 1
        },
        {
            "employeeId": "1332009050012",
            "employeeCode": "EDI HARTONO",
            "employeeName": "EDI HARTONO",
            "isActive": 1
        },
        {
            "employeeId": "9999230320150006",
            "employeeCode": "EDI LUGITO SUMARDI",
            "employeeName": "EDI LUGITO SUMARDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008100023",
            "employeeCode": "EDI PRIYANTO",
            "employeeName": "EDI PRIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008090003",
            "employeeCode": "EDI SIGAR",
            "employeeName": "EDI SIGAR",
            "isActive": 1
        },
        {
            "employeeId": "9999030220150002",
            "employeeCode": "EDISON",
            "employeeName": "EDISON",
            "isActive": 1
        },
        {
            "employeeId": "1332008080013",
            "employeeCode": "EDMUND PHUA",
            "employeeName": "EDMUND PHUA",
            "isActive": 1
        },
        {
            "employeeId": "1332008100010",
            "employeeCode": "EDWAR MALIMBU",
            "employeeName": "EDWAR MALIMBU",
            "isActive": 1
        },
        {
            "employeeId": "1332009120276",
            "employeeCode": "EDWIN CHAVEZ GAMUSO",
            "employeeName": "EDWIN CHAVEZ GAMUSO",
            "isActive": 1
        },
        {
            "employeeId": "9999060420160004",
            "employeeCode": "KELLY EE",
            "employeeName": "EE WAN NI, KELLY",
            "isActive": 1
        },
        {
            "employeeId": "1332010020006",
            "employeeCode": "EGI SUGIAWIHARJO",
            "employeeName": "EGI SUGIAWIHARJO",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040002",
            "employeeCode": "EKBER TAHULENDING",
            "employeeName": "EKBER TAHULENDING",
            "isActive": 1
        },
        {
            "employeeId": "1332008070042",
            "employeeCode": "EKO SEPTIANTO",
            "employeeName": "EKO SEPTIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999020220160002",
            "employeeCode": "EKO SETYO BUDI",
            "employeeName": "EKO SETYO BUDI",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020010",
            "employeeCode": "EKO SETYO KUNCORO",
            "employeeName": "EKO SETYO KUNCORO",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150013",
            "employeeCode": "EKO WIJOYO",
            "employeeName": "EKO WIJOYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011120007",
            "employeeCode": "EKO WINARTO",
            "employeeName": "EKO WINARTO",
            "isActive": 1
        },
        {
            "employeeId": "9999070920150001",
            "employeeCode": "EKO WIYOKO",
            "employeeName": "EKO WIYOKO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011120002",
            "employeeCode": "ELAN",
            "employeeName": "ELAN",
            "isActive": 1
        },
        {
            "employeeId": "1332011020012",
            "employeeCode": "ELANG KHALID AL",
            "employeeName": "ELANG KHALID AL BATHSI",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070006",
            "employeeCode": "ELIAS",
            "employeeName": "ELIAS",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120005",
            "employeeCode": "ELLY AL ABASIYAH",
            "employeeName": "ELLY AL ABASIYAH",
            "isActive": 1
        },
        {
            "employeeId": "9999230320150007",
            "employeeCode": "ELSON PASAE",
            "employeeName": "ELSON PASAE",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080007",
            "employeeCode": "EMANUEL YUNI SANTOSO",
            "employeeName": "EMANUEL YUNI SANTOSO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060015",
            "employeeCode": "EMILIANO HEMENTERA",
            "employeeName": "EMILIANO HEMENTERA",
            "isActive": 1
        },
        {
            "employeeId": "9999120520160015",
            "employeeCode": "EMMANUEL JR",
            "employeeName": "EMMANUEL JR CALIBO DACUYCUY",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090026",
            "employeeCode": "ENDI SETIAWAN",
            "employeeName": "ENDI SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332012030006",
            "employeeCode": "ENDRO WIBAWANTO",
            "employeeName": "ENDRO WIBAWANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010020009",
            "employeeCode": "ERIC SARANZA SENILLO",
            "employeeName": "ERIC SARANZA SENILLO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030009",
            "employeeCode": "ERIC SENILLO",
            "employeeName": "ERIC SENILLO",
            "isActive": 1
        },
        {
            "employeeId": "1332012110002",
            "employeeCode": "ERIYANTO",
            "employeeName": "ERIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110001",
            "employeeCode": "ERWIN SYAPUTRA",
            "employeeName": "ERWIN SYAPUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332009100006",
            "employeeCode": "EVAN BELTRAN",
            "employeeName": "EVAN BELTRAN",
            "isActive": 1
        },
        {
            "employeeId": "999121120140002",
            "employeeCode": "EVANGELISTA PATRICK",
            "employeeName": "EVANGELISTA PATRICK JOSE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080011",
            "employeeCode": "FABROS RENATO BANAGA",
            "employeeName": "FABROS RENATO BANAGA",
            "isActive": 1
        },
        {
            "employeeId": "9999280720150015",
            "employeeCode": "FADHLY PAPUTUNGAN",
            "employeeName": "FADHLY PAPUTUNGAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120003",
            "employeeCode": "FADJAR SETIYADI",
            "employeeName": "FADJAR SETIYADI",
            "isActive": 1
        },
        {
            "employeeId": "9999332017110006",
            "employeeCode": "FAISAL",
            "employeeName": "FAISAL",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010005",
            "employeeCode": "FAJAR APRILIS",
            "employeeName": "FAJAR APRILIS",
            "isActive": 1
        },
        {
            "employeeId": "9999040520160004",
            "employeeCode": "FAJAR ARITONANG",
            "employeeName": "FAJAR ARITONANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070002",
            "employeeCode": "FAN JIN",
            "employeeName": "FAN JIN",
            "isActive": 1
        },
        {
            "employeeId": "99992016070006",
            "employeeCode": "FANDY QUAD",
            "employeeName": "FANDY QUAD",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030012",
            "employeeCode": "FANI FELANI",
            "employeeName": "FANI FELANI",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020012",
            "employeeCode": "FANZI",
            "employeeName": "FANZI",
            "isActive": 1
        },
        {
            "employeeId": "1332011060003",
            "employeeCode": "FARIZAL",
            "employeeName": "FARIZAL",
            "isActive": 1
        },
        {
            "employeeId": "9999230320150008",
            "employeeCode": "FATHUR ROZI",
            "employeeName": "FATHUR ROZI",
            "isActive": 1
        },
        {
            "employeeId": "1332010020001",
            "employeeCode": "FAUSTINO / JEFFERSON",
            "employeeName": "FAUSTINO / JEFFERSON",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030009",
            "employeeCode": "FAUZI",
            "employeeName": "FAUZI",
            "isActive": 1
        },
        {
            "employeeId": "1332009100008",
            "employeeCode": "FAUZI ALBAAR",
            "employeeName": "FAUZI ALBAAR",
            "isActive": 1
        },
        {
            "employeeId": "9999071220150003",
            "employeeCode": "FEBRIANTO PETER L.",
            "employeeName": "FEBRIANTO PETER LATANNA",
            "isActive": 1
        },
        {
            "employeeId": "9999190420160009",
            "employeeCode": "FEBRIANTO R. T.",
            "employeeName": "FEBRIANTO ROOSEVELT TAHULENDING",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090039",
            "employeeCode": "FEBRIANTO TAHU",
            "employeeName": "FEBRIANTO TAHULENDING",
            "isActive": 1
        },
        {
            "employeeId": "1332009020003",
            "employeeCode": "FEKKY ROMPIS",
            "employeeName": "FEKKY ROMPIS",
            "isActive": 1
        },
        {
            "employeeId": "1332009100007",
            "employeeCode": "FELIX NG",
            "employeeName": "FELIX NG WOON HWEE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070011",
            "employeeCode": "FELIX SUMURUNG",
            "employeeName": "FELIX SUMURUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008070055",
            "employeeCode": "FELIXS KAREL P.",
            "employeeName": "FELIXS KAREL P.",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030008",
            "employeeCode": "FELIXS KAREL",
            "employeeName": "FELIXS KAREL PONTENGKO",
            "isActive": 1
        },
        {
            "employeeId": "9999332008070055",
            "employeeCode": "FELXIS KAREL P",
            "employeeName": "FELXIS KAREL PONGTENGKO",
            "isActive": 1
        },
        {
            "employeeId": "9999280720150012",
            "employeeCode": "FENDI GUNAWAN",
            "employeeName": "FENDI GUNAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050004",
            "employeeCode": "FENDIZA",
            "employeeName": "FENDIZA",
            "isActive": 1
        },
        {
            "employeeId": "9999070420160006",
            "employeeCode": "FENDIZA MOLRI",
            "employeeName": "FENDIZA MOLRI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080060",
            "employeeCode": "FERI RAMOS",
            "employeeName": "FERI RAMOS",
            "isActive": 1
        },
        {
            "employeeId": "1332008110020",
            "employeeCode": "FERIE FIRGIANTO",
            "employeeName": "FERIE FIRGIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009080002",
            "employeeCode": "FERRY BUSRI",
            "employeeName": "FERRY BUSRI",
            "isActive": 1
        },
        {
            "employeeId": "9999332017010002",
            "employeeCode": "FIRDAUS BIN SABTU",
            "employeeName": "FIRDAUS BIN SABTU",
            "isActive": 1
        },
        {
            "employeeId": "1332010060014",
            "employeeCode": "FIRDHAUS ADNAN",
            "employeeName": "FIRDHAUS ADNAN ASSANI",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010008",
            "employeeCode": "FIRMAN",
            "employeeName": "FIRMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070038",
            "employeeCode": "FITRAH ADI SAPUTRA",
            "employeeName": "FITRAH ADI SAPUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332008070054",
            "employeeCode": "FLEIXS KAREL P.",
            "employeeName": "FLEIXS KAREL P.",
            "isActive": 1
        },
        {
            "employeeId": "1332010040005",
            "employeeCode": "FOO CHEE MUI",
            "employeeName": "FOO CHEE  MUI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120010",
            "employeeCode": "FOO SEI QUAN",
            "employeeName": "FOO SEI QUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008120002",
            "employeeCode": "FOTRAH ADI SAPUTRA",
            "employeeName": "FOTRAH ADI SAPUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332010100014",
            "employeeCode": "FRANGKY FERRY GERUNG",
            "employeeName": "FRANGKY FERRY GERUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013030005",
            "employeeCode": "FREDDY PANGGESO",
            "employeeName": "FREDDY PANGGESO",
            "isActive": 1
        },
        {
            "employeeId": "9999130320150002",
            "employeeCode": "FREDRYK ANDREW",
            "employeeName": "FREDRYK ANDREW MANEMBU",
            "isActive": 1
        },
        {
            "employeeId": "1332008100011",
            "employeeCode": "FRIDOLIN SALAKPARANG",
            "employeeName": "FRIDOLIN SALAKPARANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100006",
            "employeeCode": "FRITRAH ADI SAPUTRA",
            "employeeName": "FRITRAH ADI SAPUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332020080006",
            "employeeCode": "FRONTEK",
            "employeeName": "FRONTEK",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110008",
            "employeeCode": "GAFFUD APRIL",
            "employeeName": "GAFFUD PARIL JOSEPH DELEON",
            "isActive": 1
        },
        {
            "employeeId": "1332018090001",
            "employeeCode": "GANESH KUMAR",
            "employeeName": "GANESH KUMAR S/O RAMASAMY",
            "isActive": 1
        },
        {
            "employeeId": "1332009110012",
            "employeeCode": "GEORGE ROGER PEPIN",
            "employeeName": "GEORGE ROGER PEPIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090003",
            "employeeCode": "GEORGE SUITELA",
            "employeeName": "GEORGE SUITELA",
            "isActive": 1
        },
        {
            "employeeId": "1332010070006",
            "employeeCode": "GERARDO AMANTE",
            "employeeName": "GERARDO AMANTE SORIANO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012090008",
            "employeeCode": "GERRY NG",
            "employeeName": "GERRY NG KOK WEE",
            "isActive": 1
        },
        {
            "employeeId": "1332011040007",
            "employeeCode": "GIYANTO",
            "employeeName": "GIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009050008",
            "employeeCode": "GLENN PAGDILAO S",
            "employeeName": "GLENN PAGDILAO S",
            "isActive": 1
        },
        {
            "employeeId": "1332010100009",
            "employeeCode": "GLORI APRIANTO",
            "employeeName": "GLORI APRIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010020021",
            "employeeCode": "GOH CHEE HIONG",
            "employeeName": "GOH CHEE HIONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110005",
            "employeeCode": "GOH SEE YAN , SEAN",
            "employeeName": "GOH SEE YAN , SEAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010020022",
            "employeeCode": "GOH SIOW LONG",
            "employeeName": "GOH SIOW LONG",
            "isActive": 1
        },
        {
            "employeeId": "999151020140004",
            "employeeCode": "GOH TEE MEM",
            "employeeName": "GOH TEE MEM",
            "isActive": 1
        },
        {
            "employeeId": "999171220140004",
            "employeeCode": "GOH WEE CHUAN",
            "employeeName": "GOH WEE CHUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070049",
            "employeeCode": "GOMAL SITOMPULUZIEL",
            "employeeName": "GOMAL SITOMPULUZIEL",
            "isActive": 1
        },
        {
            "employeeId": "9999020720150005",
            "employeeCode": "GOMBIO BENEDICT",
            "employeeName": "GOMBIO BENEDICT ALBINA",
            "isActive": 1
        },
        {
            "employeeId": "1332014020009",
            "employeeCode": "GUGUN GUNAWAN",
            "employeeName": "GUGUN GUNAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090005",
            "employeeCode": "GUM SAN AUNG",
            "employeeName": "GUM SAN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050009",
            "employeeCode": "GUNASAKARAN",
            "employeeName": "GUNASAKARAN S/O RAJANDERA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080005",
            "employeeCode": "GUNATRIES",
            "employeeName": "GUNATRIES",
            "isActive": 1
        },
        {
            "employeeId": "99992016090007",
            "employeeCode": "GUNAWAN",
            "employeeName": "GUNAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999191220150011",
            "employeeCode": "GUNAWAN W. N.",
            "employeeName": "GUNAWAN WIBISONO NAPIYUPULU",
            "isActive": 1
        },
        {
            "employeeId": "99992016080001",
            "employeeCode": "GUO XIONG",
            "employeeName": "GUO XIONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060009",
            "employeeCode": "GURSEWAK SINGH GILL",
            "employeeName": "GURSEWAK SINGH GILL S/O GURCHARAN SINGH",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120009",
            "employeeCode": "GUSTANG BIN LA PADU",
            "employeeName": "GUSTANG BIN LA PADU",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080019",
            "employeeCode": "HABIB NOOR YUSOFF",
            "employeeName": "HABIB NOOR S/O YUSOFF",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100005",
            "employeeCode": "HAIRI B. NOOR AZMAN",
            "employeeName": "HAIRI BIN NOOR AZMAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080002",
            "employeeCode": "HAM MARTHEN RAMBA",
            "employeeName": "HAM MARTHEN RAMBA",
            "isActive": 1
        },
        {
            "employeeId": "1332009110003",
            "employeeCode": "HAMSAH",
            "employeeName": "HAMSAH",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010007",
            "employeeCode": "HAN MIN PHYO",
            "employeeName": "HAN MIN PHYO",
            "isActive": 1
        },
        {
            "employeeId": "1332014020013",
            "employeeCode": "HAN WIN AUNG",
            "employeeName": "HAN WIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332020060002",
            "employeeCode": "H3",
            "employeeName": "HANTEK 3",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080018",
            "employeeCode": "HARBIB NORR YUSOFF",
            "employeeName": "HARBIB NORR S/O YUSOFF",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090002",
            "employeeCode": "HARI FIRMANSYAH",
            "employeeName": "HARI FIRMANSYAH",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090012",
            "employeeCode": "HARI ISWANDI",
            "employeeName": "HARI ISWANDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090015",
            "employeeCode": "HARI NASUTION",
            "employeeName": "HARI ISWANDI NASUTION",
            "isActive": 1
        },
        {
            "employeeId": "1332012020003",
            "employeeCode": "HARIISWANDI NASUTON",
            "employeeName": "HARI ISWANDI NASUTON (C/O)",
            "isActive": 1
        },
        {
            "employeeId": "1332012020004",
            "employeeCode": "HARI ISWANDI NASUTON",
            "employeeName": "HARI ISWANDI NASUTON (C/O)",
            "isActive": 1
        },
        {
            "employeeId": "9991332013020003",
            "employeeCode": "HARIANTO",
            "employeeName": "HARIANTO",
            "isActive": 1
        },
        {
            "employeeId": "999100520140001",
            "employeeCode": "HARTONO",
            "employeeName": "HARTONO",
            "isActive": 1
        },
        {
            "employeeId": "9999311020150004",
            "employeeCode": "HARYANTO YASIR",
            "employeeName": "HARYANTO YASIR",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150011",
            "employeeCode": "HARYOKO",
            "employeeName": "HARYOKO",
            "isActive": 1
        },
        {
            "employeeId": "1332008100007",
            "employeeCode": "HASTO BUDOYO",
            "employeeName": "HASTO BUDOYO",
            "isActive": 1
        },
        {
            "employeeId": "1332009040005",
            "employeeCode": "HASUDUNGAN",
            "employeeName": "HASUDUNGAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008120019",
            "employeeCode": "HASUDUNGAN S.",
            "employeeName": "HASUDUNGAN S.",
            "isActive": 1
        },
        {
            "employeeId": "1332009010009",
            "employeeCode": "HASUDUNGAN SIM",
            "employeeName": "HASUDUNGAN SIMATUPANG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080012",
            "employeeCode": "HEIDIR BIN MOHAMAD",
            "employeeName": "HEIDIR BIN MOHAMAD HASSIM",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110003",
            "employeeCode": "HEIMANSPIT",
            "employeeName": "HEIMANSPIT",
            "isActive": 1
        },
        {
            "employeeId": "1332009070023",
            "employeeCode": "HEIN MYAT THU",
            "employeeName": "HEIN MYAT THU",
            "isActive": 1
        },
        {
            "employeeId": "1332009070013",
            "employeeCode": "HENDRA SAPUTRA",
            "employeeName": "HENDRA SAPUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332008100017",
            "employeeCode": "HENDREK",
            "employeeName": "HENDREK",
            "isActive": 1
        },
        {
            "employeeId": "9991332014020005",
            "employeeCode": "HENDRI PERWANTO",
            "employeeName": "HENDRI PERWANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999100920150005",
            "employeeCode": "HENDRI SETIAWAN",
            "employeeName": "HENDRI SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100002",
            "employeeCode": "HENDRI YASANDRA",
            "employeeName": "HENDRI YASANDRA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070004",
            "employeeCode": "HENDRIK GUSTIAWAN",
            "employeeName": "HENDRIK GUSTIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010050009",
            "employeeCode": "HENDRIK SUGIANTO",
            "employeeName": "HENDRIK SUGIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120006",
            "employeeCode": "HENG JIAN XIONG",
            "employeeName": "HENG JIAN XIONG",
            "isActive": 1
        },
        {
            "employeeId": "1332020020001",
            "employeeCode": "RYAN HENG",
            "employeeName": "HENG YONG SHENG, RYAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090004",
            "employeeCode": "HENGKY LUTUNGAN",
            "employeeName": "HENGKY LUTUNGAN",
            "isActive": 1
        },
        {
            "employeeId": "9999141220150005",
            "employeeCode": "HENRY LIM",
            "employeeName": "HENRY LIM JIAN HONG",
            "isActive": 1
        },
        {
            "employeeId": "1332010040001",
            "employeeCode": "HENRY PILLAI",
            "employeeName": "HENRY PILLAI",
            "isActive": 1
        },
        {
            "employeeId": "1332019090002",
            "employeeCode": "HENRY SUTANTYO",
            "employeeName": "HENRY SUTANTYO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080027",
            "employeeCode": "HENRY WEE",
            "employeeName": "HENRY WEE",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120013",
            "employeeCode": "HEPY GUSNINDRA",
            "employeeName": "HEPY GUSNINDRA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060006",
            "employeeCode": "HERBERT RAJAHOL",
            "employeeName": "HERBERT RAJAHOL",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090010",
            "employeeCode": "HERDIN",
            "employeeName": "HERDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332012030002",
            "employeeCode": "HERI PURWANTO",
            "employeeName": "HERI PURWANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030013",
            "employeeCode": "HERI SUSANTO",
            "employeeName": "HERI SUSANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010017",
            "employeeCode": "HERI YANTO",
            "employeeName": "HERI YANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332020060001",
            "employeeCode": "HERIBERTUS DARMADI",
            "employeeName": "HERIBERTUS DARMADI",
            "isActive": 1
        },
        {
            "employeeId": "1332011050013",
            "employeeCode": "HERMAN",
            "employeeName": "HERMAN",
            "isActive": 1
        },
        {
            "employeeId": "9999070420160005",
            "employeeCode": "HERMAN USMAN",
            "employeeName": "HERMAN USMAN TANGGI LALLO",
            "isActive": 1
        },
        {
            "employeeId": "9999030620160002",
            "employeeCode": "HERO GARBRIEL",
            "employeeName": "HERO GARBRIEL MADELLU",
            "isActive": 1
        },
        {
            "employeeId": "1332009100010",
            "employeeCode": "HERRY",
            "employeeName": "HERRY",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120005",
            "employeeCode": "HERRY SUPARTO",
            "employeeName": "HERRY SUPARTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080054",
            "employeeCode": "HERRY WELLEM",
            "employeeName": "HERRY WELLEM PANGEMANAN",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150015",
            "employeeCode": "HERU EFENDI",
            "employeeName": "HERU EFENDI",
            "isActive": 1
        },
        {
            "employeeId": "9999030820150003",
            "employeeCode": "HERU KURNIAWAN",
            "employeeName": "HERU KURNIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040003",
            "employeeCode": "HERY GINTING",
            "employeeName": "HERY GINTING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060001",
            "employeeCode": "HIA HAN QUAN, SAMUEL",
            "employeeName": "HIA HAN QUAN, SAMUEL",
            "isActive": 1
        },
        {
            "employeeId": "1332011060001",
            "employeeCode": "HIDAYATULLAH",
            "employeeName": "HIDAYATULLAH",
            "isActive": 1
        },
        {
            "employeeId": "9999260220150008",
            "employeeCode": "HLA TUN MAUNG",
            "employeeName": "HLA TUN MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050015",
            "employeeCode": "HLA WIN",
            "employeeName": "HLA WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009110016",
            "employeeCode": "HLAING TUN OO",
            "employeeName": "HLAING TUN OO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030008",
            "employeeCode": "HLAING TUN U",
            "employeeName": "HLAING TUN U",
            "isActive": 1
        },
        {
            "employeeId": "1332010020011",
            "employeeCode": "HLWAN MOE AUNG",
            "employeeName": "HLWAN MOE AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090037",
            "employeeCode": "HO KAH WAI",
            "employeeName": "HO KAH WAI",
            "isActive": 1
        },
        {
            "employeeId": "9991332014030005",
            "employeeCode": "HO KENG SENG",
            "employeeName": "HO KENG SENG",
            "isActive": 1
        },
        {
            "employeeId": "1332020080018",
            "employeeCode": "H19",
            "employeeName": "HOETEK 19",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030005",
            "employeeCode": "HOKER NOLI PITER",
            "employeeName": "HOKER NOLI PITER",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040012",
            "employeeCode": "HONG HSIEN HONG",
            "employeeName": "HONG HSIEN HONG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020009",
            "employeeCode": "HONG YAO PAI",
            "employeeName": "HONG YAO PAI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010008",
            "employeeCode": "HOTBIN SITOHANG",
            "employeeName": "HOTBIN SITOHANG",
            "isActive": 1
        },
        {
            "employeeId": "1332012040003",
            "employeeCode": "HOTMAN SITOHANG",
            "employeeName": "HOTMAN SITOHANG",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010002",
            "employeeCode": "HOU JINJIAN",
            "employeeName": "HOU JINJIAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030010",
            "employeeCode": "HTAY AUNG TUN",
            "employeeName": "HTAY AUNG TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332009110018",
            "employeeCode": "HTEIK TIN MYO",
            "employeeName": "HTEIK TIN MYO",
            "isActive": 1
        },
        {
            "employeeId": "1332011030003",
            "employeeCode": "HTEIN LINN NYEIN",
            "employeeName": "HTEIN LINN NYEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332010100002",
            "employeeCode": "HTEIN WIN",
            "employeeName": "HTEIN WIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090009",
            "employeeCode": "HTET AUNG",
            "employeeName": "HTET AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008110012",
            "employeeCode": "HTET LWIN KYAW",
            "employeeName": "HTET LWIN KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080001",
            "employeeCode": "HTET MAUNG MAUNG",
            "employeeName": "HTET MAUNG MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "999110820140003",
            "employeeCode": "HTET MYINT AUNG",
            "employeeName": "HTET MYINT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999270420150006",
            "employeeCode": "HTET NYI AUNG",
            "employeeName": "HTET NYI AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150007",
            "employeeCode": "HTET WAI PHYO",
            "employeeName": "HTET WAI PHYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040032",
            "employeeCode": "HTIN KYAW KHANT",
            "employeeName": "HTIN KYAW KHANT",
            "isActive": 1
        },
        {
            "employeeId": "1332011010008",
            "employeeCode": "HTOI SAN AUNG",
            "employeeName": "HTOI SAN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332010030014",
            "employeeCode": "HTOO AUNG HLAING",
            "employeeName": "HTOO AUNG HLAING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080002",
            "employeeCode": "HTOO KO KO KYAW",
            "employeeName": "HTOO KO KO KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9999121020150001",
            "employeeCode": "HTOO MYAT AUNG",
            "employeeName": "HTOO MYAT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332019030001",
            "employeeCode": "HU LUO LING, ROLEEN",
            "employeeName": "HU LUO LING, ROLEEN",
            "isActive": 1
        },
        {
            "employeeId": "1332009110011",
            "employeeCode": "HUSNUL MUBARAK",
            "employeeName": "HUSNUL MUBARAK",
            "isActive": 1
        },
        {
            "employeeId": "9999250520160019",
            "employeeCode": "HUTAPEA HASUDUNGAN",
            "employeeName": "HUTAPEA HASUDUNGAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010030006",
            "employeeCode": "IBRAHIM ABDUL KADER",
            "employeeName": "IBRAHIM ABDUL KADER",
            "isActive": 1
        },
        {
            "employeeId": "1332008080044",
            "employeeCode": "IBRAHIM YACUB",
            "employeeName": "IBRAHIM YACUB",
            "isActive": 1
        },
        {
            "employeeId": "1332010030004",
            "employeeCode": "IDAMAN BANUAJI",
            "employeeName": "IDAMAN BANUAJI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100006",
            "employeeCode": "IDRIS BIN AHMAD",
            "employeeName": "IDRIS BIN AHMAD",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110013",
            "employeeCode": "IDRIS BIN BUYUNG",
            "employeeName": "IDRIS BIN BUYUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008090005",
            "employeeCode": "IDRIS YURSAL",
            "employeeName": "IDRIS YURSAL",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080021",
            "employeeCode": "IHAM HIDAYAT",
            "employeeName": "IHAM HIDAYAT BIN MOHAMED SHAR",
            "isActive": 1
        },
        {
            "employeeId": "1332008100018",
            "employeeCode": "ILHAM ACHMAD",
            "employeeName": "ILHAM ACHMAD",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110015",
            "employeeCode": "ILHAM SUNARYA",
            "employeeName": "ILHAM SUNARYA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080005",
            "employeeCode": "IMAM MUZASA",
            "employeeName": "IMAM MUZASA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070001",
            "employeeCode": "IMRAN HAJIR",
            "employeeName": "IMRAN HAJIR",
            "isActive": 1
        },
        {
            "employeeId": "1332010020008",
            "employeeCode": "INDRA PRAKOSO",
            "employeeName": "INDRA PRAKOSO SASONGKO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120020",
            "employeeCode": "INDRA PRASETIA",
            "employeeName": "INDRA PRASETIA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080006",
            "employeeCode": "INDRA SURYANA",
            "employeeName": "INDRA SURYANA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100005",
            "employeeCode": "INDRASYAH",
            "employeeName": "INDRASYAH PUTRA SIREGAR",
            "isActive": 1
        },
        {
            "employeeId": "1332012030005",
            "employeeCode": "IQBAL AMAR",
            "employeeName": "IQBAL AMAR",
            "isActive": 1
        },
        {
            "employeeId": "1332009070019",
            "employeeCode": "IRAWAN BAGUS",
            "employeeName": "IRAWAN BAGUS",
            "isActive": 1
        },
        {
            "employeeId": "1332010020014",
            "employeeCode": "IRFAN",
            "employeeName": "IRFAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090017",
            "employeeCode": "IRIS YURSAL",
            "employeeName": "IRIS YURSAL",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080004",
            "employeeCode": "IRMAN CAHYADI",
            "employeeName": "IRMAN CAHYADI",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090040",
            "employeeCode": "IRPAN",
            "employeeName": "IRPAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010060002",
            "employeeCode": "IRVAN AGUS WAHYUDI",
            "employeeName": "IRVAN AGUS WAHYUDI",
            "isActive": 1
        },
        {
            "employeeId": "9999270620160010",
            "employeeCode": "ISDIANTO",
            "employeeName": "ISDIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010010008",
            "employeeCode": "ISHAK ACHMAD",
            "employeeName": "ISHAK ACHMAD",
            "isActive": 1
        },
        {
            "employeeId": "999220820140005",
            "employeeCode": "ISKANDAR NATSIR",
            "employeeName": "ISKANDAR NATSIR",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080015",
            "employeeCode": "ISMAIL",
            "employeeName": "ISMAIL",
            "isActive": 1
        },
        {
            "employeeId": "1332008090031",
            "employeeCode": "IVANTRI",
            "employeeName": "IVANTRI",
            "isActive": 1
        },
        {
            "employeeId": "1332011020008",
            "employeeCode": "JACINTO C MARABUT",
            "employeeName": "JACINTO C MARABUT",
            "isActive": 1
        },
        {
            "employeeId": "1332010070027",
            "employeeCode": "JACINTO CARRERA",
            "employeeName": "JACINTO CARRERA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060014",
            "employeeCode": "JACK SIMON",
            "employeeName": "JACK SIMON WALANGITAN",
            "isActive": 1
        },
        {
            "employeeId": "99992016070009",
            "employeeCode": "JACKIE QUEK",
            "employeeName": "JACKIE QUEK",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100004",
            "employeeCode": "JAFAR",
            "employeeName": "JAFAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060007",
            "employeeCode": "JAKA PURNAMA",
            "employeeName": "JAKA PURNAMA",
            "isActive": 1
        },
        {
            "employeeId": "1332013030005",
            "employeeCode": "JAKA WHINA LUMAYUNG",
            "employeeName": "JAKA WHINA LUMAYUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120010",
            "employeeCode": "JAMAL SEPTIAN",
            "employeeName": "JAMAL SEPTIAN HARI PURNOMO",
            "isActive": 1
        },
        {
            "employeeId": "9999240620150007",
            "employeeCode": "JAMES AREROS",
            "employeeName": "JAMES AREROS",
            "isActive": 1
        },
        {
            "employeeId": "1332008080038",
            "employeeCode": "JAMES J.KAINAGE",
            "employeeName": "JAMES J.KAINAGE",
            "isActive": 1
        },
        {
            "employeeId": "1332008100013",
            "employeeCode": "JAMES JULIANS",
            "employeeName": "JAMES JULIANS",
            "isActive": 1
        },
        {
            "employeeId": "1332019090001",
            "employeeCode": "JAMES SHEN",
            "employeeName": "JAMES SHEN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017010001",
            "employeeCode": "JAMOT HASUDUNGAN S.",
            "employeeName": "JAMOT HASUDUNGAN SITINJAK",
            "isActive": 1
        },
        {
            "employeeId": "9999030820150002",
            "employeeCode": "JAN MICHAEL OBILLO",
            "employeeName": "JAN MICHAEL OBILLO ROQUE",
            "isActive": 1
        },
        {
            "employeeId": "1332008090032",
            "employeeCode": "JANDRI LAMER RARAS",
            "employeeName": "JANDRI LAMER RARAS",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070018",
            "employeeCode": "JANNELL TOH",
            "employeeName": "JANNELL TOH",
            "isActive": 1
        },
        {
            "employeeId": "1332008070057",
            "employeeCode": "JANSIEMENS BOAS",
            "employeeName": "JANSIEMENS BOAS",
            "isActive": 1
        },
        {
            "employeeId": "1332008070047",
            "employeeCode": "JANUAR ARIFIN",
            "employeeName": "JANUAR ARIFIN",
            "isActive": 1
        },
        {
            "employeeId": "999050720140002",
            "employeeCode": "JASMI BIN KHAMIS",
            "employeeName": "JASMI BIN KHAMIS",
            "isActive": 1
        },
        {
            "employeeId": "1332008080011",
            "employeeCode": "JASON ONG",
            "employeeName": "JASON ONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332008080011",
            "employeeCode": "JASON ONG SIOW LONG",
            "employeeName": "JASON ONG SIOW LONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080007",
            "employeeCode": "JATMIKO SIDHI",
            "employeeName": "JATMIKO SIDHI PRAMONO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030001",
            "employeeCode": "JAY ROMMEL",
            "employeeName": "JAY ROMMEL",
            "isActive": 1
        },
        {
            "employeeId": "1332009090015",
            "employeeCode": "JAYSON DUBLA URIAS",
            "employeeName": "JAYSON DUBLA URIAS",
            "isActive": 1
        },
        {
            "employeeId": "1332009120289",
            "employeeCode": "JEFFERSON GARCIA",
            "employeeName": "JEFFERSON GARCIA FAUSTINO",
            "isActive": 1
        },
        {
            "employeeId": "1332009120290",
            "employeeCode": "JEFFERSON GARCIA",
            "employeeName": "JEFFERSON GARCIA FAUSTINO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030011",
            "employeeCode": "JEFFREY C.CAIGOY",
            "employeeName": "JEFFREY C.CAIGOY",
            "isActive": 1
        },
        {
            "employeeId": "1332009100029",
            "employeeCode": "JEFFREY POSADAS",
            "employeeName": "JEFFREY POSADAS MATEO",
            "isActive": 1
        },
        {
            "employeeId": "99992016080005",
            "employeeCode": "JEFRI SAMBIRA",
            "employeeName": "JEFRI SAMBIRA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040006",
            "employeeCode": "JEKLI LAPAERE",
            "employeeName": "JEKLI LAPAERE",
            "isActive": 1
        },
        {
            "employeeId": "99992016080002",
            "employeeCode": "JEMMY KAREL",
            "employeeName": "JEMMY KAREL",
            "isActive": 1
        },
        {
            "employeeId": "1332011030008",
            "employeeCode": "JENE ROY AREROS",
            "employeeName": "JENE ROY AREROS",
            "isActive": 1
        },
        {
            "employeeId": "1332009030019",
            "employeeCode": "JERMIAS",
            "employeeName": "JERMIAS",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100012",
            "employeeCode": "JESUS NABAYRA ROPA",
            "employeeName": "JESUS NABAYRA ROPA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120019",
            "employeeCode": "JIANG YI",
            "employeeName": "JIANG YI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080007",
            "employeeCode": "JIM LYBERT SIAUTA",
            "employeeName": "JIM LYBERT SIAUTA",
            "isActive": 1
        },
        {
            "employeeId": "1332009070003",
            "employeeCode": "JOE",
            "employeeName": "JOE",
            "isActive": 1
        },
        {
            "employeeId": "1332008080032",
            "employeeCode": "JOE MA",
            "employeeName": "JOE MA",
            "isActive": 1
        },
        {
            "employeeId": "1332010040003",
            "employeeCode": "JOENEL GARALDE",
            "employeeName": "JOENEL GARALDE",
            "isActive": 1
        },
        {
            "employeeId": "9991332008070021",
            "employeeCode": "JOEY MANGADSIL",
            "employeeName": "JOEY MANGADSIL DELA PENA",
            "isActive": 1
        },
        {
            "employeeId": "9999230120160008",
            "employeeCode": "JOHAN WAKHYUDI",
            "employeeName": "JOHAN WAKHYUDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060004",
            "employeeCode": "JOHANES ANTONIUS",
            "employeeName": "JOHANES ANTONIUS SINGALE",
            "isActive": 1
        },
        {
            "employeeId": "1332009070018",
            "employeeCode": "JOHANIS",
            "employeeName": "JOHANIS",
            "isActive": 1
        },
        {
            "employeeId": "1332012030001",
            "employeeCode": "JOHANIS ANTONIUS",
            "employeeName": "JOHANIS ANTONIUS",
            "isActive": 1
        },
        {
            "employeeId": "9999071220150004",
            "employeeCode": "JOHN KEYVIN",
            "employeeName": "JOHN KEYVIN LLAMELO DELA CRUZ",
            "isActive": 1
        },
        {
            "employeeId": "1332011020013",
            "employeeCode": "JOHN PAUL FAJARDO",
            "employeeName": "JOHN PAUL FAJARDO BELTRAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010060008",
            "employeeCode": "JOKO NUGROHO",
            "employeeName": "JOKO NUGROHO",
            "isActive": 1
        },
        {
            "employeeId": "9999250820150006",
            "employeeCode": "JOKO PURWIYANTO",
            "employeeName": "JOKO PURWIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070001",
            "employeeCode": "JOKO SUSILO",
            "employeeName": "JOKO SUSILO",
            "isActive": 1
        },
        {
            "employeeId": "1332010070029",
            "employeeCode": "JOKO SUTRISNO",
            "employeeName": "JOKO SUTRISNO",
            "isActive": 1
        },
        {
            "employeeId": "1332010070030",
            "employeeCode": "JOKO WAHYU",
            "employeeName": "JOKO WAHYU",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030004",
            "employeeCode": "JON LIKU LANDE",
            "employeeName": "JON LIKU LANDE",
            "isActive": 1
        },
        {
            "employeeId": "999121220140003",
            "employeeCode": "JONI INDUK",
            "employeeName": "JONI INDUK",
            "isActive": 1
        },
        {
            "employeeId": "1332011030006",
            "employeeCode": "JONI LIKU LANDE",
            "employeeName": "JONI LIKU LANDE",
            "isActive": 1
        },
        {
            "employeeId": "1332009040014",
            "employeeCode": "JOSEPH FERNANDO",
            "employeeName": "JOSEPH FERNANDO",
            "isActive": 1
        },
        {
            "employeeId": "9999250520160017",
            "employeeCode": "JOSHUA HUANG",
            "employeeName": "JOSHUA HUANG",
            "isActive": 1
        },
        {
            "employeeId": "1332013030004",
            "employeeCode": "JUANDA HUTAPEA",
            "employeeName": "JUANDA HUTAPEA",
            "isActive": 1
        },
        {
            "employeeId": "1332009030024",
            "employeeCode": "JULIANUS TIAS",
            "employeeName": "JULAINUS TIAS",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030011",
            "employeeCode": "JULFRI SIREGAR",
            "employeeName": "JULFRI SIREGAR",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040008",
            "employeeCode": "JULIUS ALDRIN",
            "employeeName": "JULIUS ALDRIN PANSA M",
            "isActive": 1
        },
        {
            "employeeId": "1332009120273",
            "employeeCode": "JUNAEDI",
            "employeeName": "JUNAEDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100016",
            "employeeCode": "JUNAIDI",
            "employeeName": "JUNAIDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012070003",
            "employeeCode": "JUNAIDI MAKMUR",
            "employeeName": "JUNAIDI MAKMUR",
            "isActive": 1
        },
        {
            "employeeId": "1332011010015",
            "employeeCode": "JUNIMAR ORTIZ DIZON",
            "employeeName": "JUNIMAR ORTIZ DIZON",
            "isActive": 1
        },
        {
            "employeeId": "1332008070028",
            "employeeCode": "KALFEIN TELLAM WUA",
            "employeeName": "KALFEIN TELLAM WUA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050006",
            "employeeCode": "KAMARUZZAMAN",
            "employeeName": "KAMARUZZAMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010070028",
            "employeeCode": "KAMRI",
            "employeeName": "KAMRI",
            "isActive": 1
        },
        {
            "employeeId": "1332020080015",
            "employeeCode": "K2",
            "employeeName": "KANTEK 2",
            "isActive": 1
        },
        {
            "employeeId": "1332012100003",
            "employeeCode": "KARDIMIN",
            "employeeName": "KARDIMIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008100022",
            "employeeCode": "KAREL JELLY KOBIS",
            "employeeName": "KAREL JELLY KOBIS",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080009",
            "employeeCode": "KAREL PATUNGGU",
            "employeeName": "KAREL PATUNGGU",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090010",
            "employeeCode": "KARNO",
            "employeeName": "KARNO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090006",
            "employeeCode": "KARYADI EKA  SUBRATA",
            "employeeName": "KARYADI EKA  SUBRATA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120006",
            "employeeCode": "KASMAN TAMBA",
            "employeeName": "KASMAN TAMBA",
            "isActive": 1
        },
        {
            "employeeId": "999300620140009",
            "employeeCode": "KASMIRANTA SITEPU",
            "employeeName": "KASMIRANTA SITEPU",
            "isActive": 1
        },
        {
            "employeeId": "999011020140001",
            "employeeCode": "KATHICK S/O M.J",
            "employeeName": "KATHICK S/O MITHRA JAYAKRISHNA",
            "isActive": 1
        },
        {
            "employeeId": "999011020140002",
            "employeeCode": "KARTHICK S/O M.J",
            "employeeName": "KATHICK S/O MITHRA JAYAKRISHNA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100011",
            "employeeCode": "KAUNG HTET KYAW",
            "employeeName": "KAUNG HTET KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110008",
            "employeeCode": "KAUNG HTET LWIN",
            "employeeName": "KAUNG HTET LWIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020007",
            "employeeCode": "KAUNG MYAT PAING",
            "employeeName": "KAUNG MYAT PAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120012",
            "employeeCode": "KAUNG NYUNT",
            "employeeName": "KAUNG NYUNT",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030006",
            "employeeCode": "KAUNG NYUNT - M",
            "employeeName": "KAUNG NYUNT - MYANMAR",
            "isActive": 1
        },
        {
            "employeeId": "9999020220160003",
            "employeeCode": "KAUNG SAN HTET",
            "employeeName": "KAUNG SAN HTET",
            "isActive": 1
        },
        {
            "employeeId": "1332008110017",
            "employeeCode": "KAW THU",
            "employeeName": "KAW THU",
            "isActive": 1
        },
        {
            "employeeId": "1332008120012",
            "employeeCode": "KEMAS M. A. SUHAILI",
            "employeeName": "KEMAS MUHAMMAD ANWAR SUHAILI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040010",
            "employeeCode": "KERK PEI WEI",
            "employeeName": "KERK PEI WEI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090002",
            "employeeCode": "KEVIN LIM WEE KIAT",
            "employeeName": "KEVIN LIM WEE KIAT",
            "isActive": 1
        },
        {
            "employeeId": "1332008110002",
            "employeeCode": "KHAERUL",
            "employeeName": "KHAERUL",
            "isActive": 1
        },
        {
            "employeeId": "1332009120253",
            "employeeCode": "KHAIRUDDIN RIDWAN",
            "employeeName": "KHAIRUDDIN RIDWAN",
            "isActive": 1
        },
        {
            "employeeId": "999070720140004",
            "employeeCode": "KHAIRUL RIDUAN",
            "employeeName": "KHAIRUL RIDUAN BIN ASMAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050008",
            "employeeCode": "KHALIID B MOHMOOD",
            "employeeName": "KHALIID B MOHMOOD",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080011",
            "employeeCode": "KHIN AUNG",
            "employeeName": "KHIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008070051",
            "employeeCode": "KHIN MAUNG AYE",
            "employeeName": "KHIN MAUNG AYE",
            "isActive": 1
        },
        {
            "employeeId": "1332009040007",
            "employeeCode": "KHIN MAUNG MYINT",
            "employeeName": "KHIN MAUNG MYINT",
            "isActive": 1
        },
        {
            "employeeId": "1332010100006",
            "employeeCode": "KHIN MAUNG WIN",
            "employeeName": "KHIN MAUNG WIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090018",
            "employeeCode": "KHO THEAN TEIK",
            "employeeName": "KHO THEAN TEIK, ANDREW",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040004",
            "employeeCode": "KHOO YE SIANG, IAN",
            "employeeName": "KHOO YE SIANG, IAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020007",
            "employeeCode": "KIM JIMOOK",
            "employeeName": "KIM JIMOOK, MARTIN",
            "isActive": 1
        },
        {
            "employeeId": "1332020080013",
            "employeeCode": "K1",
            "employeeName": "KIMTEK 1",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100006",
            "employeeCode": "KISOR",
            "employeeName": "KISOR KUMAR BABU LALL",
            "isActive": 1
        },
        {
            "employeeId": "1332020080009",
            "employeeCode": "K9",
            "employeeName": "KITEK9",
            "isActive": 1
        },
        {
            "employeeId": "9999332017010004",
            "employeeCode": "KO KO LATT",
            "employeeName": "KO KO LATT",
            "isActive": 1
        },
        {
            "employeeId": "1332011010011",
            "employeeCode": "KO KO NAING",
            "employeeName": "KO KO NAING",
            "isActive": 1
        },
        {
            "employeeId": "9999031120150001",
            "employeeCode": "KO KO WIN",
            "employeeName": "KO KO WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011030001",
            "employeeCode": "KO SAW",
            "employeeName": "KO SAW",
            "isActive": 1
        },
        {
            "employeeId": "9999060720150006",
            "employeeCode": "KODRI DARMAWAN",
            "employeeName": "KODRI DARMAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999250520160018",
            "employeeCode": "DESMOND KOH",
            "employeeName": "KOH CHENG KAI",
            "isActive": 1
        },
        {
            "employeeId": "1332009070010",
            "employeeCode": "KOH KOON YIAN",
            "employeeName": "KOH KOON YIAN",
            "isActive": 1
        },
        {
            "employeeId": "999101120140001",
            "employeeCode": "KOH TAT HENG",
            "employeeName": "KOH TAT HENG",
            "isActive": 1
        },
        {
            "employeeId": "1332009100005",
            "employeeCode": "KRESNO YUNTORO",
            "employeeName": "KRESNO YUNTORO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070007",
            "employeeCode": "KRISTIAN SIAHAAN",
            "employeeName": "KRISTIAN SIAHAAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080017",
            "employeeCode": "KUGANESAN",
            "employeeName": "KUGANESAN S/O VIJAYAKUMAR",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090027",
            "employeeCode": "KUKUH MEGANTORO",
            "employeeName": "KUKUH MEGANTORO",
            "isActive": 1
        },
        {
            "employeeId": "1332009090020",
            "employeeCode": "KURNIA NURUL HIDAYAT",
            "employeeName": "KURNIA NURUL HIDAYAT",
            "isActive": 1
        },
        {
            "employeeId": "1332010070012",
            "employeeCode": "KWEK KANE SING",
            "employeeName": "KWEK KANE SING",
            "isActive": 1
        },
        {
            "employeeId": "99992016070010",
            "employeeCode": "KYAW BHONE LIN",
            "employeeName": "KYAW BHONE LIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100009",
            "employeeCode": "KYAW HTET ZAW",
            "employeeName": "KYAW HTET ZAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100010",
            "employeeCode": "KYAW HTET ZAW",
            "employeeName": "KYAW HTET ZAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110013",
            "employeeCode": "KYAW KO KO",
            "employeeName": "KYAW KO KO",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120003",
            "employeeCode": "KYAW LIN TUN",
            "employeeName": "KYAW LIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332009040006",
            "employeeCode": "KYAW MIN TUN",
            "employeeName": "KYAW MIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332010060004",
            "employeeCode": "KYAW MYINT HLAING",
            "employeeName": "KYAW MYINT HLAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010013",
            "employeeCode": "KYAW MYO HTIKE",
            "employeeName": "KYAW MYO HTIKE",
            "isActive": 1
        },
        {
            "employeeId": "1332011050006",
            "employeeCode": "KYAW MYO THANT",
            "employeeName": "KYAW MYO THANT",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020010",
            "employeeCode": "KYAW NAING",
            "employeeName": "KYAW NAING",
            "isActive": 1
        },
        {
            "employeeId": "1332009020005",
            "employeeCode": "KYAW NAUNG LYNN",
            "employeeName": "KYAW NAUNG LYNN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090009",
            "employeeCode": "KYAW PHYO WAI",
            "employeeName": "KYAW PHYO WAI",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020014",
            "employeeCode": "KYAW SWAR HEIN",
            "employeeName": "KYAW SWAR HEIN",
            "isActive": 1
        },
        {
            "employeeId": "9999070420160007",
            "employeeCode": "KYAW SWAR HTAY LWIN",
            "employeeName": "KYAW SWAR HTAY LWIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090030",
            "employeeCode": "KYAW SWAR WIN",
            "employeeName": "KYAW SWAR WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009050005",
            "employeeCode": "KYAW SWE WIN",
            "employeeName": "KYAW SWE WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009050006",
            "employeeCode": "KYAW SWE WIN",
            "employeeName": "KYAW SWE WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070002",
            "employeeCode": "KYAW THEIN",
            "employeeName": "KYAW THEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011050007",
            "employeeCode": "KYAW THI HA WIN",
            "employeeName": "KYAW THI HA WIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080007",
            "employeeCode": "KYAW THU AUNG",
            "employeeName": "KYAW THU AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009090003",
            "employeeCode": "KYAW THU YA",
            "employeeName": "KYAW THU YA",
            "isActive": 1
        },
        {
            "employeeId": "1332012030004",
            "employeeCode": "KYAW W.T.",
            "employeeName": "KYAW W.T.",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040009",
            "employeeCode": "KYAW WAI PHYO",
            "employeeName": "KYAW WAI PHYO",
            "isActive": 1
        },
        {
            "employeeId": "9999070320160003",
            "employeeCode": "KYAW WAI YAN",
            "employeeName": "KYAW WAI YAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012090004",
            "employeeCode": "KYAW WIN THEIN",
            "employeeName": "KYAW WIN THEIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012090005",
            "employeeCode": "KYAW WIN THEIN",
            "employeeName": "KYAW WIN THEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070050",
            "employeeCode": "KYAW ZAY YA",
            "employeeName": "KYAW ZAY YA",
            "isActive": 1
        },
        {
            "employeeId": "9991332014020002",
            "employeeCode": "KYAW ZIN HEIN",
            "employeeName": "KYAW ZIN HEIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050002",
            "employeeCode": "KYAW ZIN THET",
            "employeeName": "KYAW ZIN THET",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080014",
            "employeeCode": "KYI NYEIN HTET",
            "employeeName": "KYI NYEIN HTET",
            "isActive": 1
        },
        {
            "employeeId": "1332009110015",
            "employeeCode": "KYIN SEIN",
            "employeeName": "KYIN SEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090050",
            "employeeCode": "LA MOHAMADIA",
            "employeeName": "LA MOHAMADIA",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010006",
            "employeeCode": "LA ODE AHMAD DIAZ",
            "employeeName": "LA ODE AHMAD DIAZ HALIR",
            "isActive": 1
        },
        {
            "employeeId": "9991332014030002",
            "employeeCode": "LA ODE KAHARUDIN",
            "employeeName": "LA ODE KAHARUDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008080040",
            "employeeCode": "LA ODE YINTAN",
            "employeeName": "LA ODE YINTAN",
            "isActive": 1
        },
        {
            "employeeId": "999110820140002",
            "employeeCode": "LACSA ELMER",
            "employeeName": "LACSA ELMER MACABUNGA",
            "isActive": 1
        },
        {
            "employeeId": "1332019110002",
            "employeeCode": "FELICIA LAI",
            "employeeName": "LAI WEI SHAN, FELICIA",
            "isActive": 1
        },
        {
            "employeeId": "9999332016090015",
            "employeeCode": "LAMBOK SARAGI",
            "employeeName": "LAMBOK SARAGI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080002",
            "employeeCode": "LAODE FAHMILLUDIN",
            "employeeName": "LAODE FAHMILLUDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090009",
            "employeeCode": "LASUT",
            "employeeName": "LASUT",
            "isActive": 1
        },
        {
            "employeeId": "1332008080049",
            "employeeCode": "LATIGA",
            "employeeName": "LATIGA",
            "isActive": 1
        },
        {
            "employeeId": "9991332014030006",
            "employeeCode": "LAW POH BENG",
            "employeeName": "LAW POH BENG",
            "isActive": 1
        },
        {
            "employeeId": "1332010110002",
            "employeeCode": "LAWERANCE PEH",
            "employeeName": "LAWERANCE PEH",
            "isActive": 1
        },
        {
            "employeeId": "1332008090020",
            "employeeCode": "LEDI ASIDO",
            "employeeName": "LEDI ASIDO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080030",
            "employeeCode": "LEE BOON KWEE",
            "employeeName": "LEE BOON KWEE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060008",
            "employeeCode": "LEE CHANG FENG, ROY",
            "employeeName": "LEE CHANG FENG, ROY",
            "isActive": 1
        },
        {
            "employeeId": "13308040001",
            "employeeCode": "SHERMAN LEE",
            "employeeName": "LEE CHIN TIONG, SHERMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008080046",
            "employeeCode": "LEE CHOON CHENG",
            "employeeName": "LEE CHOON CHENG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090004",
            "employeeCode": "LEE GIM HUAT, JOSEPH",
            "employeeName": "LEE GIM HUAT, JOSEPH",
            "isActive": 1
        },
        {
            "employeeId": "9999332016090014",
            "employeeCode": "LEE JIA HAO, TITUS",
            "employeeName": "LEE JIA HAO, TITUS",
            "isActive": 1
        },
        {
            "employeeId": "1332019060002",
            "employeeCode": "KACY LEE",
            "employeeName": "LEE KENG CHAN, KACY",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090035",
            "employeeCode": "KELVIN LEE",
            "employeeName": "LEE KIN CHIEW, KELVIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060010",
            "employeeCode": "LEE LIONG HEE, ALAN",
            "employeeName": "LEE LIONG HEE, ALAN",
            "isActive": 1
        },
        {
            "employeeId": "13308050003",
            "employeeCode": "LEE SHAO KUAN",
            "employeeName": "LEE SHAO KUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009080005",
            "employeeCode": "LEE TENG YONG",
            "employeeName": "LEE TENG YONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050013",
            "employeeCode": "LEE WEE KEONG, GAREE",
            "employeeName": "LEE WEE KEONG, GAREE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060005",
            "employeeCode": "LEE YEE CHANG, ERIC",
            "employeeName": "LEE YEE CHANG, ERIC",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090016",
            "employeeCode": "LEE YONG RONG",
            "employeeName": "LEE YONG RONG, JORDAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070016",
            "employeeCode": "JORDAN LEE",
            "employeeName": "LEE YONG RONG, JORDAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040009",
            "employeeCode": "LEE ZHAN BANG",
            "employeeName": "LEE ZHAN BANG",
            "isActive": 1
        },
        {
            "employeeId": "999200920140013",
            "employeeCode": "LEK BOON KENG, RYAN",
            "employeeName": "LEK BOON KENG, RYAN",
            "isActive": 1
        },
        {
            "employeeId": "999231220140010",
            "employeeCode": "LEONARD U. CAMERO",
            "employeeName": "LEONARD UBUNGEN CAMERO",
            "isActive": 1
        },
        {
            "employeeId": "1332011120001",
            "employeeCode": "LEONG KONG MING",
            "employeeName": "LEONG KONG MING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040001",
            "employeeCode": "LEONG RICHARD",
            "employeeName": "LEONG RICHARD ABENOJAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070004",
            "employeeCode": "LEOW PEI QI",
            "employeeName": "LEOW PEI QI, JOSEPHINE",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050010",
            "employeeCode": "JOANNA LER",
            "employeeName": "LER PEI EN, JOANNA",
            "isActive": 1
        },
        {
            "employeeId": "1332010050001",
            "employeeCode": "LI AO",
            "employeeName": "LI AO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080031",
            "employeeCode": "LI MING HUI",
            "employeeName": "LI MING HUI",
            "isActive": 1
        },
        {
            "employeeId": "1332009040001",
            "employeeCode": "LI MING ZHOU",
            "employeeName": "LI MING ZHOU",
            "isActive": 1
        },
        {
            "employeeId": "1332010070015",
            "employeeCode": "LI OO",
            "employeeName": "LI OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011120001",
            "employeeCode": "LIM CHOON SHEN JASON",
            "employeeName": "LIM CHOON SHEN JASON",
            "isActive": 1
        },
        {
            "employeeId": "1332008080028",
            "employeeCode": "LIM ENG HENG",
            "employeeName": "LIM ENG HENG",
            "isActive": 1
        },
        {
            "employeeId": "9991332014030007",
            "employeeCode": "LIM HONG KENG",
            "employeeName": "LIM HONG KENG",
            "isActive": 1
        },
        {
            "employeeId": "999200920140016",
            "employeeCode": "LIM HONG XIU, JAMES",
            "employeeName": "LIM HONG XIU, JAMES",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090003",
            "employeeCode": "LIM KHENG HUAT",
            "employeeName": "LIM KHENG HUAT",
            "isActive": 1
        },
        {
            "employeeId": "999230420140010",
            "employeeCode": "LIM KHNG SENG",
            "employeeName": "LIM KHNG SENG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080034",
            "employeeCode": "LIM KIAM HO",
            "employeeName": "LIM KIAM HO",
            "isActive": 1
        },
        {
            "employeeId": "1332012100007",
            "employeeCode": "RICHARD LIM",
            "employeeName": "LIM KIAN HO, RICHARD",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080001",
            "employeeCode": "LIM LIONEL",
            "employeeName": "LIM LIONEL",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040008",
            "employeeCode": "LIM MING DAO, JOEL",
            "employeeName": "LIM MING DAO, JOEL",
            "isActive": 1
        },
        {
            "employeeId": "9991332010020012",
            "employeeCode": "GARY LIM PH",
            "employeeName": "LIM PUAY HWA, GARY",
            "isActive": 1
        },
        {
            "employeeId": "1332010070014",
            "employeeCode": "LIM SEE BOON",
            "employeeName": "LIM SEE BOON",
            "isActive": 1
        },
        {
            "employeeId": "9999332015110009",
            "employeeCode": "LIM SHAN QIAN",
            "employeeName": "LIM SHAN QIAN",
            "isActive": 1
        },
        {
            "employeeId": "9999231120150009",
            "employeeCode": "LIM SHAN QUAN",
            "employeeName": "LIM SHAN QUAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017110005",
            "employeeCode": "LIM SI JIN, FABIAN",
            "employeeName": "LIM SI JIN, FABIAN",
            "isActive": 1
        },
        {
            "employeeId": "1332018120001",
            "employeeCode": "LIM WEE KIAT KEVIN",
            "employeeName": "LIM WEE KIAT KEVIN",
            "isActive": 1
        },
        {
            "employeeId": "1332020040004",
            "employeeCode": "GARY LIM YH",
            "employeeName": "LIM YAO HUI, GARY",
            "isActive": 1
        },
        {
            "employeeId": "13308050007",
            "employeeCode": "ALEX LIM",
            "employeeName": "LIM YU JIN, ALEX",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040033",
            "employeeCode": "LIN THANT MAUNG",
            "employeeName": "LIN THANT MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060009",
            "employeeCode": "LIN WEI LONG",
            "employeeName": "LIN WEI LONG",
            "isActive": 1
        },
        {
            "employeeId": "1332010050003",
            "employeeCode": "LIN WENMING",
            "employeeName": "LIN WENMING",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030009",
            "employeeCode": "LIN YAO ZONG",
            "employeeName": "LIN YAO ZONG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050007",
            "employeeCode": "LIN YAO ZONG, ACE",
            "employeeName": "LIN YAO ZONG, ACE",
            "isActive": 1
        },
        {
            "employeeId": "1332010100013",
            "employeeCode": "LINUS ROBERT",
            "employeeName": "LINUS ROBERT",
            "isActive": 1
        },
        {
            "employeeId": "1332008070009",
            "employeeCode": "LIU QUAN",
            "employeeName": "LIU QUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010050013",
            "employeeCode": "LIU SHAO YUAN",
            "employeeName": "LIU SHAO YUAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100002",
            "employeeCode": "LIU SHENG DONG",
            "employeeName": "LIU SHENG DONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010013",
            "employeeCode": "LIU XI",
            "employeeName": "LIU XI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013030003",
            "employeeCode": "LIU XIAOKE",
            "employeeName": "LIU XIAOKE",
            "isActive": 1
        },
        {
            "employeeId": "1332011010002",
            "employeeCode": "LIU YONG",
            "employeeName": "LIU YONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060004",
            "employeeCode": "LOH JUN KAI",
            "employeeName": "LOH JUN KAI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090052",
            "employeeCode": "LOH KIA TIEW",
            "employeeName": "LOH KIA TIEW",
            "isActive": 1
        },
        {
            "employeeId": "1332009100023",
            "employeeCode": "LOO CHOO KEONG",
            "employeeName": "LOO CHOO KEONG",
            "isActive": 1
        },
        {
            "employeeId": "9999270720150010",
            "employeeCode": "LOO ENG LIN",
            "employeeName": "LOO ENG LIN",
            "isActive": 1
        },
        {
            "employeeId": "13308050011",
            "employeeCode": "LOW PIN PIN",
            "employeeName": "LOW PIN PIN",
            "isActive": 1
        },
        {
            "employeeId": "1332020080001",
            "employeeCode": "LSL EXPRESS",
            "employeeName": "LSL EXPRESS",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120008",
            "employeeCode": "LU MIN AUNG",
            "employeeName": "LU MIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332010050012",
            "employeeCode": "LU WEI JIA",
            "employeeName": "LU WEI JIA",
            "isActive": 1
        },
        {
            "employeeId": "999160620140004",
            "employeeCode": "LUDI SUKMA MULYANA",
            "employeeName": "LUDI SUKMA MULYANA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040006",
            "employeeCode": "LUQMAN HAKIM",
            "employeeName": "LUQMAN HAKIM BIN MOHAMED ALIAS",
            "isActive": 1
        },
        {
            "employeeId": "9991332014020001",
            "employeeCode": "M CHUSNI TAMRIN",
            "employeeName": "M CHUSNI TAMRIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009100024",
            "employeeCode": "MA LEK FAI",
            "employeeName": "MA LEK FAI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080010",
            "employeeCode": "MAATA JOHN",
            "employeeName": "MAATA JOHN LLOYD MANGINSAY",
            "isActive": 1
        },
        {
            "employeeId": "1332008080048",
            "employeeCode": "MAHFUD HALWANU",
            "employeeName": "MAHFUD HALWANU",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100024",
            "employeeCode": "MAHMUD HOSSAIN",
            "employeeName": "MAHMUD HOSSAIN",
            "isActive": 1
        },
        {
            "employeeId": "9999020720150002",
            "employeeCode": "MAHMUDDIN LURU",
            "employeeName": "MAHMUDDIN LURU",
            "isActive": 1
        },
        {
            "employeeId": "9999100920150002",
            "employeeCode": "MAICO ARY PRAMONO",
            "employeeName": "MAICO ARY PRAMONO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030007",
            "employeeCode": "MALIK ROMPO",
            "employeeName": "MALIK ROMPO",
            "isActive": 1
        },
        {
            "employeeId": "999300420140019",
            "employeeCode": "MANGADSIL FHIEL",
            "employeeName": "MANGADSIL FHIEL VILLANOS",
            "isActive": 1
        },
        {
            "employeeId": "1332008120022",
            "employeeCode": "MANGASIDOS S.",
            "employeeName": "MANGASIDOS S.",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010012",
            "employeeCode": "MANGOLO DAMRI",
            "employeeName": "MANGOLO DAMRI KRISTAL",
            "isActive": 1
        },
        {
            "employeeId": "1332010020002",
            "employeeCode": "MANITO ZAINABUN",
            "employeeName": "MANITO ZAINABUN",
            "isActive": 1
        },
        {
            "employeeId": "1332009090016",
            "employeeCode": "MANNY COLLADO",
            "employeeName": "MANNY COLLADO",
            "isActive": 1
        },
        {
            "employeeId": "1332009100032",
            "employeeCode": "MARIO D RANGKANG",
            "employeeName": "MARIO DANTJE RANGKANG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030002",
            "employeeCode": "MARIO SINAGA",
            "employeeName": "MARIO SINAGA",
            "isActive": 1
        },
        {
            "employeeId": "1332009060004",
            "employeeCode": "MARK FRANCIS",
            "employeeName": "MARK FRANCIS",
            "isActive": 1
        },
        {
            "employeeId": "999110420090015",
            "employeeCode": "MARK TRAJANG S.",
            "employeeName": "MARK TRAJANG S.",
            "isActive": 1
        },
        {
            "employeeId": "1332009040015",
            "employeeCode": "MARK TRAJANO S.",
            "employeeName": "MARK TRAJANO S.",
            "isActive": 1
        },
        {
            "employeeId": "1332010120001",
            "employeeCode": "MARK TRAJANO SORIANO",
            "employeeName": "MARK TRAJANO SORIANO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080052",
            "employeeCode": "MARKUS DAMAR",
            "employeeName": "MARKUS DAMAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090004",
            "employeeCode": "MARKUS MARIANG",
            "employeeName": "MARKUS MARIANG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080001",
            "employeeCode": "MARKUS TANDI",
            "employeeName": "MARKUS TANDI",
            "isActive": 1
        },
        {
            "employeeId": "1332010080004",
            "employeeCode": "MARSIANUS",
            "employeeName": "MARSIANUS",
            "isActive": 1
        },
        {
            "employeeId": "9999332017060004",
            "employeeCode": "MARTHUNIS ABU BAKAR",
            "employeeName": "MARTHUNIS ABU BAKAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010007",
            "employeeCode": "MARTIN CLINTON",
            "employeeName": "MARTIN CLINTON SIMAMORA",
            "isActive": 1
        },
        {
            "employeeId": "1332008080016",
            "employeeCode": "MARTIN PHUA",
            "employeeName": "MARTIN PHUA",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010020",
            "employeeCode": "MARTINEZ JOENEL",
            "employeeName": "MARTINEZ JOENEL GARALDE",
            "isActive": 1
        },
        {
            "employeeId": "1332008120001",
            "employeeCode": "MARTINUS HEGEWATI",
            "employeeName": "MARTINUS HEGEWATI",
            "isActive": 1
        },
        {
            "employeeId": "1332009020004",
            "employeeCode": "MARVIC MATEO MATIUS",
            "employeeName": "MARVIC MATEO MATIUS",
            "isActive": 1
        },
        {
            "employeeId": "1332009040002",
            "employeeCode": "MARWIN BAWILUKU",
            "employeeName": "MARWIN BAWILUKU",
            "isActive": 1
        },
        {
            "employeeId": "9999332017010005",
            "employeeCode": "MARYANTO",
            "employeeName": "MARYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080019",
            "employeeCode": "MARZUKI NOORIZZAD",
            "employeeName": "MARZUKI NOORIZZAD BIN SANIP",
            "isActive": 1
        },
        {
            "employeeId": "1332012060001",
            "employeeCode": "MASKUR DAMAR",
            "employeeName": "MASKUR DAMAR",
            "isActive": 1
        },
        {
            "employeeId": "1332009110008",
            "employeeCode": "MASROKAN",
            "employeeName": "MASROKAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100007",
            "employeeCode": "MAULANA HAYAT",
            "employeeName": "MAULANA HAYAT",
            "isActive": 1
        },
        {
            "employeeId": "1332008100004",
            "employeeCode": "MAUNG AUNG LIN TUN",
            "employeeName": "MAUNG AUNG LIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040006",
            "employeeCode": "MAUNG MAUNG KYAW",
            "employeeName": "MAUNG MAUNG KYAW",
            "isActive": 1
        },
        {
            "employeeId": "1332009030012",
            "employeeCode": "MAUNG MAUNG MYINT",
            "employeeName": "MAUNG MAUNG MYINT",
            "isActive": 1
        },
        {
            "employeeId": "1332010070008",
            "employeeCode": "MAUNG PHONE KHIN",
            "employeeName": "MAUNG PHONE KHIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009100019",
            "employeeCode": "MAXIMILIANUS",
            "employeeName": "MAXIMILIANUS WILFRIDUS",
            "isActive": 1
        },
        {
            "employeeId": "1332009090014",
            "employeeCode": "MAXMILIANUS",
            "employeeName": "MAXMILIANUS",
            "isActive": 1
        },
        {
            "employeeId": "1332008090033",
            "employeeCode": "MAXMILLIAN WAWONDOS",
            "employeeName": "MAXMILLIAN WAWONDOS",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090006",
            "employeeCode": "MAZZAKI",
            "employeeName": "MAZZAKI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050006",
            "employeeCode": "MD AFIQ",
            "employeeName": "MD AFIQ",
            "isActive": 1
        },
        {
            "employeeId": "9991332013020002",
            "employeeCode": "MD JAHIDUL ISLAM",
            "employeeName": "MD JAHIDUL ISLAM",
            "isActive": 1
        },
        {
            "employeeId": "1332014030001",
            "employeeCode": "MD. JAHIDUL ISLAM",
            "employeeName": "MD. JAHIDUL ISLAM",
            "isActive": 1
        },
        {
            "employeeId": "1332008090013",
            "employeeCode": "MEIDIE BASTIAN",
            "employeeName": "MEIDIE BASTIAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010070025",
            "employeeCode": "MELVIN MATIAS",
            "employeeName": "MELVIN MATIAS",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040001",
            "employeeCode": "MERVIN BAWILUKU",
            "employeeName": "MERVIN BAWILUKU",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040004",
            "employeeCode": "MERVIN TOLETE IBARRA",
            "employeeName": "MERVIN TOLETE IBARRA",
            "isActive": 1
        },
        {
            "employeeId": "1332010090007",
            "employeeCode": "MEVIN M BONIFACIO",
            "employeeName": "MEVIN MATIAS BONIFACIO",
            "isActive": 1
        },
        {
            "employeeId": "1332008120016",
            "employeeCode": "MEZI HERMANA",
            "employeeName": "MEZI HERMANA",
            "isActive": 1
        },
        {
            "employeeId": "1332009060015",
            "employeeCode": "MICHAEL DAVIS AYIM",
            "employeeName": "MICHAEL DAVIS AYIM",
            "isActive": 1
        },
        {
            "employeeId": "1332011020004",
            "employeeCode": "MICHEAL PAN",
            "employeeName": "MICHEAL PAN",
            "isActive": 1
        },
        {
            "employeeId": "9999270720150011",
            "employeeCode": "MIN CHAN MYAE OO",
            "employeeName": "MIN CHAN MYAE OO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030031",
            "employeeCode": "MIN MIN LATT",
            "employeeName": "MIN MIN LATT",
            "isActive": 1
        },
        {
            "employeeId": "1332010080003",
            "employeeCode": "MIN SWEE OO",
            "employeeName": "MIN SWEE OO",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100008",
            "employeeCode": "MIN THU HTWE",
            "employeeName": "MIN THU HTWE",
            "isActive": 1
        },
        {
            "employeeId": "99992016090001",
            "employeeCode": "MIN THU KHANT",
            "employeeName": "MIN THU KHANT",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030010",
            "employeeCode": "MIN TUN AUNG",
            "employeeName": "MIN TUN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999180620150005",
            "employeeCode": "MINN KHANT KYAW",
            "employeeName": "MINN KHANT KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070011",
            "employeeCode": "MIRSAD",
            "employeeName": "MIRSAD UMAR",
            "isActive": 1
        },
        {
            "employeeId": "1332012100004",
            "employeeCode": "MOCH YOGIE",
            "employeeName": "MOCH YOGIE",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110005",
            "employeeCode": "MOCHAMAD ARIK SUNARI",
            "employeeName": "MOCHAMAD ARIK SUNARI",
            "isActive": 1
        },
        {
            "employeeId": "9999141120150004",
            "employeeCode": "MOCHAMAD ICHSAN",
            "employeeName": "MOCHAMAD ICHSAN",
            "isActive": 1
        },
        {
            "employeeId": "1332013020005",
            "employeeCode": "M. YOGIE. W",
            "employeeName": "MOCHAMAD YOGIE WISUDAWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332013020006",
            "employeeCode": "M. YOGIE. W",
            "employeeName": "MOCHAMAD YOGIE WISUDAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012090002",
            "employeeCode": "MOCHAMAD YOGIE",
            "employeeName": "MOCHAMAD YOGIE WISUDAWANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009090004",
            "employeeCode": "MOE MIN OO",
            "employeeName": "MOE MIN OO",
            "isActive": 1
        },
        {
            "employeeId": "999190720140008",
            "employeeCode": "MOE WIN KYAW",
            "employeeName": "MOE WIN KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020004",
            "employeeCode": "MOE ZAW KO",
            "employeeName": "MOE ZAW KO",
            "isActive": 1
        },
        {
            "employeeId": "1332009090021",
            "employeeCode": "MOH RIDWAN",
            "employeeName": "MOH RIDWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010004",
            "employeeCode": "MOH. NOOR BIN TAIB",
            "employeeName": "MOH. NOOR BIN TAID",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100003",
            "employeeCode": "MOH. RIZAL",
            "employeeName": "MOH. RIZAL",
            "isActive": 1
        },
        {
            "employeeId": "1332011020002",
            "employeeCode": "MOHAMAD BASHIR",
            "employeeName": "MOHAMAD BASHIR",
            "isActive": 1
        },
        {
            "employeeId": "1332011020003",
            "employeeCode": "MOHAMAD BASHIR",
            "employeeName": "MOHAMAD BASHIR",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150010",
            "employeeCode": "MOHAMAD ISMANGIL",
            "employeeName": "MOHAMAD ISMANGIL",
            "isActive": 1
        },
        {
            "employeeId": "1332008080041",
            "employeeCode": "MOHAMADIA",
            "employeeName": "MOHAMADIA",
            "isActive": 1
        },
        {
            "employeeId": "1332009060013",
            "employeeCode": "MOHAMED ANUARY B A",
            "employeeName": "MOHAMED ANUARY BIN ARIFFIN",
            "isActive": 1
        },
        {
            "employeeId": "9999120520160014",
            "employeeCode": "MOHAMED ASLAM HAROON",
            "employeeName": "MOHAMED ASLAM HAROON",
            "isActive": 1
        },
        {
            "employeeId": "999130920140008",
            "employeeCode": "MOHAMED NAIM",
            "employeeName": "MOHAMED NAIM BIN ABDUL KARIM",
            "isActive": 1
        },
        {
            "employeeId": "1332009050001",
            "employeeCode": "MOHAMED RAHARAHMUDI",
            "employeeName": "MOHAMED RAHARAHMUDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080009",
            "employeeCode": "MOHAMED RUSLI",
            "employeeName": "MOHAMED RUSLI BIN MAHMOOD",
            "isActive": 1
        },
        {
            "employeeId": "1332009120254",
            "employeeCode": "MOHAMMAD AKBAR",
            "employeeName": "MOHAMMAD AKBAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100026",
            "employeeCode": "MOHAMMAD ALFAN",
            "employeeName": "MOHAMMAD ALFAN SAIFULLAH",
            "isActive": 1
        },
        {
            "employeeId": "9999220420160011",
            "employeeCode": "MOHAMMAD HAKI",
            "employeeName": "MOHAMMAD HAKI B. YUSOPE",
            "isActive": 1
        },
        {
            "employeeId": "999190520140003",
            "employeeCode": "MOHAMMAD HOSEN",
            "employeeName": "MOHAMMAD HOSEN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018060010",
            "employeeCode": "M. KABUL LAKSMANA",
            "employeeName": "MOHAMMAD KABUL LAKSMANA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090024",
            "employeeCode": "MOHAMMAD MUSTAALA",
            "employeeName": "MOHAMMAD MUSTAALA BIN MOHAMMAD ISMAIL",
            "isActive": 1
        },
        {
            "employeeId": "9999220420160012",
            "employeeCode": "MOHAMMAD SOPFIAN",
            "employeeName": "MOHAMMAD SOPFIAN B. MOHAMMAD ISMAIL",
            "isActive": 1
        },
        {
            "employeeId": "9999220420160014",
            "employeeCode": "MOHAMMAD SOUFIE",
            "employeeName": "MOHAMMAD SOUFIE B. ABDUL LATIFF",
            "isActive": 1
        },
        {
            "employeeId": "999300420140016",
            "employeeCode": "MOHAMMAD TAUFIK",
            "employeeName": "MOHAMMAD TAUFIK",
            "isActive": 1
        },
        {
            "employeeId": "999300620140008",
            "employeeCode": "MOHAMMAD YASIN",
            "employeeName": "MOHAMMAD YASIN AL PRAMATUH",
            "isActive": 1
        },
        {
            "employeeId": "1332009120270",
            "employeeCode": "ANUARY",
            "employeeName": "MOHD ANUARY BIN ARIFFIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070015",
            "employeeCode": "MOHD HARIHT",
            "employeeName": "MOHD HARIHT",
            "isActive": 1
        },
        {
            "employeeId": "1332008080035",
            "employeeCode": "MOHD HARIHT BIN JUFF",
            "employeeName": "MOHD HARIHT BIN JUFFRI",
            "isActive": 1
        },
        {
            "employeeId": "1332009120269",
            "employeeCode": "HAZIQ",
            "employeeName": "MOHD HAZIQ BIN KAMARUDIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100012",
            "employeeCode": "MOHD KABUL LAKSMANA",
            "employeeName": "MOHD KABUL LAKSMANA",
            "isActive": 1
        },
        {
            "employeeId": "1332009070014",
            "employeeCode": "MOHD RIDWAN",
            "employeeName": "MOHD RIDWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999020320160002",
            "employeeCode": "MOHD SANUSI",
            "employeeName": "MOHD SANUSI BIN MANSOOR",
            "isActive": 1
        },
        {
            "employeeId": "1332010120004",
            "employeeCode": "MOKH.SA'DULLAH",
            "employeeName": "MOKH.SA'DULLAH",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110012",
            "employeeCode": "MONCI NYELO",
            "employeeName": "MONCI NYELO",
            "isActive": 1
        },
        {
            "employeeId": "1332008100012",
            "employeeCode": "MORRI RAJAGUGUK",
            "employeeName": "MORRI RAJAGUGUK",
            "isActive": 1
        },
        {
            "employeeId": "1332010010007",
            "employeeCode": "MUCHAMAD NOOR",
            "employeeName": "MUCHAMAD NOOR",
            "isActive": 1
        },
        {
            "employeeId": "1332010050008",
            "employeeCode": "MUCHYIYIN DIDIK",
            "employeeName": "MUCHYIYIN DIDIK",
            "isActive": 1
        },
        {
            "employeeId": "1332010010002",
            "employeeCode": "MUDHAMAD NOOR EFENDI",
            "employeeName": "MUDHAMAD NOOR EFENDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008110028",
            "employeeCode": "MUDJIANTO",
            "employeeName": "MUDJIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080002",
            "employeeCode": "MUHAMAD",
            "employeeName": "MUHAMAD",
            "isActive": 1
        },
        {
            "employeeId": "1332008080061",
            "employeeCode": "MUHAMAD ANDARMANTO",
            "employeeName": "MUHAMAD ANDARMANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010017",
            "employeeCode": "MUHAMAD CHUSNI",
            "employeeName": "MUHAMAD CHUSNI TAMRIN",
            "isActive": 1
        },
        {
            "employeeId": "1332019060003",
            "employeeCode": "FADLI",
            "employeeName": "MUHAMAD FADLI",
            "isActive": 1
        },
        {
            "employeeId": "1332008110011",
            "employeeCode": "MUHAMAD I NASUTION",
            "employeeName": "MUHAMAD I NASUTION",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100019",
            "employeeCode": "MUHAMAD IDRIS",
            "employeeName": "MUHAMAD IDRIS",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010018",
            "employeeCode": "MUHAMAD KAMDI",
            "employeeName": "MUHAMAD KAMDI",
            "isActive": 1
        },
        {
            "employeeId": "1332009120272",
            "employeeCode": "MUHAMAD NOOR EFENDI",
            "employeeName": "MUHAMAD NOOR EFENDI",
            "isActive": 1
        },
        {
            "employeeId": "1332010050011",
            "employeeCode": "MUHAMAD PESSY",
            "employeeName": "MUHAMAD PESSY",
            "isActive": 1
        },
        {
            "employeeId": "1332012100002",
            "employeeCode": "MUHAMAD RIYANTO",
            "employeeName": "MUHAMAD RIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "999300420140018",
            "employeeCode": "MUHAMAD SULAEMAN",
            "employeeName": "MUHAMAD SULAEMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008080058",
            "employeeCode": "MUHAMAD SYAKUR",
            "employeeName": "MUHAMAD SYAKUR",
            "isActive": 1
        },
        {
            "employeeId": "1332008110026",
            "employeeCode": "MUHAMAD Z ARIFIN N",
            "employeeName": "MUHAMAD ZAENAL ARIFIN NASUTION",
            "isActive": 1
        },
        {
            "employeeId": "1332009030002",
            "employeeCode": "MUHAMMAD ABDUH",
            "employeeName": "MUHAMMAD ABDUH",
            "isActive": 1
        },
        {
            "employeeId": "1332009120255",
            "employeeCode": "MUHAMMAD AKBAR",
            "employeeName": "MUHAMMAD AKBAR",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120004",
            "employeeCode": "MUHAMMAD AL-AZIM",
            "employeeName": "MUHAMMAD AL-AZIM BIN AZIZ",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120014",
            "employeeCode": "MUHAMMAD ARFAN",
            "employeeName": "MUHAMMAD ARFAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100005",
            "employeeCode": "MUHAMMAD ASLAM",
            "employeeName": "MUHAMMAD ASLAM BIN MOHAMED KHADER",
            "isActive": 1
        },
        {
            "employeeId": "999060620140002",
            "employeeCode": "MUHAMMAD BIN HASSAN",
            "employeeName": "MUHAMMAD BIN HASSAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050011",
            "employeeCode": "MUHAMMAD BIN ROSLI",
            "employeeName": "MUHAMMAD BIN ROSLI",
            "isActive": 1
        },
        {
            "employeeId": "1332009070007",
            "employeeCode": "MUHAMMAD DAFI",
            "employeeName": "MUHAMMAD DAFI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090047",
            "employeeCode": "MUHAMMAD F.B.A.L",
            "employeeName": "MUHAMMAD F.B ABDUL LATIFF",
            "isActive": 1
        },
        {
            "employeeId": "1332019060004",
            "employeeCode": "MUHAMMAD FAHMI",
            "employeeName": "MUHAMMAD FAHMI BIN MOHAMED ALI",
            "isActive": 1
        },
        {
            "employeeId": "1332020110001",
            "employeeCode": "MUHAMMAD FAISAL",
            "employeeName": "MUHAMMAD FAISAL BIN KAMIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020005",
            "employeeCode": "MUHAMMAD FAJAR ASPA",
            "employeeName": "MUHAMMAD FAJAR ASPA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090022",
            "employeeCode": "MUHAMMAD FARSYUN BIN",
            "employeeName": "MUHAMMAD FARSYUN BIN BAHTIAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080018",
            "employeeCode": "MUHAMMAD FAUZI",
            "employeeName": "MUHAMMAD FAUZI BIN OSMAN",
            "isActive": 1
        },
        {
            "employeeId": "9999011220150001",
            "employeeCode": "MUHAMMAD FEHRIN",
            "employeeName": "MUHAMMAD FEHRIN BIN ABDUL RAHIM",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110007",
            "employeeCode": "MUHD HAIRULNIZAM",
            "employeeName": "MUHAMMAD HAIRULNIZAM BIN SAMAD",
            "isActive": 1
        },
        {
            "employeeId": "1332008080063",
            "employeeCode": "MUHAMMAD IKSAN MALIK",
            "employeeName": "MUHAMMAD IKSAN MALIK",
            "isActive": 1
        },
        {
            "employeeId": "9999110220150004",
            "employeeCode": "MUHAMMAD IZWAN",
            "employeeName": "MUHAMMAD IZWAN BIN ASHARI",
            "isActive": 1
        },
        {
            "employeeId": "9999332016090016",
            "employeeCode": "MUHAMMAD JAMIL",
            "employeeName": "MUHAMMAD JAMIL",
            "isActive": 1
        },
        {
            "employeeId": "1332008080004",
            "employeeCode": "MUHAMMAD NATAS JANIA",
            "employeeName": "MUHAMMAD NATAS JANIANSYH",
            "isActive": 1
        },
        {
            "employeeId": "9999220420160015",
            "employeeCode": "MUHAMMAD NAWAWI",
            "employeeName": "MUHAMMAD NAWAWI B. NASRAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040007",
            "employeeCode": "MUHAMMAD NORAZLI",
            "employeeName": "MUHAMMAD NORAZLI BIN RAZALI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100004",
            "employeeCode": "MUHD. NUR FIRHAUS",
            "employeeName": "MUHAMMAD NUR FIRDHAUS BIN ROSLI",
            "isActive": 1
        },
        {
            "employeeId": "9999040520160012",
            "employeeCode": "MUHD. NUR FIRDHUAS",
            "employeeName": "MUHAMMAD NUR FIRDHUAS BIN ROSLI",
            "isActive": 1
        },
        {
            "employeeId": "1332010070031",
            "employeeCode": "MUHAMMAD RUSLAN",
            "employeeName": "MUHAMMAD RUSLAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060008",
            "employeeCode": "MUHAMMAD SULHAN",
            "employeeName": "MUHAMMAD SULHAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010010",
            "employeeCode": "MUHAMMAD SYAHRENDRA",
            "employeeName": "MUHAMMAD SYAHRENDRA",
            "isActive": 1
        },
        {
            "employeeId": "999211020140005",
            "employeeCode": "MUHAMMAD ZAHIN",
            "employeeName": "MUHAMMAD ZAHIN BIN JUMAHAT",
            "isActive": 1
        },
        {
            "employeeId": "1332011060009",
            "employeeCode": "MUHIBBIN ISKANDAR",
            "employeeName": "MUHIBBIN ISKANDAR MAULANA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120003",
            "employeeCode": "MUHAMMAD SHAFIEE",
            "employeeName": "MUHMMAD SHAFIEE BIN NOH",
            "isActive": 1
        },
        {
            "employeeId": "1332010070011",
            "employeeCode": "MUJIANTO",
            "employeeName": "MUJIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009060001",
            "employeeCode": "MUKHOLID",
            "employeeName": "MUKHOLID",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090005",
            "employeeCode": "MUKTAR AMBUI",
            "employeeName": "MUKTAR AMBUI",
            "isActive": 1
        },
        {
            "employeeId": "99992016070011",
            "employeeCode": "MULIANTO",
            "employeeName": "MULIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090004",
            "employeeCode": "MUSLIHIM",
            "employeeName": "MUSLIHIM",
            "isActive": 1
        },
        {
            "employeeId": "1332011040008",
            "employeeCode": "MUZAKKI",
            "employeeName": "MUZAKKI",
            "isActive": 1
        },
        {
            "employeeId": "1332011020009",
            "employeeCode": "MY ZAW HEIN",
            "employeeName": "MY ZAW HEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011030002",
            "employeeCode": "MYA MIN",
            "employeeName": "MYA MIN",
            "isActive": 1
        },
        {
            "employeeId": "1332011050001",
            "employeeCode": "MYAT KO",
            "employeeName": "MYAT KO",
            "isActive": 1
        },
        {
            "employeeId": "999020720140001",
            "employeeCode": "MYAT THAN ZIN",
            "employeeName": "MYAT THAN ZIN",
            "isActive": 1
        },
        {
            "employeeId": "999090720140005",
            "employeeCode": "MYAT THAW ZIN",
            "employeeName": "MYAT THAW ZIN",
            "isActive": 1
        },
        {
            "employeeId": "9999150620160006",
            "employeeCode": "MYAT THU SAN",
            "employeeName": "MYAT THU SAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080008",
            "employeeCode": "MYINT AUNG",
            "employeeName": "MYINT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009100009",
            "employeeCode": "MYINT LWIN",
            "employeeName": "MYINT LWIN",
            "isActive": 1
        },
        {
            "employeeId": "1332010100003",
            "employeeCode": "MYINT OO",
            "employeeName": "MYINT OO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030014",
            "employeeCode": "MYINT THU",
            "employeeName": "MYINT THU",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020018",
            "employeeCode": "MYO HAN",
            "employeeName": "MYO HAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110002",
            "employeeCode": "MYO HTUN",
            "employeeName": "MYO HTUN",
            "isActive": 1
        },
        {
            "employeeId": "999170120150011",
            "employeeCode": "MYO KYAW KHIN",
            "employeeName": "MYO KYAW KHIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030005",
            "employeeCode": "MYO KYI",
            "employeeName": "MYO KYI",
            "isActive": 1
        },
        {
            "employeeId": "1332008110019",
            "employeeCode": "MYO MAUNG MAUNG",
            "employeeName": "MYO MAUNG MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110014",
            "employeeCode": "MYO MIN AUNG",
            "employeeName": "MYO MIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332011050004",
            "employeeCode": "MYO MYINT AUNG",
            "employeeName": "MYO MYINT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110007",
            "employeeCode": "MYO MYINT MYAT",
            "employeeName": "MYO MYINT MYAT",
            "isActive": 1
        },
        {
            "employeeId": "1332010120006",
            "employeeCode": "MYO MYINT THU",
            "employeeName": "MYO MYINT THU",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070001",
            "employeeCode": "MYO NYUNT OO",
            "employeeName": "MYO NYUNT OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110008",
            "employeeCode": "MYO OO THAN",
            "employeeName": "MYO OO THAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010020007",
            "employeeCode": "MYO ZAW HEIN",
            "employeeName": "MYO ZAW HEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009030018",
            "employeeCode": "NAING LIN TUN",
            "employeeName": "NAING LIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080014",
            "employeeCode": "NAING LWIN MING",
            "employeeName": "NAING LWIN MING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040031",
            "employeeCode": "NAING WIN",
            "employeeName": "NAING WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332010010013",
            "employeeCode": "NANAN ABDUL ROHMAN",
            "employeeName": "NANAN ABDUL ROHMAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030001",
            "employeeCode": "NANANG ROMANSYAH",
            "employeeName": "NANANG ROMANSYAH",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100002",
            "employeeCode": "NANDA DYAN PRADANA",
            "employeeName": "NANDA DYAN PRADANA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050014",
            "employeeCode": "NARIYAN",
            "employeeName": "NARIYAN ABDUL HAMID ABDUL MAJEED",
            "isActive": 1
        },
        {
            "employeeId": "1332009040019",
            "employeeCode": "NARLI SIMBOLON",
            "employeeName": "NARLI SIMBOLON",
            "isActive": 1
        },
        {
            "employeeId": "9991332012040007",
            "employeeCode": "NAY HLAING OO",
            "employeeName": "NAY HLAING OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110001",
            "employeeCode": "NAY LIN SOE",
            "employeeName": "NAY LIN SOE",
            "isActive": 1
        },
        {
            "employeeId": "1332009100030",
            "employeeCode": "NAY MOE TUN",
            "employeeName": "NAY MOE TUN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070006",
            "employeeCode": "NAY MYO HTET",
            "employeeName": "NAY MYO HTET",
            "isActive": 1
        },
        {
            "employeeId": "999220720140010",
            "employeeCode": "NAY WIN MYINT",
            "employeeName": "NAY WIN MYINT",
            "isActive": 1
        },
        {
            "employeeId": "1332008070010",
            "employeeCode": "NAY WIN NAING",
            "employeeName": "NAY WIN NAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030004",
            "employeeCode": "NAYON RAMOS BASILIO",
            "employeeName": "NAYON RAMOS BASILIO JR",
            "isActive": 1
        },
        {
            "employeeId": "1332008090015",
            "employeeCode": "NELSON SINADIA",
            "employeeName": "NELSON SINADIA",
            "isActive": 1
        },
        {
            "employeeId": "1332011060008",
            "employeeCode": "NELSON",
            "employeeName": "NELSON,NEO LIH CHONG",
            "isActive": 1
        },
        {
            "employeeId": "1332011010016",
            "employeeCode": "NEO JIN CHUAN",
            "employeeName": "NEO JIN CHUAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120016",
            "employeeCode": "NG FOO LEONG , LEON",
            "employeeName": "NG FOO LEONG , LEON",
            "isActive": 1
        },
        {
            "employeeId": "999111220120016",
            "employeeCode": "NG FU LEONG , LEON",
            "employeeName": "NG FU LEONG , LEON",
            "isActive": 1
        },
        {
            "employeeId": "1332008080009",
            "employeeCode": "NG GIM HUA",
            "employeeName": "NG GIM HUA",
            "isActive": 1
        },
        {
            "employeeId": "13308050008",
            "employeeCode": "BENNY",
            "employeeName": "NG HOCK TECK, BENNY",
            "isActive": 1
        },
        {
            "employeeId": "1332008070011",
            "employeeCode": "NG HOW PIN",
            "employeeName": "NG HOW PIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017100001",
            "employeeCode": "NG HUI JIE",
            "employeeName": "NG HUI JIE",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110006",
            "employeeCode": "NG KAR HENG",
            "employeeName": "NG KAR HENG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020014",
            "employeeCode": "ADAM NG KIAN LAM",
            "employeeName": "NG KIAN LAM, ADAM",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070017",
            "employeeCode": "ESPY NG",
            "employeeName": "NG SUAT PENG, ESPY",
            "isActive": 1
        },
        {
            "employeeId": "1332019060005",
            "employeeCode": "ALEX NG",
            "employeeName": "NG SWEE GUAN, ALEX",
            "isActive": 1
        },
        {
            "employeeId": "9991332012090007",
            "employeeCode": "NG WEE KIAT ALOYSIUS",
            "employeeName": "NG WEE KIAT, ALOYSIUS",
            "isActive": 1
        },
        {
            "employeeId": "9999211120150008",
            "employeeCode": "WILLARD NG",
            "employeeName": "NG ZHENG YANG, WILLARD",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040025",
            "employeeCode": "NGIO CHIN HUI DANNY",
            "employeeName": "NGIO CHIN HUI DANNY",
            "isActive": 1
        },
        {
            "employeeId": "1332009030005",
            "employeeCode": "NICHOLAS FERNANDO",
            "employeeName": "NICHOLAS FERNANDO",
            "isActive": 1
        },
        {
            "employeeId": "9999170320150005",
            "employeeCode": "NICKO PRANOTO",
            "employeeName": "NICKO PRANOTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011120003",
            "employeeCode": "NIKO SUSUNTO",
            "employeeName": "NIKO SUSUNTO",
            "isActive": 1
        },
        {
            "employeeId": "1332013030006",
            "employeeCode": "NILO BASUKI",
            "employeeName": "NILO BASUKI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090034",
            "employeeCode": "NIXON MOHOGI KANTOHE",
            "employeeName": "NIXON MOHOGI KANTOHE",
            "isActive": 1
        },
        {
            "employeeId": "1332008100015",
            "employeeCode": "NOLDY LOTULUNG",
            "employeeName": "NOLDY LOTULUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090003",
            "employeeCode": "NOR AZMAN MOHD YUNES",
            "employeeName": "NOR AZMAN BIN MOHD YUNES",
            "isActive": 1
        },
        {
            "employeeId": "1332010010006",
            "employeeCode": "NORBERTO JR RUBA",
            "employeeName": "NORBERTO JR RUBA FONTBUENA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090001",
            "employeeCode": "NORDIN BACHA",
            "employeeName": "NORDIN BACHA",
            "isActive": 1
        },
        {
            "employeeId": "1332013030003",
            "employeeCode": "NOVA RUDIANA",
            "employeeName": "NOVA RUDIANA",
            "isActive": 1
        },
        {
            "employeeId": "1332009120265",
            "employeeCode": "NUGROHO WIKYU TRIONO",
            "employeeName": "NUGROHO WIKYU TRIONO",
            "isActive": 1
        },
        {
            "employeeId": "1332008120004",
            "employeeCode": "NUR CAHYONO",
            "employeeName": "NUR CAHYONO",
            "isActive": 1
        },
        {
            "employeeId": "1332010060009",
            "employeeCode": "NURHUDA",
            "employeeName": "NURHUDA",
            "isActive": 1
        },
        {
            "employeeId": "1332008100020",
            "employeeCode": "NURSAN",
            "employeeName": "NURSAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080006",
            "employeeCode": "NURSANI",
            "employeeName": "NURSANI",
            "isActive": 1
        },
        {
            "employeeId": "1332011020001",
            "employeeCode": "NURSIDI",
            "employeeName": "NURSIDI",
            "isActive": 1
        },
        {
            "employeeId": "9999270820150012",
            "employeeCode": "NURUL HIDAYAT",
            "employeeName": "NURUL HIDAYAT",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050012",
            "employeeCode": "NURYANTO",
            "employeeName": "NURYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050004",
            "employeeCode": "NYAN LYNN HTUN",
            "employeeName": "NYAN LYNN HTUN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020011",
            "employeeCode": "NYAN TUN",
            "employeeName": "NYAN TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332009040009",
            "employeeCode": "NYEIN CHAN",
            "employeeName": "NYEIN CHAN",
            "isActive": 1
        },
        {
            "employeeId": "999300620140010",
            "employeeCode": "NYEIN CHAN AUNG",
            "employeeName": "NYEIN CHAN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080004",
            "employeeCode": "NYI NYAR NA AUNG",
            "employeeName": "NYI NYAR NA AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009010010",
            "employeeCode": "NYI NYI OO",
            "employeeName": "NYI NYI OO",
            "isActive": 1
        },
        {
            "employeeId": "1332010100004",
            "employeeCode": "NYUNT HTWE",
            "employeeName": "NYUNT HTWE",
            "isActive": 1
        },
        {
            "employeeId": "9999040520160008",
            "employeeCode": "OBETH IMANUEL",
            "employeeName": "OBETH IMANUEL TIBLOLA",
            "isActive": 1
        },
        {
            "employeeId": "1332008090040",
            "employeeCode": "OCTORY ISHAK SAMPELA",
            "employeeName": "OCTORY ISHAK SAMPELAN",
            "isActive": 1
        },
        {
            "employeeId": "1332013020009",
            "employeeCode": "OKTOWANTO",
            "employeeName": "OKTOWANTO",
            "isActive": 1
        },
        {
            "employeeId": "99992016070013",
            "employeeCode": "OLDRIAN I. MANUMPAHI",
            "employeeName": "OLDRIAN I. MANUMPAHI",
            "isActive": 1
        },
        {
            "employeeId": "99992016070014",
            "employeeCode": "OLDRIAN I. MANUMPAHI",
            "employeeName": "OLDRIAN IRVAN MANUMPAHI",
            "isActive": 1
        },
        {
            "employeeId": "999171220140006",
            "employeeCode": "RIORDAN ONG",
            "employeeName": "ONG RUI RONG, RIORDAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009090006",
            "employeeCode": "ONG SIOW KEONG",
            "employeeName": "ONG SIOW KEONG",
            "isActive": 1
        },
        {
            "employeeId": "1332010090003",
            "employeeCode": "ONG SIOW LONG",
            "employeeName": "ONG SIOW LONG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080020",
            "employeeCode": "ONG THIAM HOCK",
            "employeeName": "ONG THIAM HOCK",
            "isActive": 1
        },
        {
            "employeeId": "9999030520160001",
            "employeeCode": "ONG TIONG YEE",
            "employeeName": "ONG TIONG YEE",
            "isActive": 1
        },
        {
            "employeeId": "1332010050004",
            "employeeCode": "ONY SURYANTO",
            "employeeName": "ONY SURYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332019080001",
            "employeeCode": "TERRY OOI",
            "employeeName": "OOI KOK JIN, TERRY",
            "isActive": 1
        },
        {
            "employeeId": "1332009060007",
            "employeeCode": "OSCAR BARCENA GARDE",
            "employeeName": "OSCAR BARCENA GARDE",
            "isActive": 1
        },
        {
            "employeeId": "1332009060008",
            "employeeCode": "OSCAR BARCENA GARDE",
            "employeeName": "OSCAR BARCENA GARDE",
            "isActive": 1
        },
        {
            "employeeId": "1332008090002",
            "employeeCode": "PADIL BIN ADON",
            "employeeName": "PADIL BIN ADON",
            "isActive": 1
        },
        {
            "employeeId": "13308050009",
            "employeeCode": "NICOLE PAI",
            "employeeName": "PAI GUAT MOOI, NICOLE",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090001",
            "employeeCode": "PAI HONG BOON",
            "employeeName": "PAI HONG BOON",
            "isActive": 1
        },
        {
            "employeeId": "9999250320150009",
            "employeeCode": "PAI HONG KOON",
            "employeeName": "PAI HONG KOON",
            "isActive": 1
        },
        {
            "employeeId": "9999332017060003",
            "employeeCode": "PAI HONG WEI",
            "employeeName": "PAI HONG WEI",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090041",
            "employeeCode": "PAI HONG WEN",
            "employeeName": "PAI HONG WEN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090019",
            "employeeCode": "PAI HONG XIANG",
            "employeeName": "PAI HONG XIANG",
            "isActive": 1
        },
        {
            "employeeId": "999290120150012",
            "employeeCode": "PAI HONG YAO",
            "employeeName": "PAI HONG YAO",
            "isActive": 1
        },
        {
            "employeeId": "13308050010",
            "employeeCode": "PAI KENG PHENG",
            "employeeName": "PAI KENG PHENG",
            "isActive": 1
        },
        {
            "employeeId": "13308050002",
            "employeeCode": "PAI KHENG HIAN",
            "employeeName": "PAI KHENG HIAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070004",
            "employeeCode": "PAI KIM TECK",
            "employeeName": "PAI KIM TECK",
            "isActive": 1
        },
        {
            "employeeId": "1332009070005",
            "employeeCode": "PAI KIM TECK",
            "employeeName": "PAI KIM TECK",
            "isActive": 1
        },
        {
            "employeeId": "1332008100008",
            "employeeCode": "PAILUS ARRUAN",
            "employeeName": "PAILUS ARRUAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080008",
            "employeeCode": "PAING HTET SOE",
            "employeeName": "PAING HTET SOE",
            "isActive": 1
        },
        {
            "employeeId": "1332011010014",
            "employeeCode": "PAN DE WEI",
            "employeeName": "PAN DE WEI",
            "isActive": 1
        },
        {
            "employeeId": "1332015060001",
            "employeeCode": "PAN HONG YU",
            "employeeName": "PAN HONG YU",
            "isActive": 1
        },
        {
            "employeeId": "1332008080045",
            "employeeCode": "PANDER HARIANJA",
            "employeeName": "PANDER HARIANJA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010010",
            "employeeCode": "PAREMED DANIEL",
            "employeeName": "PAREMED DANIEL",
            "isActive": 1
        },
        {
            "employeeId": "1332008070031",
            "employeeCode": "PARIS TUMBELAKA",
            "employeeName": "PARIS TUMBELAKA",
            "isActive": 1
        },
        {
            "employeeId": "9999280720150018",
            "employeeCode": "PARSAULIAN MARIHAT",
            "employeeName": "PARSAULIAN MARIHAT PANJAITAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009120259",
            "employeeCode": "PATONI RIYADI",
            "employeeName": "PATONI RIYADI",
            "isActive": 1
        },
        {
            "employeeId": "999141120140006",
            "employeeCode": "PATRICK JOSE PAPA",
            "employeeName": "PATRICK JOSE PAPA EVENGELISTA",
            "isActive": 1
        },
        {
            "employeeId": "1332011050012",
            "employeeCode": "PAUL YAKIN",
            "employeeName": "PAUL YAKIN TELAUMBANUA",
            "isActive": 1
        },
        {
            "employeeId": "1332008100009",
            "employeeCode": "PAULUS AGUNG",
            "employeeName": "PAULUS AGUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080062",
            "employeeCode": "PAULUS AGUNG PRABOWO",
            "employeeName": "PAULUS AGUNG PRABOWO",
            "isActive": 1
        },
        {
            "employeeId": "1332008120017",
            "employeeCode": "PAULUS ARRUAN",
            "employeeName": "PAULUS ARRUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010100008",
            "employeeCode": "PAWANG GUNENDRO",
            "employeeName": "PAWANG GUNENDRO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110003",
            "employeeCode": "PAYE PHYO HAN",
            "employeeName": "PAYE PHYO HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110009",
            "employeeCode": "PE THEIN",
            "employeeName": "PE THEIN",
            "isActive": 1
        },
        {
            "employeeId": "999200920140011",
            "employeeCode": "PEH CHONG KIAN",
            "employeeName": "PEH CHONG KIAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010120003",
            "employeeCode": "PEH LEE NEO",
            "employeeName": "PEH LEE NEO",
            "isActive": 1
        },
        {
            "employeeId": "1332010030016",
            "employeeCode": "PENDI EPENDI",
            "employeeName": "PENDI EPENDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080014",
            "employeeCode": "PETER ANG",
            "employeeName": "PETER ANG",
            "isActive": 1
        },
        {
            "employeeId": "9999210620160007",
            "employeeCode": "PETER JACKSON",
            "employeeName": "PETER JACKSON",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100007",
            "employeeCode": "PHAE PHYO HEIN",
            "employeeName": "PHAE PHYO HEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070011",
            "employeeCode": "PHUA YONG CHING",
            "employeeName": "PHUA YONG CHING",
            "isActive": 1
        },
        {
            "employeeId": "1332009070012",
            "employeeCode": "PHUA YONG HOCK",
            "employeeName": "PHUA YONG HOCK",
            "isActive": 1
        },
        {
            "employeeId": "1332009070024",
            "employeeCode": "PHYE PHYO THANT",
            "employeeName": "PHYE PHYO THANT",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090016",
            "employeeCode": "PHYO KO KO",
            "employeeName": "PHYO KO KO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030010",
            "employeeCode": "PHYO KO KO HTWE",
            "employeeName": "PHYO KO KO HTWE",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040010",
            "employeeCode": "PHYO MIN AUNG",
            "employeeName": "PHYO MIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332018060006",
            "employeeCode": "PHYO MIN MAUNG",
            "employeeName": "PHYO MIN MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008110013",
            "employeeCode": "PHYO MIN SAN",
            "employeeName": "PHYO MIN SAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020003",
            "employeeCode": "PHYO PHYO AUNG",
            "employeeName": "PHYO PHYO AUNG",
            "isActive": 1
        },
        {
            "employeeId": "999190520140002",
            "employeeCode": "PHYO THU REIN",
            "employeeName": "PHYO THU REIN MYINT THEIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030006",
            "employeeCode": "PHYO WAI HAN",
            "employeeName": "PHYO WAI HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050022",
            "employeeCode": "PHYO WAI LWIN",
            "employeeName": "PHYO WAI LWIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070004",
            "employeeCode": "PITHER KAPA",
            "employeeName": "PITHER KAPA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080009",
            "employeeCode": "POH LONG TAT",
            "employeeName": "POH LONG TAT",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100020",
            "employeeCode": "POH LYE HUAT, BEN",
            "employeeName": "POH LYE HUAT, BEN",
            "isActive": 1
        },
        {
            "employeeId": "1332020040002",
            "employeeCode": "BEN POH",
            "employeeName": "POH LYE HUAT, BEN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110005",
            "employeeCode": "POQUIZ BRYAN CALIZO",
            "employeeName": "POQUIZ BRYAN CALIZO",
            "isActive": 1
        },
        {
            "employeeId": "1332011010001",
            "employeeCode": "PRADANA HADI WIJAYA",
            "employeeName": "PRADANA HADI WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "1332011010013",
            "employeeCode": "PUAY SI HAO",
            "employeeName": "PUAY SI HAO",
            "isActive": 1
        },
        {
            "employeeId": "1332008090029",
            "employeeCode": "PUENOH DALIE",
            "employeeName": "PUENOH DAILIE",
            "isActive": 1
        },
        {
            "employeeId": "1332008080053",
            "employeeCode": "PURNOMO",
            "employeeName": "PURNOMO",
            "isActive": 1
        },
        {
            "employeeId": "999170920140009",
            "employeeCode": "PYAE PAING SOE",
            "employeeName": "PYAE PAING SOE",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110015",
            "employeeCode": "PYAE PHYO AUNG",
            "employeeName": "PYAE PHYO AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009030025",
            "employeeCode": "PYAE PHYO HAN",
            "employeeName": "PYAE PHYO HAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100008",
            "employeeCode": "PYAE PHYO HEIN",
            "employeeName": "PYAE PHYO HEIN",
            "isActive": 1
        },
        {
            "employeeId": "9999120520160016",
            "employeeCode": "PYAE PHYO KYAW",
            "employeeName": "PYAE PHYO KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9999040520160002",
            "employeeCode": "PYAE PHYO LWIN",
            "employeeName": "PYAE PHYO LWIN",
            "isActive": 1
        },
        {
            "employeeId": "9999020220160001",
            "employeeCode": "PYI THEINN KYAW",
            "employeeName": "PYI THEINN KYAW",
            "isActive": 1
        },
        {
            "employeeId": "1332010020013",
            "employeeCode": "PYREL AGUS TRIONO",
            "employeeName": "PYREL AGUS TRIONO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060006",
            "employeeCode": "QIAN JIAN KUI",
            "employeeName": "QIAN JIAN KUI",
            "isActive": 1
        },
        {
            "employeeId": "9991332010090004",
            "employeeCode": "QUEK CHENG BOCK",
            "employeeName": "QUEK CHENG BOCK",
            "isActive": 1
        },
        {
            "employeeId": "1332010090004",
            "employeeCode": "QUEK CHENG BOK",
            "employeeName": "QUEK CHENG BOK",
            "isActive": 1
        },
        {
            "employeeId": "1332010070038",
            "employeeCode": "QUIRICO JR",
            "employeeName": "QUIRICO JR CAGUIMBAGA",
            "isActive": 1
        },
        {
            "employeeId": "1332010020017",
            "employeeCode": "R SRI PUJO",
            "employeeName": "R SRI PUJO WIJAYANTO H",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090011",
            "employeeCode": "RACHMAN HASIBUAN",
            "employeeName": "RACHMAN HASIBUAN",
            "isActive": 1
        },
        {
            "employeeId": "99992016090010",
            "employeeCode": "RADEN AHMAD RAFIQ",
            "employeeName": "RADEN AHMAD RAFIQ",
            "isActive": 1
        },
        {
            "employeeId": "1332009100020",
            "employeeCode": "RADOMAR MAIHAILOVIC",
            "employeeName": "RADOMAR MAIHAILOVIC",
            "isActive": 1
        },
        {
            "employeeId": "1332011030009",
            "employeeCode": "RAGIL SETIAWAN",
            "employeeName": "RAGIL SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9999100420150003",
            "employeeCode": "RAGIL TEGUH PRIYANTO",
            "employeeName": "RAGIL TEGUH PRIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009120274",
            "employeeCode": "RAGIL TIRTO AJI",
            "employeeName": "RAGIL TIRTO AJI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100027",
            "employeeCode": "RAHAYUDIN ARIFIN",
            "employeeName": "RAHAYUDIN ARIFIN",
            "isActive": 1
        },
        {
            "employeeId": "999300620140006",
            "employeeCode": "RAHMAD DIANTO",
            "employeeName": "RAHMAD DIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010100007",
            "employeeCode": "RAHMAN BIN ABDULLAH",
            "employeeName": "RAHMAN BIN ABDULLAH",
            "isActive": 1
        },
        {
            "employeeId": "1332010120005",
            "employeeCode": "RAHMAN BIN AWANG",
            "employeeName": "RAHMAN BIN AWANG",
            "isActive": 1
        },
        {
            "employeeId": "1332012030007",
            "employeeCode": "RAHMAN HASIBUAN",
            "employeeName": "RAHMAN HASIBUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008080006",
            "employeeCode": "RAHMAT ARIEF RAHADI",
            "employeeName": "RAHMAT ARIEF RAHADI",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080023",
            "employeeCode": "RAMIT DAVE SINGH",
            "employeeName": "RAMIT DAVE SINGH",
            "isActive": 1
        },
        {
            "employeeId": "1332014020003",
            "employeeCode": "RAMIT DAVIS",
            "employeeName": "RAMIT DAVIS",
            "isActive": 1
        },
        {
            "employeeId": "99992016090011",
            "employeeCode": "RAMLAN GUNI",
            "employeeName": "RAMLAN GUNI",
            "isActive": 1
        },
        {
            "employeeId": "1332009050010",
            "employeeCode": "RAY SETIAWAN",
            "employeeName": "RAY SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070040",
            "employeeCode": "RAYMOND MATINDAS",
            "employeeName": "RAYMOND MATINDAS",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110012",
            "employeeCode": "RAYMOND TINUNGKI",
            "employeeName": "RAYMOND TINUNGKI",
            "isActive": 1
        },
        {
            "employeeId": "1332010070035",
            "employeeCode": "RDHAUS ADNAN ASSANI",
            "employeeName": "RDHAUS ADNAN ASSANI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080037",
            "employeeCode": "RECKI RAHADIKA",
            "employeeName": "RECKI RAHADIKA",
            "isActive": 1
        },
        {
            "employeeId": "1332011010012",
            "employeeCode": "REFLIS",
            "employeeName": "REFLIS",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100011",
            "employeeCode": "REKSA HARIADI",
            "employeeName": "REKSA HARIADI SUBAGYA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013100003",
            "employeeCode": "RENATO BANAGA",
            "employeeName": "RENATO BANAGA FABROS",
            "isActive": 1
        },
        {
            "employeeId": "9999040420160003",
            "employeeCode": "RENOLD A. MANURUNG",
            "employeeName": "RENOLD ALBERTUS MANURUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012050002",
            "employeeCode": "RETNO ANDIKA",
            "employeeName": "RETNO ANDIKA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012050001",
            "employeeCode": "RETNO SUNDOWO",
            "employeeName": "RETNO SUNDOWO",
            "isActive": 1
        },
        {
            "employeeId": "999111020140003",
            "employeeCode": "REVALDI SULISTIYO",
            "employeeName": "REVALDI SULISTIYO",
            "isActive": 1
        },
        {
            "employeeId": "9999040520160009",
            "employeeCode": "REZA YULIZAR",
            "employeeName": "REZA YULIZAR",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070008",
            "employeeCode": "REZKI NURCAHYO",
            "employeeName": "REZKI NURCAHYO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010006",
            "employeeCode": "RICHARD LATSAR",
            "employeeName": "RICHARD LATSAR NAMARUBESSY",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120009",
            "employeeCode": "RICHO",
            "employeeName": "RICHO",
            "isActive": 1
        },
        {
            "employeeId": "9999250220150006",
            "employeeCode": "RICKY IRIYAN",
            "employeeName": "RICKY IRIYAN NOOR IFANSYAH",
            "isActive": 1
        },
        {
            "employeeId": "1332008090001",
            "employeeCode": "RICKY KANG",
            "employeeName": "RICKY KANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080005",
            "employeeCode": "RICKY ROLAND BOGIAN",
            "employeeName": "RICKY ROLAND BOGIAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080005",
            "employeeCode": "RICO PRATAMA",
            "employeeName": "RICO PRATAMA",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090013",
            "employeeCode": "RIKARDO LAUREN",
            "employeeName": "RIKARDO LAUREN",
            "isActive": 1
        },
        {
            "employeeId": "1332012030003",
            "employeeCode": "RIKARDO LAURENT",
            "employeeName": "RIKARDO LAURENT",
            "isActive": 1
        },
        {
            "employeeId": "1332008120023",
            "employeeCode": "RIKIARJON RAJAGUKGUK",
            "employeeName": "RIKIARJON RAJAGUKGUK",
            "isActive": 1
        },
        {
            "employeeId": "9999060220150003",
            "employeeCode": "RIO H. HERLAMBANG",
            "employeeName": "RIO HANGGA HERLAMANG",
            "isActive": 1
        },
        {
            "employeeId": "1332009010006",
            "employeeCode": "RIRIS V. SULISTIYANI",
            "employeeName": "RIRIS VERY SULISTIYANI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080015",
            "employeeCode": "RISAL RAMBUNG",
            "employeeName": "RISAL RAMBUNG",
            "isActive": 1
        },
        {
            "employeeId": "999061220140002",
            "employeeCode": "RISKAR FADIL NAULI",
            "employeeName": "RISKAR FADIL NAULI",
            "isActive": 1
        },
        {
            "employeeId": "99992016080004",
            "employeeCode": "RISKY BER. SIHOMBING",
            "employeeName": "RISKY BERWAN SIHOMBING",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100008",
            "employeeCode": "RISNO BAWILUKU",
            "employeeName": "RISNO BAWILUKU",
            "isActive": 1
        },
        {
            "employeeId": "1332009100022",
            "employeeCode": "RITCHIE M. REBOTE",
            "employeeName": "RITCHIE MICMIC REBOTE",
            "isActive": 1
        },
        {
            "employeeId": "9999332017060005",
            "employeeCode": "RIVAI AREROS",
            "employeeName": "RIVAI AREROS",
            "isActive": 1
        },
        {
            "employeeId": "99992016070004",
            "employeeCode": "ROBBY LIRPA S.",
            "employeeName": "ROBBY LIRPA SUGIHARTO",
            "isActive": 1
        },
        {
            "employeeId": "1332011040009",
            "employeeCode": "ROBI ADE PUTRA",
            "employeeName": "ROBI ADE PUTRA",
            "isActive": 1
        },
        {
            "employeeId": "1332011040010",
            "employeeCode": "ROBI ADE PUTRA",
            "employeeName": "ROBI ADE PUTRA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030015",
            "employeeCode": "ROBI USMAN",
            "employeeName": "ROBI USMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009100003",
            "employeeCode": "ROBIANTO MATO",
            "employeeName": "ROBIANTO MATO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030002",
            "employeeCode": "RODOLFO CANONIZADO",
            "employeeName": "RODOLFO CANONIZADO SUMALILENG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020002",
            "employeeCode": "ROLAS SIMBOLON",
            "employeeName": "ROLAS SIMBOLON",
            "isActive": 1
        },
        {
            "employeeId": "1332014020010",
            "employeeCode": "ROMMEL ERIKSON",
            "employeeName": "ROMMEL ERIKSON",
            "isActive": 1
        },
        {
            "employeeId": "1332008120020",
            "employeeCode": "ROMMY FELLIX",
            "employeeName": "ROMMY FELLIX",
            "isActive": 1
        },
        {
            "employeeId": "1332010030012",
            "employeeCode": "RONALD E.CONSTANTINO",
            "employeeName": "RONALD E.CONSTANTINO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030013",
            "employeeCode": "RONI",
            "employeeName": "RONI",
            "isActive": 1
        },
        {
            "employeeId": "9999150120160007",
            "employeeCode": "RONY ADE PUTRA",
            "employeeName": "RONY ADE PUTRA",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010015",
            "employeeCode": "RONY STENLY LAOTJI",
            "employeeName": "RONY STENLY LAOTJI",
            "isActive": 1
        },
        {
            "employeeId": "1332011060006",
            "employeeCode": "RONY SUHERY",
            "employeeName": "RONY SUHERY PERANGIN ANGIN",
            "isActive": 1
        },
        {
            "employeeId": "9999280720150014",
            "employeeCode": "ROQUE JAN M. O.",
            "employeeName": "ROQUE JAN MICHAEL OBILLO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080008",
            "employeeCode": "ROY",
            "employeeName": "ROY",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090042",
            "employeeCode": "ROY HARIYANTO",
            "employeeName": "ROY HARIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050020",
            "employeeCode": "ROYNALDI LUBIS",
            "employeeName": "ROYNALDI LUBIS",
            "isActive": 1
        },
        {
            "employeeId": "999270520140005",
            "employeeCode": "RUBY KARNO",
            "employeeName": "RUBY KARNO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070006",
            "employeeCode": "RUDI",
            "employeeName": "RUDI",
            "isActive": 1
        },
        {
            "employeeId": "1332010120007",
            "employeeCode": "RUDIN AMBO",
            "employeeName": "RUDIN AMBO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080012",
            "employeeCode": "RUDIN AMBON",
            "employeeName": "RUDIN AMBON",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100001",
            "employeeCode": "RUDY RICKY MANGKEY",
            "employeeName": "RUDY RICKY MANGKEY",
            "isActive": 1
        },
        {
            "employeeId": "9999332017110001",
            "employeeCode": "RUKAYAHDI",
            "employeeName": "RUKAYAHDI",
            "isActive": 1
        },
        {
            "employeeId": "1332010020004",
            "employeeCode": "RULI SEPTIAWAN",
            "employeeName": "RULI SEPTIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009050011",
            "employeeCode": "RUMADI",
            "employeeName": "RUMADI",
            "isActive": 1
        },
        {
            "employeeId": "1332010070026",
            "employeeCode": "RUNDY SOLIVEN U",
            "employeeName": "RUNDY SOLIVEN U",
            "isActive": 1
        },
        {
            "employeeId": "9999260520150003",
            "employeeCode": "RURY",
            "employeeName": "RURY",
            "isActive": 1
        },
        {
            "employeeId": "1332008100019",
            "employeeCode": "RUSLI WIBAWA",
            "employeeName": "RUSLI WIBAWA",
            "isActive": 1
        },
        {
            "employeeId": "1332011040006",
            "employeeCode": "SABRI",
            "employeeName": "SABRI",
            "isActive": 1
        },
        {
            "employeeId": "9999250220150007",
            "employeeCode": "SAFARUDIN",
            "employeeName": "SAFARUDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009120260",
            "employeeCode": "SAFRIZAL",
            "employeeName": "SAFRIZAL",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020004",
            "employeeCode": "SAHAPI ZAINI",
            "employeeName": "SAHAPI ZAINI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020003",
            "employeeCode": "SAHMIN ACHMAD",
            "employeeName": "SAHMIN ACHMAD",
            "isActive": 1
        },
        {
            "employeeId": "1332011020006",
            "employeeCode": "SAI TIN TUN AUNG",
            "employeeName": "SAI TIN TUN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110012",
            "employeeCode": "SAI WIN THU REIN",
            "employeeName": "SAI WIN THU REIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010010",
            "employeeCode": "SAIFUL BAHRI",
            "employeeName": "SAIFUL BAHRI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080015",
            "employeeCode": "SAIFUL BIN ABIDIN",
            "employeeName": "SAIFUL BIN ABIDIN",
            "isActive": 1
        },
        {
            "employeeId": "9999060720150007",
            "employeeCode": "SAIFULLAH",
            "employeeName": "SAIFULLAH",
            "isActive": 1
        },
        {
            "employeeId": "1332010070017",
            "employeeCode": "SALMET",
            "employeeName": "SALMET",
            "isActive": 1
        },
        {
            "employeeId": "1332010070019",
            "employeeCode": "SALMET",
            "employeeName": "SALMET",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010001",
            "employeeCode": "SAM L. MAKALEW",
            "employeeName": "SAM L. MAKALEW",
            "isActive": 1
        },
        {
            "employeeId": "1332009010011",
            "employeeCode": "SAM LUMEMPOUW",
            "employeeName": "SAM LUMEMPOUW",
            "isActive": 1
        },
        {
            "employeeId": "1332009120262",
            "employeeCode": "SAM MYINT",
            "employeeName": "SAM MYINT",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120011",
            "employeeCode": "SAMIN BIN JAMAL",
            "employeeName": "SAMIN BIN JAMAL",
            "isActive": 1
        },
        {
            "employeeId": "9999121020150002",
            "employeeCode": "SAMSUL ARIF",
            "employeeName": "SAMSUL ARIF",
            "isActive": 1
        },
        {
            "employeeId": "9999332018040007",
            "employeeCode": "SAN LIN AUNG",
            "employeeName": "SAN LIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009120263",
            "employeeCode": "SAN MYINT",
            "employeeName": "SAN MYINT",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050001",
            "employeeCode": "SAN WIN",
            "employeeName": "SAN WIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050002",
            "employeeCode": "SAN WIN",
            "employeeName": "SAN WIN",
            "isActive": 1
        },
        {
            "employeeId": "999131120140005",
            "employeeCode": "SAN WIN KO",
            "employeeName": "SAN WIN KO",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040001",
            "employeeCode": "SANCOKO",
            "employeeName": "SANCOKO",
            "isActive": 1
        },
        {
            "employeeId": "1332008120005",
            "employeeCode": "SANDI TANDU LANGI",
            "employeeName": "SANDI TANDU LANGI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120004",
            "employeeCode": "SANDRI PENUS DAENG",
            "employeeName": "SANDRI PENUS DAENG",
            "isActive": 1
        },
        {
            "employeeId": "1332008070052",
            "employeeCode": "SANTJE T.",
            "employeeName": "SANTJE T.",
            "isActive": 1
        },
        {
            "employeeId": "1332008110018",
            "employeeCode": "SAO KYAW AUNG",
            "employeeName": "SAO KYAW AUNG",
            "isActive": 1
        },
        {
            "employeeId": "999230520140004",
            "employeeCode": "SAPON CHARLES",
            "employeeName": "SAPON CHARLES NEIL DONIS",
            "isActive": 1
        },
        {
            "employeeId": "1332009060006",
            "employeeCode": "SAPTADI RAHARJO",
            "employeeName": "SAPTADI RAHARJO",
            "isActive": 1
        },
        {
            "employeeId": "1332009110006",
            "employeeCode": "SAPTO USODO",
            "employeeName": "SAPTO USODO WARIS DIHONO",
            "isActive": 1
        },
        {
            "employeeId": "1332009090019",
            "employeeCode": "SAROHA SIAHAAN",
            "employeeName": "SAROHA SIAHAAN",
            "isActive": 1
        },
        {
            "employeeId": "99992016090006",
            "employeeCode": "SAW JUNE EDISON",
            "employeeName": "SAW JUNE EDISON",
            "isActive": 1
        },
        {
            "employeeId": "1332011010010",
            "employeeCode": "SAW LWIN",
            "employeeName": "SAW LWIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020008",
            "employeeCode": "SAW MAW TAY LA",
            "employeeName": "SAW MAW TAY LA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090032",
            "employeeCode": "SAW MYO THU RA OO",
            "employeeName": "SAW MYO THU RA OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100008",
            "employeeCode": "SAW OO",
            "employeeName": "SAW OO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090036",
            "employeeCode": "SAW SHAR TUN PHOUNG",
            "employeeName": "SAW SHAR TUN PHOUNG",
            "isActive": 1
        },
        {
            "employeeId": "99992016090008",
            "employeeCode": "SAW YE HTIKE",
            "employeeName": "SAW YE HTIKE",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050003",
            "employeeCode": "SAW YE MYINT NAING",
            "employeeName": "SAW YE MYINT NAING",
            "isActive": 1
        },
        {
            "employeeId": "1332008090008",
            "employeeCode": "SAWAL BIN AZIA",
            "employeeName": "SAWAL BIN AZIA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030008",
            "employeeCode": "SEAH CHIN KWAN",
            "employeeName": "SEAH CHIN KWAN, ALVIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008080017",
            "employeeCode": "SEAN LOO",
            "employeeName": "SEAN LOO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070010",
            "employeeCode": "SEBASTIAN HO KAH WAI",
            "employeeName": "SEBASTIAN HO KAH WAI",
            "isActive": 1
        },
        {
            "employeeId": "1332009090023",
            "employeeCode": "SEE AH LIM",
            "employeeName": "SEE AH LIM",
            "isActive": 1
        },
        {
            "employeeId": "1332019070001",
            "employeeCode": "SEMUEL LINTING",
            "employeeName": "SEMUEL LINTING",
            "isActive": 1
        },
        {
            "employeeId": "1332010030018",
            "employeeCode": "SENILLO ERIC SARANZA",
            "employeeName": "SENILLO ERIC SARANZA",
            "isActive": 1
        },
        {
            "employeeId": "1332010080001",
            "employeeCode": "SENIN BIN WAKIO",
            "employeeName": "SENIN BIN WAKIO",
            "isActive": 1
        },
        {
            "employeeId": "1332020080002",
            "employeeCode": "S20",
            "employeeName": "SENTEK 20",
            "isActive": 1
        },
        {
            "employeeId": "1332020080008",
            "employeeCode": "S21",
            "employeeName": "SENTEK 21",
            "isActive": 1
        },
        {
            "employeeId": "1332020080020",
            "employeeCode": "S23",
            "employeeName": "SENTEK 23",
            "isActive": 1
        },
        {
            "employeeId": "1332020080005",
            "employeeCode": "S25",
            "employeeName": "SENTEK 25",
            "isActive": 1
        },
        {
            "employeeId": "1332020080012",
            "employeeCode": "S27",
            "employeeName": "SENTEK 27",
            "isActive": 1
        },
        {
            "employeeId": "1332020080021",
            "employeeCode": "S28",
            "employeeName": "SENTEK 28",
            "isActive": 1
        },
        {
            "employeeId": "1332020080010",
            "employeeCode": "S30",
            "employeeName": "SENTEK 30",
            "isActive": 1
        },
        {
            "employeeId": "1332020080022",
            "employeeCode": "S31",
            "employeeName": "SENTEK 31",
            "isActive": 1
        },
        {
            "employeeId": "1332020080004",
            "employeeCode": "S32",
            "employeeName": "SENTEK 32",
            "isActive": 1
        },
        {
            "employeeId": "1332020080024",
            "employeeCode": "S33",
            "employeeName": "SENTEK 33",
            "isActive": 1
        },
        {
            "employeeId": "1332020060005",
            "employeeCode": "S35",
            "employeeName": "SENTEK 35",
            "isActive": 1
        },
        {
            "employeeId": "1332020080011",
            "employeeCode": "S36",
            "employeeName": "SENTEK 36",
            "isActive": 1
        },
        {
            "employeeId": "1332020080016",
            "employeeCode": "S8",
            "employeeName": "SENTEK 8",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110010",
            "employeeCode": "SEOW CHIN KEONG",
            "employeeName": "SEOW CHIN KEONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040004",
            "employeeCode": "RICC SEOW",
            "employeeName": "SEOW HWEE LING, RICC",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060003",
            "employeeCode": "SEOW TZE SEN",
            "employeeName": "SEOW TZE SEN",
            "isActive": 1
        },
        {
            "employeeId": "999131120140003",
            "employeeCode": "EDDY SEOW",
            "employeeName": "SEOW ZHENG WEI, EDDY",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040030",
            "employeeCode": "SEPTIAN RASDI",
            "employeeName": "SEPTIAN RASDI",
            "isActive": 1
        },
        {
            "employeeId": "9999060820150005",
            "employeeCode": "SET SHEIN THA",
            "employeeName": "SET SHEIN THA",
            "isActive": 1
        },
        {
            "employeeId": "999211020140006",
            "employeeCode": "SHARIZAL",
            "employeeName": "SHARIZAL BIN RAHMAT",
            "isActive": 1
        },
        {
            "employeeId": "1332020080017",
            "employeeCode": "S10",
            "employeeName": "SHENGTEK",
            "isActive": 1
        },
        {
            "employeeId": "1332009060003",
            "employeeCode": "SHERWIN",
            "employeeName": "SHERWIN",
            "isActive": 1
        },
        {
            "employeeId": "999080920140001",
            "employeeCode": "SHERWIN JIMENEZ",
            "employeeName": "SHERWIN JIMENEZ FAJARDO",
            "isActive": 1
        },
        {
            "employeeId": "1332010060003",
            "employeeCode": "SHI XIU MING",
            "employeeName": "SHI XIU MING",
            "isActive": 1
        },
        {
            "employeeId": "999210720140009",
            "employeeCode": "SHOLEHUDDIN",
            "employeeName": "SHOLEHUDDIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100007",
            "employeeCode": "SHWE TUN",
            "employeeName": "SHWE TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332009070008",
            "employeeCode": "SI THU AUNG",
            "employeeName": "SI THU AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010006",
            "employeeCode": "SIGIT PRASETYO UTOMO",
            "employeeName": "SIGIT PRASETYO UTOMO",
            "isActive": 1
        },
        {
            "employeeId": "1332009120275",
            "employeeCode": "SIGIT TRI WALOYO",
            "employeeName": "SIGIT TRI WALOYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332014020003",
            "employeeCode": "SIGIT YUNIANTO",
            "employeeName": "SIGIT YUNIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999080620150003",
            "employeeCode": "FRANCIS SIM",
            "employeeName": "SIM BENG WEE, FRANCIS",
            "isActive": 1
        },
        {
            "employeeId": "1332008080019",
            "employeeCode": "SIM CHEE YONG",
            "employeeName": "SIM CHEE YONG",
            "isActive": 1
        },
        {
            "employeeId": "1332012060004",
            "employeeCode": "SINGGIH RUDIANTO",
            "employeeName": "SINGGIH RUDIANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030016",
            "employeeCode": "SIOW KAI LOON",
            "employeeName": "SIOW KAI LOON",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110004",
            "employeeCode": "SIRAIT SURYA DHARMA",
            "employeeName": "SIRAIT SURYA DHARMA",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040028",
            "employeeCode": "SIVASHANMUKA GUNASHA",
            "employeeName": "SIVASHANMUKA S/O GUNASHAGRAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012070001",
            "employeeCode": "SLAMET",
            "employeeName": "SLAMET",
            "isActive": 1
        },
        {
            "employeeId": "1332008120013",
            "employeeCode": "SLAMET SUDJONO",
            "employeeName": "SLAMET SUDJONO",
            "isActive": 1
        },
        {
            "employeeId": "9999130220150005",
            "employeeCode": "SOBRI",
            "employeeName": "SOBRI",
            "isActive": 1
        },
        {
            "employeeId": "1332011020010",
            "employeeCode": "SOE KYAW",
            "employeeName": "SOE KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100011",
            "employeeCode": "SOE MIN NAING",
            "employeeName": "SOE MIN NAING",
            "isActive": 1
        },
        {
            "employeeId": "1332013030001",
            "employeeCode": "SOE MOE WIN",
            "employeeName": "SOE MOE WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332013020010",
            "employeeCode": "SOE NAING",
            "employeeName": "SOE NAING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013040003",
            "employeeCode": "SOE THEIN",
            "employeeName": "SOE THEIN",
            "isActive": 1
        },
        {
            "employeeId": "1332009030027",
            "employeeCode": "SOE TUN TUNG",
            "employeeName": "SOE TUN TUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013030006",
            "employeeCode": "SOE WAI AUNG",
            "employeeName": "SOE WAI AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060013",
            "employeeCode": "SOE WUNNA",
            "employeeName": "SOE WUNNA",
            "isActive": 1
        },
        {
            "employeeId": "1332011040005",
            "employeeCode": "SOEGENG GHINDARTO",
            "employeeName": "SOEGENG GHINDARTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110006",
            "employeeCode": "SOEKARDI KARSAN M.",
            "employeeName": "SOEKARDI KARSAN MARTOSUWIRYO",
            "isActive": 1
        },
        {
            "employeeId": "1332009110001",
            "employeeCode": "SOEPHIAN",
            "employeeName": "SOEPHIAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100006",
            "employeeCode": "SOH YONG CHIANG",
            "employeeName": "SOH YONG CHIANG",
            "isActive": 1
        },
        {
            "employeeId": "1332008100016",
            "employeeCode": "SOLEMAN M RAMBING",
            "employeeName": "SOLEMAN M RAMBING",
            "isActive": 1
        },
        {
            "employeeId": "1332010030017",
            "employeeCode": "SOMANG",
            "employeeName": "SOMANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060012",
            "employeeCode": "SONNY EDI WIJAYA",
            "employeeName": "SONNY EDI WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "1332008110030",
            "employeeCode": "SONY AWAWANGI",
            "employeeName": "SONY AWAWANGI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012060011",
            "employeeCode": "SONY EDI WIJAYA",
            "employeeName": "SONY EDI WIJAYA",
            "isActive": 1
        },
        {
            "employeeId": "1332008090048",
            "employeeCode": "SOPIAN",
            "employeeName": "SOPIAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010010009",
            "employeeCode": "SRI PUJO WIJAYANTO",
            "employeeName": "SRI PUJO WIJAYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080021",
            "employeeCode": "STANLEY SIM",
            "employeeName": "STANLEY SIM",
            "isActive": 1
        },
        {
            "employeeId": "999220720140011",
            "employeeCode": "STERIF MARIO",
            "employeeName": "STERIF MARIO LODEWIK PAENDONG",
            "isActive": 1
        },
        {
            "employeeId": "9999150120150009",
            "employeeCode": "STEVEN NEUBRONNER",
            "employeeName": "STEVEN NEUBRONNER",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100007",
            "employeeCode": "SU JUN RONG",
            "employeeName": "SU JUN RONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009070021",
            "employeeCode": "SUARNIATI",
            "employeeName": "SUARNIATI",
            "isActive": 1
        },
        {
            "employeeId": "1332010060012",
            "employeeCode": "SUCIPTO",
            "employeeName": "SUCIPTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009090012",
            "employeeCode": "SUDARMAN",
            "employeeName": "SUDARMAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080014",
            "employeeCode": "SUDIRMAN",
            "employeeName": "SUDIRMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332011010004",
            "employeeCode": "SUHARDI",
            "employeeName": "SUHARDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080043",
            "employeeCode": "SUHARYANTO",
            "employeeName": "SUHARYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011010004",
            "employeeCode": "SUHERDI",
            "employeeName": "SUHERDI",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100003",
            "employeeCode": "SUJAR MULYONO",
            "employeeName": "SUJAR MULYONO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010009",
            "employeeCode": "SUKIS",
            "employeeName": "SUKIS",
            "isActive": 1
        },
        {
            "employeeId": "1332014020011",
            "employeeCode": "SULAIMAN FADALI",
            "employeeName": "SULAIMAN FADALI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010006",
            "employeeCode": "SULAIMAN GILI",
            "employeeName": "SULAIMAN GILI",
            "isActive": 1
        },
        {
            "employeeId": "1332008070029",
            "employeeCode": "SULIS HADI",
            "employeeName": "SULIS HADI",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070003",
            "employeeCode": "SULISTIYO NUR ALIM",
            "employeeName": "SULISTIYO NUR ALIM",
            "isActive": 1
        },
        {
            "employeeId": "9999030520160003",
            "employeeCode": "SULTAN AKBAR TANJUNG",
            "employeeName": "SULTAN AKBAR TANJUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332010100020",
            "employeeCode": "SUMADI",
            "employeeName": "SUMADI",
            "isActive": 1
        },
        {
            "employeeId": "1332008070034",
            "employeeCode": "SUMARDIONO",
            "employeeName": "SUMARDIONO",
            "isActive": 1
        },
        {
            "employeeId": "1332010090005",
            "employeeCode": "SUMONDO",
            "employeeName": "SUMONDO",
            "isActive": 1
        },
        {
            "employeeId": "1332008080042",
            "employeeCode": "SUNARTO",
            "employeeName": "SUNARTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010120002",
            "employeeCode": "SUNNY KAMAL",
            "employeeName": "SUNNY KAMAL",
            "isActive": 1
        },
        {
            "employeeId": "999220820140004",
            "employeeCode": "SUPARJAN",
            "employeeName": "SUPARJAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008100006",
            "employeeCode": "SUPARMAN B N",
            "employeeName": "SUPARMAN B NAJAMUDDIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070030",
            "employeeCode": "SUPRIADI",
            "employeeName": "SUPRIADI",
            "isActive": 1
        },
        {
            "employeeId": "1332010070016",
            "employeeCode": "SUPRIADI ADONG",
            "employeeName": "SUPRIADI ADONG",
            "isActive": 1
        },
        {
            "employeeId": "1332008120024",
            "employeeCode": "SUPRIYADI",
            "employeeName": "SUPRIYADI",
            "isActive": 1
        },
        {
            "employeeId": "1332011030004",
            "employeeCode": "SURIANSYAH",
            "employeeName": "SURIANSYAH",
            "isActive": 1
        },
        {
            "employeeId": "1332009120257",
            "employeeCode": "SURYO BURISNO",
            "employeeName": "SURYO BURISNO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090001",
            "employeeCode": "SUSANTO",
            "employeeName": "SUSANTO",
            "isActive": 1
        },
        {
            "employeeId": "9999270420150005",
            "employeeCode": "SUTARNO",
            "employeeName": "SUTARNO",
            "isActive": 1
        },
        {
            "employeeId": "1332008110029",
            "employeeCode": "SUTRTISNO",
            "employeeName": "SUTRISNO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030015",
            "employeeCode": "SUWARGI",
            "employeeName": "SUWARGI",
            "isActive": 1
        },
        {
            "employeeId": "1332010070039",
            "employeeCode": "SUWONDO",
            "employeeName": "SUWONDO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080004",
            "employeeCode": "SWAN ZAY AUNG",
            "employeeName": "SWAN ZAY AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009070001",
            "employeeCode": "SWINGLI MELKI TUWING",
            "employeeName": "SWINGLI MELKI TUWING",
            "isActive": 1
        },
        {
            "employeeId": "999100920140004",
            "employeeCode": "SYAFRIWAN",
            "employeeName": "SYAFRIWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009060002",
            "employeeCode": "SYAIFUDIN",
            "employeeName": "SYAIFUDIN",
            "isActive": 1
        },
        {
            "employeeId": "9999220420160013",
            "employeeCode": "SYED THAHA AHMAD",
            "employeeName": "SYED THAHA AHMAD B. SYED ALI",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050005",
            "employeeCode": "SYOFYARDI",
            "employeeName": "SYOFYARDI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012120008",
            "employeeCode": "SYUAIB",
            "employeeName": "SYUAIB",
            "isActive": 1
        },
        {
            "employeeId": "9999010420150001",
            "employeeCode": "SYUKRI",
            "employeeName": "SYUKRI",
            "isActive": 1
        },
        {
            "employeeId": "1332008080059",
            "employeeCode": "TAHRORI",
            "employeeName": "TAHRORI",
            "isActive": 1
        },
        {
            "employeeId": "1332010060001",
            "employeeCode": "TAJUDIN",
            "employeeName": "TAJUDIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060002",
            "employeeCode": "TAN CHEE BENG, ERIC",
            "employeeName": "TAN CHEE BENG, ERIC",
            "isActive": 1
        },
        {
            "employeeId": "9991332012010005",
            "employeeCode": "RON TAN",
            "employeeName": "TAN CHEE KIONG, RON",
            "isActive": 1
        },
        {
            "employeeId": "1332010020023",
            "employeeCode": "TAN CHENG CHUAN",
            "employeeName": "TAN CHENG CHUAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010004",
            "employeeCode": "TAN CHIAU TONG",
            "employeeName": "TAN CHIAU TONG",
            "isActive": 1
        },
        {
            "employeeId": "9999250620160008",
            "employeeCode": "TAN HEE TAT",
            "employeeName": "TAN HEE TAT",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110011",
            "employeeCode": "TAN KAR SEONG EDDIE",
            "employeeName": "TAN KAR SEONG EDDIE",
            "isActive": 1
        },
        {
            "employeeId": "9991332008080015",
            "employeeCode": "TAN KHOON HEE",
            "employeeName": "TAN KHOON HEE",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040005",
            "employeeCode": "TAN KIA JIN , JIMMY",
            "employeeName": "TAN KIA JIN , JIMMY",
            "isActive": 1
        },
        {
            "employeeId": "999211120140008",
            "employeeCode": "EDWARD TAN",
            "employeeName": "TAN KIAT HONG, EDWARD",
            "isActive": 1
        },
        {
            "employeeId": "9999332016080001",
            "employeeCode": "TAN KOK HEONG",
            "employeeName": "TAN KOK HEONG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080015",
            "employeeCode": "TAN KOON HEE",
            "employeeName": "TAN KOON HEE",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090005",
            "employeeCode": "TAN KWANG ZIA, AININ",
            "employeeName": "TAN KWANG ZIA, AININ",
            "isActive": 1
        },
        {
            "employeeId": "9999031220150002",
            "employeeCode": "TAN LAN KOK , DON",
            "employeeName": "TAN LAN KOK , DON",
            "isActive": 1
        },
        {
            "employeeId": "1332019060001",
            "employeeCode": "TAN MINGJI",
            "employeeName": "TAN MINGJI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012100021",
            "employeeCode": "LINCOLN TAN",
            "employeeName": "TAN NAM HENG, LINCOLN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060007",
            "employeeCode": "TAN WAI SENG",
            "employeeName": "TAN WAI SENG",
            "isActive": 1
        },
        {
            "employeeId": "999200920140015",
            "employeeCode": "TANG KIAT HONG",
            "employeeName": "TANG KIAT HONG, EDWARD",
            "isActive": 1
        },
        {
            "employeeId": "9999100520160008",
            "employeeCode": "EDWARD TANG",
            "employeeName": "TANG KIAT HONG, EDWARD",
            "isActive": 1
        },
        {
            "employeeId": "1332019050001",
            "employeeCode": "TANG WAI PING",
            "employeeName": "TANG WAI PING",
            "isActive": 1
        },
        {
            "employeeId": "1332012100006",
            "employeeCode": "WILSON TANG",
            "employeeName": "TANG YONG SENG, WILSON",
            "isActive": 1
        },
        {
            "employeeId": "1332008070036",
            "employeeCode": "TARDI",
            "employeeName": "TARDI",
            "isActive": 1
        },
        {
            "employeeId": "1332011060002",
            "employeeCode": "TATANG SATIRI",
            "employeeName": "TATANG SATIRI",
            "isActive": 1
        },
        {
            "employeeId": "1332009030032",
            "employeeCode": "TAWIL",
            "employeeName": "TAWIL",
            "isActive": 1
        },
        {
            "employeeId": "1332009030029",
            "employeeCode": "TAY BOON ONG",
            "employeeName": "TAY BOON ONG",
            "isActive": 1
        },
        {
            "employeeId": "9999120120160004",
            "employeeCode": "TAY KHONG BENG",
            "employeeName": "TAY KHONG BENG",
            "isActive": 1
        },
        {
            "employeeId": "9999211120150007",
            "employeeCode": "KENNY TAY",
            "employeeName": "TAY KHONG BENG",
            "isActive": 1
        },
        {
            "employeeId": "1332008070017",
            "employeeCode": "TAY",
            "employeeName": "TAY KOK LEONG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020004",
            "employeeCode": "TEDDY FIRMANSYAH",
            "employeeName": "TEDDY FIRMANSYAH",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030014",
            "employeeCode": "TEDI MURTOPO",
            "employeeName": "TEDI MURTOPO",
            "isActive": 1
        },
        {
            "employeeId": "1332011010005",
            "employeeCode": "TEDOY JR BADONGAO",
            "employeeName": "TEDOY JR BADONGAO ABENOJAR",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090028",
            "employeeCode": "TEJADA MARK JENNUEL",
            "employeeName": "TEJADA MARK JENNUEL",
            "isActive": 1
        },
        {
            "employeeId": "9999332017070020",
            "employeeCode": "WILLIE TEO",
            "employeeName": "TEO CHEE HWEE, WILLIE",
            "isActive": 1
        },
        {
            "employeeId": "1332008080018",
            "employeeCode": "TEO CHEE LENG",
            "employeeName": "TEO CHEE LENG",
            "isActive": 1
        },
        {
            "employeeId": "1332008070014",
            "employeeCode": "SAM",
            "employeeName": "TEO JWEE CHIP, SAM",
            "isActive": 1
        },
        {
            "employeeId": "1332012060009",
            "employeeCode": "SAM TEO",
            "employeeName": "TEO JWEE CHIP, SAM",
            "isActive": 1
        },
        {
            "employeeId": "1332008070018",
            "employeeCode": "TEO JWEE NGIAP",
            "employeeName": "TEO JWEE NGIAP",
            "isActive": 1
        },
        {
            "employeeId": "99992016070008",
            "employeeCode": "TEO KHENG MENG",
            "employeeName": "TEO KHENG MENG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050001",
            "employeeCode": "TEO SONGVITH",
            "employeeName": "TEO SONGVITH",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080002",
            "employeeCode": "TEO SOON GUAN",
            "employeeName": "TEO SOON GUAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011080003",
            "employeeCode": "TEO SOON GUAN",
            "employeeName": "TEO SOON GUAN",
            "isActive": 1
        },
        {
            "employeeId": "1332010070013",
            "employeeCode": "TEO TZI YONG",
            "employeeName": "TEO TZI YONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009060009",
            "employeeCode": "TEODY JR BADIO",
            "employeeName": "TEODY JR BADIO",
            "isActive": 1
        },
        {
            "employeeId": "1332008120018",
            "employeeCode": "TEODY JR BADONGAO",
            "employeeName": "TEODY JR BADONGAO",
            "isActive": 1
        },
        {
            "employeeId": "9999270420160016",
            "employeeCode": "TERRY YUEN",
            "employeeName": "TERRY YUEN YI SIONG",
            "isActive": 1
        },
        {
            "employeeId": "999100920140005",
            "employeeCode": "TEST123",
            "employeeName": "TEST123",
            "isActive": 1
        },
        {
            "employeeId": "999221120140009",
            "employeeCode": "TEUSDY YOSEPH",
            "employeeName": "TEUSDY YOSEPH",
            "isActive": 1
        },
        {
            "employeeId": "1332019010001",
            "employeeCode": "THAM YIN CHIU",
            "employeeName": "THAM YIN CHIU",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120006",
            "employeeCode": "THAMMAWONG SUNTHON",
            "employeeName": "THAMMAWONG SUNTHON",
            "isActive": 1
        },
        {
            "employeeId": "1332008090022",
            "employeeCode": "THAN HTIKE",
            "employeeName": "THAN HTIKE",
            "isActive": 1
        },
        {
            "employeeId": "1332010100001",
            "employeeCode": "THAN TUN",
            "employeeName": "THAN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017010003",
            "employeeCode": "THANT SIN TUN",
            "employeeName": "THANT SIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120009",
            "employeeCode": "THANT ZIN",
            "employeeName": "THANT ZIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070003",
            "employeeCode": "THANT ZIN TUN",
            "employeeName": "THANT ZIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332010110003",
            "employeeCode": "THATTARAKKAL AMJITH",
            "employeeName": "THATTARAKKAL AMJITH",
            "isActive": 1
        },
        {
            "employeeId": "1332009040008",
            "employeeCode": "THAUNG NAING OO",
            "employeeName": "THAUNG NAING OO",
            "isActive": 1
        },
        {
            "employeeId": "9999040420160002",
            "employeeCode": "THAW NYI NYI TUN",
            "employeeName": "THAW NYI NYI TUN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010016",
            "employeeCode": "THAW ZIN",
            "employeeName": "THAW ZIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018060004",
            "employeeCode": "THAW ZIN AUNG",
            "employeeName": "THAW ZIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999060820150004",
            "employeeCode": "THEIN HTUT MAW",
            "employeeName": "THEIN HTUT MAW",
            "isActive": 1
        },
        {
            "employeeId": "9991332011120006",
            "employeeCode": "THEIN NAING",
            "employeeName": "THEIN NAING",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030011",
            "employeeCode": "THEIN NYUNT",
            "employeeName": "THEIN NYUNT",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070005",
            "employeeCode": "THEIN YIN MIN",
            "employeeName": "THEIN YIN MIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090012",
            "employeeCode": "THEIN ZAW",
            "employeeName": "THEIN ZAW",
            "isActive": 1
        },
        {
            "employeeId": "9999332016100002",
            "employeeCode": "THET HTOO AUNG",
            "employeeName": "THET HTOO AUNG",
            "isActive": 1
        },
        {
            "employeeId": "999090120150004",
            "employeeCode": "THET KO KO",
            "employeeName": "THET KO KO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020017",
            "employeeCode": "THET MON SOE",
            "employeeName": "THET MON SOE",
            "isActive": 1
        },
        {
            "employeeId": "9999040620150002",
            "employeeCode": "THET NYI AUNG",
            "employeeName": "THET NYI AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999100420150004",
            "employeeCode": "THET PAING PAING SOE",
            "employeeName": "THET PAING PAING SOE",
            "isActive": 1
        },
        {
            "employeeId": "9999140120160006",
            "employeeCode": "THET TUN NAING",
            "employeeName": "THET TUN NAING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080009",
            "employeeCode": "THET TUN PAING",
            "employeeName": "THET TUN PAING",
            "isActive": 1
        },
        {
            "employeeId": "1332010050002",
            "employeeCode": "THET WIN NAING",
            "employeeName": "THET WIN NAING",
            "isActive": 1
        },
        {
            "employeeId": "1332009120264",
            "employeeCode": "THI HA MYINE",
            "employeeName": "THI HA MYINE",
            "isActive": 1
        },
        {
            "employeeId": "999150120150009",
            "employeeCode": "THIAN BOON KWANG",
            "employeeName": "THIAN BOON KWANG",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030003",
            "employeeCode": "ROLAND THIAN",
            "employeeName": "THIAN BOON KWANG, ROLAND",
            "isActive": 1
        },
        {
            "employeeId": "1332010070032",
            "employeeCode": "THU HTET",
            "employeeName": "THU HTET",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020006",
            "employeeCode": "THU REIN CHAN",
            "employeeName": "THU REIN CHAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013070007",
            "employeeCode": "THURA",
            "employeeName": "THURA",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090029",
            "employeeCode": "THURA ZAW",
            "employeeName": "THURA ZAW",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020001",
            "employeeCode": "THUREIN CHAN",
            "employeeName": "THUREIN CHAN",
            "isActive": 1
        },
        {
            "employeeId": "1332011040001",
            "employeeCode": "TIGOR NAPITUPULU",
            "employeeName": "TIGOR NAPITUPULU",
            "isActive": 1
        },
        {
            "employeeId": "9999332018030014",
            "employeeCode": "TIN KHINE",
            "employeeName": "TIN KHINE",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020016",
            "employeeCode": "TIN KO KO",
            "employeeName": "TIN KO KO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030026",
            "employeeCode": "TIN MAUNG AYE",
            "employeeName": "TIN MAUNG AYE",
            "isActive": 1
        },
        {
            "employeeId": "1332010100019",
            "employeeCode": "TIN MAUNG KHIN",
            "employeeName": "TIN MAUNG KHIN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017110002",
            "employeeCode": "TIN MOE HLAING",
            "employeeName": "TIN MOE HLAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332017110003",
            "employeeCode": "TIN NAING OO",
            "employeeName": "TIN NAING OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080020",
            "employeeCode": "TING LENG SHIANG",
            "employeeName": "TING LENG SHIANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070003",
            "employeeCode": "TINT LWIN SOE",
            "employeeName": "TINT LWIN SOE",
            "isActive": 1
        },
        {
            "employeeId": "1332009060018",
            "employeeCode": "TOAK RIDEL",
            "employeeName": "TOAK RIDEL",
            "isActive": 1
        },
        {
            "employeeId": "9991332012110011",
            "employeeCode": "TOE HTET NAING",
            "employeeName": "TOE HTET NAING",
            "isActive": 1
        },
        {
            "employeeId": "999061220140001",
            "employeeCode": "TOE PYAE SONE",
            "employeeName": "TOE PYAE SONE",
            "isActive": 1
        },
        {
            "employeeId": "999913308050005",
            "employeeCode": "ARTHUR TOH",
            "employeeName": "TOH LIANG GUANG, ARTHUR",
            "isActive": 1
        },
        {
            "employeeId": "999150120150010",
            "employeeCode": "TOH SAI HUA",
            "employeeName": "TOH SAI HUA",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080012",
            "employeeCode": "TOHIRWAN",
            "employeeName": "TOHIRWAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070037",
            "employeeCode": "TOMY PURNIAWAN EKA S",
            "employeeName": "TOMY PURNIAWAN EKA SUKMA",
            "isActive": 1
        },
        {
            "employeeId": "9991332014030001",
            "employeeCode": "TONI DARIYANTO",
            "employeeName": "TONI DARIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332008070020",
            "employeeCode": "TOO THAM KONG",
            "employeeName": "TOO THAM KONG",
            "isActive": 1
        },
        {
            "employeeId": "9999311020150003",
            "employeeCode": "TORANG SITUMORANG",
            "employeeName": "TORANG SITUMORANG",
            "isActive": 1
        },
        {
            "employeeId": "999300420140017",
            "employeeCode": "TRI ARIF UJIANTO",
            "employeeName": "TRI ARIF UJIANTO",
            "isActive": 1
        },
        {
            "employeeId": "999020120150001",
            "employeeCode": "TRI HERYANTO",
            "employeeName": "TRI HERYANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332010010001",
            "employeeCode": "TRI WALOYO",
            "employeeName": "TRI WALOYO",
            "isActive": 1
        },
        {
            "employeeId": "9999191220150006",
            "employeeCode": "TRIS SUGIANTO",
            "employeeName": "TRIS SUGIANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009070006",
            "employeeCode": "TRISNO SABARUDIN",
            "employeeName": "TRISNO SABARUDIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014040007",
            "employeeCode": "TU YANG",
            "employeeName": "TU YANG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080057",
            "employeeCode": "TUESDY YOSEPH",
            "employeeName": "TUESDY YOSEPH",
            "isActive": 1
        },
        {
            "employeeId": "1332010030013",
            "employeeCode": "TUN KYAW THU",
            "employeeName": "TUN KYAW THU",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120007",
            "employeeCode": "TUN LIN AUNG",
            "employeeName": "TUN LIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "999131120140004",
            "employeeCode": "TUN LIN OO",
            "employeeName": "TUN LIN OO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017010006",
            "employeeCode": "TUN TUN OO",
            "employeeName": "TUN TUN OO",
            "isActive": 1
        },
        {
            "employeeId": "1332009050009",
            "employeeCode": "TUN TUN WIN",
            "employeeName": "TUN TUN WIN",
            "isActive": 1
        },
        {
            "employeeId": "1332008120021",
            "employeeCode": "TUNAS EKO.P",
            "employeeName": "TUNAS EKO.P",
            "isActive": 1
        },
        {
            "employeeId": "1332011010007",
            "employeeCode": "TWEK KANE SING",
            "employeeName": "TWEK KANE SING",
            "isActive": 1
        },
        {
            "employeeId": "9991332008070006",
            "employeeCode": "U AUNG KYI",
            "employeeName": "U AUNG KYI",
            "isActive": 1
        },
        {
            "employeeId": "1332010010012",
            "employeeCode": "U SAN MYINT",
            "employeeName": "U SAN MYINT",
            "isActive": 1
        },
        {
            "employeeId": "1332008090049",
            "employeeCode": "UMAR MBAKAI",
            "employeeName": "UMAR MBAKAI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020022",
            "employeeCode": "UMAR MUANAJAT",
            "employeeName": "UMAR MUANAJAT",
            "isActive": 1
        },
        {
            "employeeId": "1332011030005",
            "employeeCode": "UMESH KUMAR SINGH",
            "employeeName": "UMESH KUMAR SINGH",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110001",
            "employeeCode": "UPENDRA YADAV",
            "employeeName": "UPENDRA YADAV",
            "isActive": 1
        },
        {
            "employeeId": "9999020720150003",
            "employeeCode": "USMAN",
            "employeeName": "USMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008070048",
            "employeeCode": "UZIEL TAMBOTTO",
            "employeeName": "UZIEL TAMBOTTO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050007",
            "employeeCode": "VALENTINO SITINJAK",
            "employeeName": "VALENTINO SITINJAK",
            "isActive": 1
        },
        {
            "employeeId": "1332020060003",
            "employeeCode": "V6",
            "employeeName": "VANTEK 6",
            "isActive": 1
        },
        {
            "employeeId": "1332009030003",
            "employeeCode": "VICTOR L SINELEYAN",
            "employeeName": "VICTOR L SINELEYAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020003",
            "employeeCode": "VIKY MULINGKA",
            "employeeName": "VIKY MULINGKA",
            "isActive": 1
        },
        {
            "employeeId": "1332009010004",
            "employeeCode": "VINCENT QUEK",
            "employeeName": "VINCENT QUEK",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080013",
            "employeeCode": "WADUCHI",
            "employeeName": "WADUCHI",
            "isActive": 1
        },
        {
            "employeeId": "1332010060017",
            "employeeCode": "WAHYU PRAMESTIADI",
            "employeeName": "WAHYU PRAMESTIADI",
            "isActive": 1
        },
        {
            "employeeId": "1332012060005",
            "employeeCode": "WAHYU PRASETYO W",
            "employeeName": "WAHYU PRASETYO W",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020006",
            "employeeCode": "WAHYU PRASETYO",
            "employeeName": "WAHYU PRASETYO WIBOWO",
            "isActive": 1
        },
        {
            "employeeId": "1332009060005",
            "employeeCode": "WAHYUDI",
            "employeeName": "WAHYUDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090012",
            "employeeCode": "WAI LIN TUN",
            "employeeName": "WAI LIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012020009",
            "employeeCode": "WAI PHYO",
            "employeeName": "WAI PHYO",
            "isActive": 1
        },
        {
            "employeeId": "1332009070022",
            "employeeCode": "WAI PHYO AUNG",
            "employeeName": "WAI PHYO AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020005",
            "employeeCode": "WAI YAN PHYO",
            "employeeName": "WAI YAN PHYO",
            "isActive": 1
        },
        {
            "employeeId": "1332010090001",
            "employeeCode": "WAKIO",
            "employeeName": "WAKIO",
            "isActive": 1
        },
        {
            "employeeId": "999181220140008",
            "employeeCode": "WANA THAING WIN",
            "employeeName": "WANA THAING WIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110013",
            "employeeCode": "WANG HONGLIANG",
            "employeeName": "WANG HONGLIANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080011",
            "employeeCode": "WANG KAI",
            "employeeName": "WANG KAI",
            "isActive": 1
        },
        {
            "employeeId": "9991332012050006",
            "employeeCode": "WANG RENTIAN",
            "employeeName": "WANG RENTIAN",
            "isActive": 1
        },
        {
            "employeeId": "9999121120150003",
            "employeeCode": "WANG YU",
            "employeeName": "WANG YU",
            "isActive": 1
        },
        {
            "employeeId": "1332010100015",
            "employeeCode": "WANG YU PING",
            "employeeName": "WANG YU PING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110011",
            "employeeCode": "WARMAN",
            "employeeName": "WARMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009030030",
            "employeeCode": "WARREN M INOCENCIO",
            "employeeName": "WARREN M INOCENCIO",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010014",
            "employeeCode": "WARSONO",
            "employeeName": "WARSONO",
            "isActive": 1
        },
        {
            "employeeId": "1332009030010",
            "employeeCode": "WAYNE, KIANG",
            "employeeName": "WAYNE, KIANG",
            "isActive": 1
        },
        {
            "employeeId": "1332009090007",
            "employeeCode": "WEE HUAN LONG",
            "employeeName": "WEE HUAN LONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332009090007",
            "employeeCode": "WEE HUAN LONG HENRY",
            "employeeName": "WEE HUAN LONG, HENRY",
            "isActive": 1
        },
        {
            "employeeId": "1332020080003",
            "employeeCode": "WEITEK 18",
            "employeeName": "WEITEK 18",
            "isActive": 1
        },
        {
            "employeeId": "9999260320160005",
            "employeeCode": "WELLY WICAKSONO",
            "employeeName": "WELLY WICAKSONO",
            "isActive": 1
        },
        {
            "employeeId": "1332009060017",
            "employeeCode": "WENDRA HARDI",
            "employeeName": "WENDRA HARDI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090030",
            "employeeCode": "WENTRICH KANTOHE",
            "employeeName": "WENTRICH KANTOHE",
            "isActive": 1
        },
        {
            "employeeId": "1332009090013",
            "employeeCode": "WIJI PRIYANTO",
            "employeeName": "WIJI PRIYANTO",
            "isActive": 1
        },
        {
            "employeeId": "9991332014010001",
            "employeeCode": "WILDAN SYAIFUL ANWAR",
            "employeeName": "WILDAN SYAIFUL ANWAR",
            "isActive": 1
        },
        {
            "employeeId": "1332010020003",
            "employeeCode": "WILLEAN GINSA KALAN",
            "employeeName": "WILLEAN GINSA KALAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110002",
            "employeeCode": "WILSON SIMAN",
            "employeeName": "WILSON SIMAN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090010",
            "employeeCode": "WIN HLAING",
            "employeeName": "WIN HLAING",
            "isActive": 1
        },
        {
            "employeeId": "999090120150006",
            "employeeCode": "WIN HTAIK AUNG",
            "employeeName": "WIN HTAIK AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332018070001",
            "employeeCode": "WIN KHINE",
            "employeeName": "WIN KHINE",
            "isActive": 1
        },
        {
            "employeeId": "1332009040011",
            "employeeCode": "WIN KO LATT",
            "employeeName": "WIN KO LATT",
            "isActive": 1
        },
        {
            "employeeId": "9999270620150010",
            "employeeCode": "WIN LWIN OO",
            "employeeName": "WIN LWIN OO",
            "isActive": 1
        },
        {
            "employeeId": "1332009120271",
            "employeeCode": "WIN MAUNG",
            "employeeName": "WIN MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017020002",
            "employeeCode": "WIN MIN HTET",
            "employeeName": "WIN MIN HTET",
            "isActive": 1
        },
        {
            "employeeId": "1332008090011",
            "employeeCode": "WIN MYINT",
            "employeeName": "WIN MYINT",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090031",
            "employeeCode": "WIN NAY MYO SHWE",
            "employeeName": "WIN NAY MYO SHWE",
            "isActive": 1
        },
        {
            "employeeId": "9991332012030007",
            "employeeCode": "WIN THANT",
            "employeeName": "WIN THANT",
            "isActive": 1
        },
        {
            "employeeId": "1332009040013",
            "employeeCode": "WINARNO",
            "employeeName": "WINARNO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013080008",
            "employeeCode": "WINNIE CHEUNG",
            "employeeName": "WINNIE CHEUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332010100016",
            "employeeCode": "WINSTON KATANGING",
            "employeeName": "WINSTON KATANGING",
            "isActive": 1
        },
        {
            "employeeId": "1332009010007",
            "employeeCode": "WIRYO SETYONO",
            "employeeName": "WIRYO SETYONO",
            "isActive": 1
        },
        {
            "employeeId": "99992016070007",
            "employeeCode": "WISNU WIDAYAT",
            "employeeName": "WISNU WIDAYAT",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110001",
            "employeeCode": "WOLE REAGEN ROBERT",
            "employeeName": "WOLE REAGEN ROBERT",
            "isActive": 1
        },
        {
            "employeeId": "1332009060016",
            "employeeCode": "WOLEY REAGAN",
            "employeeName": "WOLEY REAGAN",
            "isActive": 1
        },
        {
            "employeeId": "1332009090009",
            "employeeCode": "WONG CHIN KIONG",
            "employeeName": "WONG CHIN KIONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009080001",
            "employeeCode": "WONG KEOW CHAI",
            "employeeName": "WONG KEOW CHAI",
            "isActive": 1
        },
        {
            "employeeId": "999200920140012",
            "employeeCode": "WONG KIM HONG",
            "employeeName": "WONG KIM HONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009090008",
            "employeeCode": "WONG KUIN WAH",
            "employeeName": "WONG KUIN WAH",
            "isActive": 1
        },
        {
            "employeeId": "1332018060001",
            "employeeCode": "WONG MEI CHUN,SHERRY",
            "employeeName": "WONG MEI CHUN,SHERRY",
            "isActive": 1
        },
        {
            "employeeId": "1332008070005",
            "employeeCode": "TANG FOONG",
            "employeeName": "WONG TANG FOONG",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100015",
            "employeeCode": "WONG WAI MENG",
            "employeeName": "WONG WAI MENG, ROBIN",
            "isActive": 1
        },
        {
            "employeeId": "13308050006",
            "employeeCode": "DAWAIN WONG",
            "employeeName": "WONG WAI SENG, DAWAIN",
            "isActive": 1
        },
        {
            "employeeId": "9999111120150003",
            "employeeCode": "WONG YU",
            "employeeName": "WONG YU",
            "isActive": 1
        },
        {
            "employeeId": "1332010070021",
            "employeeCode": "WU BAO YONG",
            "employeeName": "WU BAO YONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009070017",
            "employeeCode": "WYNSTON KATANGING",
            "employeeName": "WYNSTON KATANGING",
            "isActive": 1
        },
        {
            "employeeId": "9991332013020004",
            "employeeCode": "XIA LEI",
            "employeeName": "XIA LEI",
            "isActive": 1
        },
        {
            "employeeId": "1332010070020",
            "employeeCode": "XIAO HAN BIAO",
            "employeeName": "XIAO HAN BIAO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011090008",
            "employeeCode": "XIAO YAO",
            "employeeName": "XIAO YAO",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110017",
            "employeeCode": "XIONG ZHOU",
            "employeeName": "XIONG ZHOU",
            "isActive": 1
        },
        {
            "employeeId": "9999170320150004",
            "employeeCode": "YAN NAING TOE",
            "employeeName": "YAN NAING TOE",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010003",
            "employeeCode": "YAN PUI TUN",
            "employeeName": "YAN PUI TUN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110016",
            "employeeCode": "YAN PYAY",
            "employeeName": "YAN PYAY",
            "isActive": 1
        },
        {
            "employeeId": "9999332017120011",
            "employeeCode": "YAN PYI TUN",
            "employeeName": "YAN PYI TUN",
            "isActive": 1
        },
        {
            "employeeId": "1332008100014",
            "employeeCode": "YANG CHENG YAN",
            "employeeName": "YANG CHENG YAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013010002",
            "employeeCode": "YANG KAI JIE",
            "employeeName": "YANG KAI JIE, TERENCE",
            "isActive": 1
        },
        {
            "employeeId": "1332012060003",
            "employeeCode": "YANSEN SIMANJUNTAK",
            "employeeName": "YANSEN SIMANJUNTAK",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010011",
            "employeeCode": "YANTO HERI",
            "employeeName": "YANTO HERI",
            "isActive": 1
        },
        {
            "employeeId": "1332008090021",
            "employeeCode": "YAP TEEN CHEE",
            "employeeName": "YAP TEEN CHEE",
            "isActive": 1
        },
        {
            "employeeId": "1332009090005",
            "employeeCode": "YAR ZAR HEIN",
            "employeeName": "YAR ZAR HEIN",
            "isActive": 1
        },
        {
            "employeeId": "999080920140003",
            "employeeCode": "YAR ZAR MYO",
            "employeeName": "YAR ZAR MYO MINN THAW",
            "isActive": 1
        },
        {
            "employeeId": "999180920140010",
            "employeeCode": "YATAVEE HENG",
            "employeeName": "YATAVEE HENG",
            "isActive": 1
        },
        {
            "employeeId": "1332009110017",
            "employeeCode": "YE HTUT AUNG",
            "employeeName": "YE HTUT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008110027",
            "employeeCode": "YE KYAW AUNG",
            "employeeName": "YE KYAW AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332008080036",
            "employeeCode": "YE KYAW LIN",
            "employeeName": "YE KYAW LIN",
            "isActive": 1
        },
        {
            "employeeId": "9999030820150001",
            "employeeCode": "YE LIN AUNG",
            "employeeName": "YE LIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090025",
            "employeeCode": "YE LIN NAING",
            "employeeName": "YE LIN NAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120004",
            "employeeCode": "YE LIN PAING",
            "employeeName": "YE LIN PAING",
            "isActive": 1
        },
        {
            "employeeId": "9999332018060005",
            "employeeCode": "YE LWIN AUNG",
            "employeeName": "YE LWIN AUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020001",
            "employeeCode": "YE MIN KYAW",
            "employeeName": "YE MIN KYAW",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020006",
            "employeeCode": "YE MIN MYINT",
            "employeeName": "YE MIN MYINT",
            "isActive": 1
        },
        {
            "employeeId": "9999140320160004",
            "employeeCode": "YE MYAT THU",
            "employeeName": "YE MYAT THU",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050006",
            "employeeCode": "YE NANDA",
            "employeeName": "YE NANDA",
            "isActive": 1
        },
        {
            "employeeId": "1332009030017",
            "employeeCode": "YE THUT AUNG",
            "employeeName": "YE THUT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009090002",
            "employeeCode": "YE WIN",
            "employeeName": "YE WIN",
            "isActive": 1
        },
        {
            "employeeId": "9999030220150001",
            "employeeCode": "YE WINT AUNG",
            "employeeName": "YE WINT AUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332011050005",
            "employeeCode": "YE YINT MIN MAUNG",
            "employeeName": "YE YINT MIN MAUNG",
            "isActive": 1
        },
        {
            "employeeId": "9999332017090002",
            "employeeCode": "YEO GUEK POH, ERICIA",
            "employeeName": "YEO GUEK POH, ERICIA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050009",
            "employeeCode": "YEO ZHENG HAN",
            "employeeName": "YEO ZHENG HAN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017030006",
            "employeeCode": "MICKY YEOW",
            "employeeName": "YEOW LI CHENG, MICKY",
            "isActive": 1
        },
        {
            "employeeId": "1332019120001",
            "employeeCode": "YIN CHIU",
            "employeeName": "YIN CHIU",
            "isActive": 1
        },
        {
            "employeeId": "1332010100012",
            "employeeCode": "YONES MARCHIS",
            "employeeName": "YONES MARCHIS",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100019",
            "employeeCode": "YOSEP BONGGA",
            "employeeName": "YOSEP BONGGA",
            "isActive": 1
        },
        {
            "employeeId": "1332008080039",
            "employeeCode": "YOSEPH F.KUHEPENG",
            "employeeName": "YOSEPH F.KUHEPENG",
            "isActive": 1
        },
        {
            "employeeId": "9999031120150002",
            "employeeCode": "YU WANG",
            "employeeName": "YU WANG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013060011",
            "employeeCode": "YUAN JUN",
            "employeeName": "YUAN JUN",
            "isActive": 1
        },
        {
            "employeeId": "1332009010001",
            "employeeCode": "YUDA PRANOTO",
            "employeeName": "YUDA PRANOTO",
            "isActive": 1
        },
        {
            "employeeId": "99992016070012",
            "employeeCode": "YUEN YI SIONG",
            "employeeName": "YUEN YI SIONG",
            "isActive": 1
        },
        {
            "employeeId": "1332010010004",
            "employeeCode": "YULIANA SILAEN",
            "employeeName": "YULIANA SILAEN",
            "isActive": 1
        },
        {
            "employeeId": "1332008090006",
            "employeeCode": "YULIANUS TIAS",
            "employeeName": "YULIANUS TIAS",
            "isActive": 1
        },
        {
            "employeeId": "1332009080003",
            "employeeCode": "YULIK SISWANTO",
            "employeeName": "YULIK SISWANTO",
            "isActive": 1
        },
        {
            "employeeId": "1332009040016",
            "employeeCode": "YULIUS KALA P",
            "employeeName": "YULIUS KALA P",
            "isActive": 1
        },
        {
            "employeeId": "1332009040017",
            "employeeCode": "YULIUS KALA P",
            "employeeName": "YULIUS KALA P",
            "isActive": 1
        },
        {
            "employeeId": "1332008070027",
            "employeeCode": "YULIUS SETIAWAN",
            "employeeName": "YULIUS SETIAWAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013120002",
            "employeeCode": "YUNAN ISMAYADI ARYAN",
            "employeeName": "YUNAN ISMAYADI ARYAN",
            "isActive": 1
        },
        {
            "employeeId": "1332012040004",
            "employeeCode": "YUNI SANTOSO",
            "employeeName": "YUNI SANTOSO",
            "isActive": 1
        },
        {
            "employeeId": "1332008110003",
            "employeeCode": "YUNUS BUNGA",
            "employeeName": "YUNUS BUNGA",
            "isActive": 1
        },
        {
            "employeeId": "1332010080002",
            "employeeCode": "YUSISKANDAR BIN YUSO",
            "employeeName": "YUSISKANDAR BIN YUSOP",
            "isActive": 1
        },
        {
            "employeeId": "9991332013090015",
            "employeeCode": "YUSRA",
            "employeeName": "YUSRA",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010007",
            "employeeCode": "YUSUF PARAENG",
            "employeeName": "YUSUF PARAENG",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050023",
            "employeeCode": "ZAHRUL ZAKARIA",
            "employeeName": "ZAHRUL ZAKARIA",
            "isActive": 1
        },
        {
            "employeeId": "1332009050004",
            "employeeCode": "ZAINUDDIN",
            "employeeName": "ZAINUDDIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332012050008",
            "employeeCode": "ZAINUDDIN BIN",
            "employeeName": "ZAINUDDIN BIN ZULAIHI",
            "isActive": 1
        },
        {
            "employeeId": "1332009020002",
            "employeeCode": "ZAR NI TIN OO",
            "employeeName": "ZAR NI TIN OO",
            "isActive": 1
        },
        {
            "employeeId": "9999332017050003",
            "employeeCode": "ZAW HTET",
            "employeeName": "ZAW HTET",
            "isActive": 1
        },
        {
            "employeeId": "9999332018010014",
            "employeeCode": "ZAW HTOO NAING",
            "employeeName": "ZAW HTOO NAING",
            "isActive": 1
        },
        {
            "employeeId": "99992016090004",
            "employeeCode": "ZAW JUNE EDISON",
            "employeeName": "ZAW JUNE EDISON",
            "isActive": 1
        },
        {
            "employeeId": "1332008110009",
            "employeeCode": "ZAW LIN TUN",
            "employeeName": "ZAW LIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9999332016120001",
            "employeeCode": "ZAW MIN OO",
            "employeeName": "ZAW MIN OO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080013",
            "employeeCode": "ZAW MIN TUN",
            "employeeName": "ZAW MIN TUN",
            "isActive": 1
        },
        {
            "employeeId": "9999332017080003",
            "employeeCode": "ZAW MYO HTUT",
            "employeeName": "ZAW MYO HTUT",
            "isActive": 1
        },
        {
            "employeeId": "9999030620150001",
            "employeeCode": "ZAW NANDA",
            "employeeName": "ZAW NANDA",
            "isActive": 1
        },
        {
            "employeeId": "1332010070033",
            "employeeCode": "ZAW THEIN OO",
            "employeeName": "ZAW THEIN OO",
            "isActive": 1
        },
        {
            "employeeId": "1332010100005",
            "employeeCode": "ZAW WIN",
            "employeeName": "ZAW WIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110017",
            "employeeCode": "ZAW WYNN",
            "employeeName": "ZAW WYNN",
            "isActive": 1
        },
        {
            "employeeId": "9999332016110003",
            "employeeCode": "ZAW ZAW  AUNG",
            "employeeName": "ZAW ZAW  AUNG",
            "isActive": 1
        },
        {
            "employeeId": "99992016090002",
            "employeeCode": "ZAW ZAW TUN",
            "employeeName": "ZAW ZAW TUN",
            "isActive": 1
        },
        {
            "employeeId": "9999290220160004",
            "employeeCode": "ZAWE MYINT WAI YAN",
            "employeeName": "ZAWE MYINT WAI YAN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011100010",
            "employeeCode": "ZAYAR PHIYO",
            "employeeName": "ZAYAR PHIYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332011110004",
            "employeeCode": "ZAYAR PHYO",
            "employeeName": "ZAYAR PHYO",
            "isActive": 1
        },
        {
            "employeeId": "9991332012080010",
            "employeeCode": "ZHANGJIE",
            "employeeName": "ZHA ZHANGJIE",
            "isActive": 1
        },
        {
            "employeeId": "9991332013110016",
            "employeeCode": "ZHANG XIAOYUN",
            "employeeName": "ZHANG XIAOYUN",
            "isActive": 1
        },
        {
            "employeeId": "9999332018050001",
            "employeeCode": "ZHANG XU",
            "employeeName": "ZHANG XU",
            "isActive": 1
        },
        {
            "employeeId": "1332011010003",
            "employeeCode": "ZHOU WEI LONG",
            "employeeName": "ZHOU WEI LONG",
            "isActive": 1
        },
        {
            "employeeId": "1332009030028",
            "employeeCode": "ZIN HPONE WAI",
            "employeeName": "ZIN HPONE WAI",
            "isActive": 1
        },
        {
            "employeeId": "999090120150005",
            "employeeCode": "ZIN MAUNG MAUNG",
            "employeeName": "ZIN MAUNG MAUNG THEIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050003",
            "employeeCode": "ZIN MIN NAUNG",
            "employeeName": "ZIN MIN NAUNG",
            "isActive": 1
        },
        {
            "employeeId": "1332009050003",
            "employeeCode": "ZIN MIN OO",
            "employeeName": "ZIN MIN OO",
            "isActive": 1
        },
        {
            "employeeId": "9999332018020009",
            "employeeCode": "ZIN MIN THEIN",
            "employeeName": "ZIN MIN THEIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332011070009",
            "employeeCode": "ZOE PAI",
            "employeeName": "ZOE PAI",
            "isActive": 1
        },
        {
            "employeeId": "9991332013050008",
            "employeeCode": "ZUL HARIZ",
            "employeeName": "ZUL HARIZ B JAMALUDDIN",
            "isActive": 1
        },
        {
            "employeeId": "999300420140015",
            "employeeCode": "ZULKARNAIN",
            "employeeName": "ZULKARNAIN",
            "isActive": 1
        },
        {
            "employeeId": "9991332014020004",
            "employeeCode": "ZULMUBIN",
            "employeeName": "ZULMUBIN",
            "isActive": 1
        }
    ],
    "error": null
}

export default employeeList.data;
