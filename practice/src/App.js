import React,{Component} from 'react'
import { BrowserRouter as Router,Route,Switch } from 'react-router-dom';
import './App.css';
import './Requisition.css'
import Login from'./login'
import Requisition from './Requisition'
function App(){
  return (
   <Router>
     <Switch>
        <Route exact path="/" component={Login}/>
        <Route exact path="/Requisition" component={Requisition}/> 
     </Switch>
   </Router>
  )
}
export default App;