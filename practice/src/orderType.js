import React from 'react';
// import commonService from '../services/commonService';

const OrderTypes = {
  "status": "OK",
  "data": [
      {
          "orderTypeId": 1007,
          "branchId": 1,
          "orderCategory": 1,
          "orderTypeCode": "OTHERS",
          "orderTypeName": "OTHERS",
          "accountCategory": "134080001",
          "isActive": 1,
          "isSystem": 1,
          "createBy": "bts",
          "editBy": "",
          "createDate": "2010-11-29",
          "editDate": null
      },
      {
          "orderTypeId": 1004,
          "branchId": 1,
          "orderCategory": 1,
          "orderTypeCode": "SPARES",
          "orderTypeName": "SPARES",
          "accountCategory": "134080001",
          "isActive": 1,
          "isSystem": 1,
          "createBy": "bts",
          "editBy": "",
          "createDate": "2010-11-29",
          "editDate": null
      },
      {
          "orderTypeId": 1005,
          "branchId": 1,
          "orderCategory": 1,
          "orderTypeCode": "STORES",
          "orderTypeName": "STORES",
          "accountCategory": "134080001",
          "isActive": 1,
          "isSystem": 1,
          "createBy": "bts",
          "editBy": "",
          "createDate": "2010-11-29",
          "editDate": null
      },
      {
          "orderTypeId": 1006,
          "branchId": 1,
          "orderCategory": 2,
          "orderTypeCode": "WORKS",
          "orderTypeName": "WORKS",
          "accountCategory": "134080001",
          "isActive": 1,
          "isSystem": 1,
          "createBy": "bts",
          "editBy": "",
          "createDate": "2010-11-29",
          "editDate": null
      }
  ],
  "error": null
};

export default OrderTypes.data;